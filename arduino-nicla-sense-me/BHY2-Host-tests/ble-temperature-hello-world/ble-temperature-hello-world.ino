#include "Arduino_BHY2Host.h"

Sensor temperature(SENSOR_ID_TEMP);

/**
   BLE Temperature "Hello World" Test

   Steps:
   1. Connect the Nicla Sense ME
   2. Load the sketch placed in File > Examples > Arduino_BHY2 > App
   3. Connect the Arduino host: a BLE compatible board
      (MKR family, almost every Nano, Portenta boards, ...)
   4. Load this sketch

   Docs: https://docs.arduino.cc/tutorials/nicla-sense-me/cheat-sheet#bluetooth-low-energy
*/
void setup() {
  Serial.begin(115200);
  BHY2Host.begin(false, NICLA_VIA_BLE);
  temperature.begin();
}

void loop() {
  static auto lastCheck = millis();
  BHY2Host.update();

  // check sensor values every second
  if (millis() - lastCheck >= 1000) {
    lastCheck = millis();
    Serial.println(String("temperature: ") + String(int(temperature.value())));
  }
}
