#include "Arduino_BHY2.h"

SensorOrientation orientation(SENSOR_ID_ORI);
SensorOrientation orientationWakeUp(SENSOR_ID_ORI_WU);
SensorOrientation deviceOrientation(SENSOR_ID_DEVICE_ORI);
SensorOrientation deviceOrientationWakeUp(SENSOR_ID_DEVICE_ORI_WU);

/**
   Nicla Sense ME Orientation Sensors

   Docs: https://pietropoluzzi.it/blog/iot/arduino-nicla-sense-me/
*/
void setup() {
  // initialize serial communication and wait for port to open
  Serial.begin(115200);
  // wait for serial port to connect. Needed for native USB port only
  while (!Serial);

  BHY2.begin();

  orientation.begin();
  orientationWakeUp.begin();
  deviceOrientation.begin();
  deviceOrientationWakeUp.begin();

  Serial.println("\n### Orientation Sensors\n");
}

void loop() {
  static auto lastCheck = millis();

  // update function should be continuously polled
  BHY2.update();

  if (millis() - lastCheck >= 1000) {
    lastCheck = millis();

    serialPrintSensorOrientation("orientation", orientation);
    serialPrintSensorOrientation("orientationWakeUp", orientationWakeUp);
    serialPrintSensorOrientation("deviceOrientation", deviceOrientation);
    serialPrintSensorOrientation("deviceOrientationWakeUp", deviceOrientationWakeUp);

    Serial.println("___________________________________________________________\n");
  }
}

void serialPrintSensorOrientation(String sensor_name, SensorOrientation sensor) {
  Serial.println(sensor_name + ": pitch = " + sensor.pitch() + ", roll = " + sensor.roll() + ", heading = " + sensor.heading());
}
