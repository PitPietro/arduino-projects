#include "Arduino_BHY2.h"

SensorXYZ magPassthrough(SENSOR_ID_MAG_PASS);
SensorXYZ magUncalibrated(SENSOR_ID_MAG_RAW);
SensorXYZ magCorrected(SENSOR_ID_MAG);
SensorXYZ magOffset(SENSOR_ID_MAG_BIAS);
SensorXYZ magCorrectedWakeUp(SENSOR_ID_MAG_WU);
SensorXYZ magUncalibratedWakeUp(SENSOR_ID_MAG_RAW_WU);
SensorXYZ magOffsetWakeUp(SENSOR_ID_MAG_BIAS_WU);


/**
   Nicla Sense ME Magnetometer Sensors

   Docs: https://pietropoluzzi.it/blog/iot/arduino-nicla-sense-me/
*/
void setup() {
  // initialize serial communication and wait for port to open
  Serial.begin(115200);
  // wait for serial port to connect. Needed for native USB port only
  while (!Serial);

  BHY2.begin();

  magPassthrough.begin();
  magUncalibrated.begin();
  magCorrected.begin();
  magOffset.begin();
  magCorrectedWakeUp.begin();
  magUncalibratedWakeUp.begin();
  magOffsetWakeUp.begin();

  Serial.println("\n####### Magnetometer Sensors #######\n");
}

void loop() {
  static auto lastCheck = millis();

  // update function should be continuously polled
  BHY2.update();

  if (millis() - lastCheck >= 1000) {
    lastCheck = millis();

    Serial.println(String("Passthrough: (") + magPassthrough.x() + ", " + magPassthrough.y() + ", " + magPassthrough.z() + ")");
    Serial.println(String("Uncalibrated: (") + magUncalibrated.x() + ", " + magUncalibrated.y() + ", " + magUncalibrated.z() + ")");
    Serial.println(String("Corrected: (") + magCorrected.x() + ", " + magCorrected.y() + ", " + magCorrected.z() + ")");
    Serial.println(String("Offset: (") + magOffset.x() + ", " + magOffset.y() + ", " + magOffset.z() + ")");
    Serial.println(String("CorrectedWakeUp: (") + magCorrectedWakeUp.x() + ", " + magCorrectedWakeUp.y() + ", " + magCorrectedWakeUp.z() + ")");
    Serial.println(String("UncalibratedWakeUp: (") + magUncalibratedWakeUp.x() + ", " + magUncalibratedWakeUp.y() + ", " + magUncalibratedWakeUp.z() + ")");
    Serial.println(String("OffsetWakeUp: (") + magOffsetWakeUp.x() + ", " + magOffsetWakeUp.y() + ", " + magOffsetWakeUp.z() + ")");
    Serial.println("_______________________________________________________\n");
  }
}
