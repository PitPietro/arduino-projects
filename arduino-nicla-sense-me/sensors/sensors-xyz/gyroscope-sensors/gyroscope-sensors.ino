#include "Arduino_BHY2.h"

SensorXYZ gyroPassthrough(SENSOR_ID_GYRO_PASS);
SensorXYZ gyroUncalibrated(SENSOR_ID_GYRO_RAW);
SensorXYZ gyroCorrected(SENSOR_ID_GYRO);
SensorXYZ gyroOffset(SENSOR_ID_GYRO_BIAS);
SensorXYZ gyroCorrectedWakeUp(SENSOR_ID_GYRO_WU);
SensorXYZ gyroUncalibratedWakeUp(SENSOR_ID_GYRO_RAW_WU);
SensorXYZ gyroOffsetWakeUp(SENSOR_ID_GYRO_BIAS_WU);


/**
   Nicla Sense ME Gyroscope Sensors

   Docs: https://pietropoluzzi.it/blog/iot/arduino-nicla-sense-me/
*/
void setup() {
  // initialize serial communication and wait for port to open
  Serial.begin(115200);
  // wait for serial port to connect. Needed for native USB port only
  while (!Serial);

  BHY2.begin();

  gyroPassthrough.begin();
  gyroUncalibrated.begin();
  gyroCorrected.begin();
  gyroOffset.begin();
  gyroCorrectedWakeUp.begin();
  gyroUncalibratedWakeUp.begin();
  gyroOffsetWakeUp.begin();

  Serial.println("\n####### Gyroscope Sensors #######\n");
}

void loop() {
  static auto lastCheck = millis();

  // update function should be continuously polled
  BHY2.update();

  if (millis() - lastCheck >= 1000) {
    lastCheck = millis();

    Serial.println(String("Passthrough: (") + gyroPassthrough.x() + ", " + gyroPassthrough.y() + ", " + gyroPassthrough.z() + ")");
    Serial.println(String("Uncalibrated: (") + gyroUncalibrated.x() + ", " + gyroUncalibrated.y() + ", " + gyroUncalibrated.z() + ")");
    Serial.println(String("Corrected: (") + gyroCorrected.x() + ", " + gyroCorrected.y() + ", " + gyroCorrected.z() + ")");
    Serial.println(String("Offset: (") + gyroOffset.x() + ", " + gyroOffset.y() + ", " + gyroOffset.z() + ")");
    Serial.println(String("CorrectedWakeUp: (") + gyroCorrectedWakeUp.x() + ", " + gyroCorrectedWakeUp.y() + ", " + gyroCorrectedWakeUp.z() + ")");
    Serial.println(String("UncalibratedWakeUp: (") + gyroUncalibratedWakeUp.x() + ", " + gyroUncalibratedWakeUp.y() + ", " + gyroUncalibratedWakeUp.z() + ")");
    Serial.println(String("OffsetWakeUp: (") + gyroOffsetWakeUp.x() + ", " + gyroOffsetWakeUp.y() + ", " + gyroOffsetWakeUp.z() + ")");
    Serial.println("_______________________________________________________\n");
  }
}
