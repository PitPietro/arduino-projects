#include "Arduino_BHY2.h"

SensorXYZ mySensor(SENSOR_ID_MAG);

/**
   Nicla Sense ME SensorXYZ & Serial Plotter
   Steps:
   1) change the SensorXYZ ID according to the sensor you want to see plotted
   2) Open the Serial Plotter from Tools menu to see the given sensor being plotted

   Website: https://pietropoluzzi.it/blog/iot/arduino-nicla-sense-me/
   Docs: https://docs.arduino.cc/software/ide-v2/tutorials/ide-v2-serial-plotter
*/
void setup() {
  // initialize serial communication and wait for port to open
  Serial.begin(115200);
  // wait for serial port to connect. Needed for native USB port only
  while (!Serial);

  BHY2.begin();

  mySensor.begin();
  // Serial.println("\n####### Nicla Sense ME SensorXYZ & Serial Plotter #######\n");
}

void loop() {
  static auto lastCheck = millis();

  // update function should be continuously polled
  BHY2.update();

  if (millis() - lastCheck >= 50) {
    lastCheck = millis();

    Serial.print("x:");
    Serial.print(mySensor.x());
    Serial.print("\t");
    Serial.print("y:");
    Serial.print(mySensor.y());
    Serial.print("\t");
    Serial.print("z:");
    Serial.print(mySensor.z());
    Serial.print("\n");
  }
}
