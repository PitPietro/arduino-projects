#include "Arduino_BHY2.h"

Sensor tiltDetector(SENSOR_ID_TILT_DETECTOR);
Sensor stepDetector(SENSOR_ID_STD);
Sensor stepCounter(SENSOR_ID_STC);
Sensor stepCounterWakeUp(SENSOR_ID_STC_WU);
Sensor significantMotion(SENSOR_ID_SIG);


/**
   Nicla Sense ME Tilt & Motion Sensors

   All sensors of class Sensor cannot stay in the same sketch because of
   lack of memory: they're splitted into different sketches.

   This class include all Nicla sensors which are not from the other sensors's classes:
   - SensorOrientation
   - SensorXYZ
   - SensorQuaternion
   - SensorActivity
   - SensorBSEC

   Website: https://pietropoluzzi.it/blog/iot/arduino-nicla-sense-me/
*/
void setup() {
  // initialize serial communication and wait for port to open
  Serial.begin(115200);
  // wait for serial port to connect. Needed for native USB port only
  while (!Serial);

  BHY2.begin();

  tiltDetector.begin();
  stepDetector.begin();
  stepCounter.begin();
  stepCounterWakeUp.begin();
  significantMotion.begin();
}

void loop() {
  static auto lastCheck = millis();

  // update function should be continuously polled
  BHY2.update();

  if (millis() - lastCheck >= 100) {
    lastCheck = millis();

    printSensor("tilt_detector", tiltDetector.value());
    printSensor("step_detector", stepDetector.value());
    printSensor("step_counter", stepCounter.value());
    printSensor("step_counter_wake_up", stepCounterWakeUp.value());
    printSensor("significant_motion", significantMotion.value());

    Serial.print("\n");
  }
}

void printSensor(String sensor_name, float sensor_value) {
  Serial.print(sensor_name + ":");
  Serial.print(sensor_value);
  Serial.print("\t");
}
