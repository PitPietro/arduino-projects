#include "Arduino_BHY2.h"

Sensor hardwareStepCounter(SENSOR_ID_STC_HW);
Sensor hardwareStepDetector(SENSOR_ID_SIG_HW);
Sensor hardwareSignificantMotion(SENSOR_ID_SIG_HW);
Sensor hardwareStepCounterWakeUp(SENSOR_ID_STC_HW_WU);
Sensor hardwareStepDetectorWakeUp(SENSOR_ID_SIG_HW_WU);
Sensor anyMotion(SENSOR_ID_ANY_MOTION);
Sensor anyMotionWakeUp(SENSOR_ID_ANY_MOTION_WU);


/**
   Nicla Sense ME Hardware Sensors

   All sensors of class Sensor cannot stay in the same sketch because of
   lack of memory: they're splitted into different sketches.

   This class include all Nicla sensors which are not from the other sensors's classes:
   - SensorOrientation
   - SensorXYZ
   - SensorQuaternion
   - SensorActivity
   - SensorBSEC

   Website: https://pietropoluzzi.it/blog/iot/arduino-nicla-sense-me/
*/
void setup() {
  // initialize serial communication and wait for port to open
  Serial.begin(115200);
  // wait for serial port to connect. Needed for native USB port only
  while (!Serial);

  BHY2.begin();

  hardwareStepCounter.begin();
  hardwareStepDetector.begin();
  hardwareSignificantMotion.begin();
  hardwareStepCounterWakeUp.begin();
  hardwareStepDetectorWakeUp.begin();
  anyMotion.begin();
  anyMotionWakeUp.begin();
}

void loop() {
  static auto lastCheck = millis();

  // update function should be continuously polled
  BHY2.update();

  if (millis() - lastCheck >= 100) {
    lastCheck = millis();

    printSensor("hardware_step_counter", hardwareStepCounter.value());
    printSensor("hardware_step_detector", hardwareStepDetector.value());
    printSensor("hardware_significant_motion", hardwareSignificantMotion.value());
    printSensor("hardware_step_counter_wakeup", hardwareStepCounterWakeUp.value());
    printSensor("hardware_step_detector_wakeup", hardwareStepDetectorWakeUp.value());
    printSensor("any_motion", anyMotion.value());
    printSensor("any_motion_wakeup", anyMotionWakeUp.value());

    Serial.print("\n");
  }
}

void printSensor(String sensor_name, float sensor_value) {
  Serial.print(sensor_name + ":");
  Serial.print(sensor_value);
  Serial.print("\t");
}
