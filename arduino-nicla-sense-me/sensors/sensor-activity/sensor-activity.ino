#include "Arduino_BHY2.h"

SensorActivity activity(SENSOR_ID_AR);

/**
   Nicla Sense ME Activity Sensor

   Website: https://pietropoluzzi.it/blog/iot/arduino-nicla-sense-me/
*/
void setup() {
  // initialize serial communication and wait for port to open
  Serial.begin(115200);
  // wait for serial port to connect. Needed for native USB port only
  while (!Serial);

  BHY2.begin();
  activity.begin();
}

void loop() {
  static auto lastCheck = millis();

  // update function should be continuously polled
  BHY2.update();

  if (millis() - lastCheck >= 100) {
    lastCheck = millis();

    Serial.print(activity.value());
    Serial.print("\t");
    Serial.println(activity.getActivity());
  }
}
