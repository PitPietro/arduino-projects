#include "Arduino_BHY2.h"

SensorBSEC bsec(SENSOR_ID_BSEC);

/**
   Nicla Sense ME BSEC Sensor & Serial Plotter

   Open the Serial Plotter from Tools menu to see the BSEC sensor being plotted

   Website: https://pietropoluzzi.it/blog/iot/arduino-nicla-sense-me/
   Docs: https://docs.arduino.cc/software/ide-v2/tutorials/ide-v2-serial-plotter
*/
void setup() {
  // initialize serial communication and wait for port to open
  Serial.begin(115200);
  // wait for serial port to connect. Needed for native USB port only
  while (!Serial);

  BHY2.begin();
  bsec.begin();
}

void loop() {
  static auto lastCheck = millis();

  // update function should be continuously polled
  BHY2.update();

  if (millis() - lastCheck >= 100) {
    lastCheck = millis();

    Serial.print("IAQ:");
    Serial.print(bsec.iaq()); // IAQ value for regular use case (0 to 500)
    Serial.print("\t");
    Serial.print("Stationary_IAQ:");
    Serial.print(bsec.iaq_s()); // IAQ value for stationary use cases (0 to 500)
    Serial.print("\t");
    Serial.print("BVOC:");
    Serial.print(bsec.b_voc_eq()); // BVOC equivalent (ppm)
    // Serial.print("\t");
    // Serial.print("CO2:");
    // Serial.print(bsec.co2_eq()); // CO2 equivalent (ppm) [400,]
    Serial.print("\t");
    Serial.print("comp_Temp:");
    Serial.print(bsec.comp_t()); // compensated temperature (Celsius)
    Serial.print("\t");
    Serial.print("comp_Hum:");
    Serial.print(bsec.comp_h()); // compensated humidity
    Serial.print("\t");
    Serial.print("comp_Gas:");
    Serial.print(bsec.comp_g()); // compensated gas resistance (Ohms)
    Serial.print("\t");
    Serial.print("Accuracy:");
    Serial.print(bsec.accuracy()); // accuracy level: [0-3]
    Serial.print("\n");
  }
}
