/*
   Bluetooth Low Energy - Index Air Quality Application
   
   Please Note: ArduinoBLE.h library is required.
   https://github.com/arduino-libraries/ArduinoBLE

   Docs:
   1. https://www.arduino.cc/reference/en/libraries/arduinoble/ (Arduino reference)
   2. https://axodyne.com/2020/08/ble-uuids/ (UUIDs clarifier)

   GATT (General Attribute Profile)
   1. services & characteristics https://www.bluetooth.com/specifications/assigned-numbers/
   2. https://btprodspecificationrefs.blob.core.windows.net/assigned-values/16-bit%20UUID%20Numbers%20Document.pdf
*/

#include "Nicla_System.h"
#include <ArduinoBLE.h>
#include "Arduino_BHY2.h"

#define BLE_SENSE_UUID(val) ("ffaabbcc-" val "-00ed-aabe-ff45bdff0123")

SensorBSEC bsec(SENSOR_ID_BSEC);

// from "16-bit UUID Numbers Document" document, the service below has:
// - Allocation Type: GATT Service
// - Allocated UUID: 0x180F
// - Device Information: Battery

// create a new Bluetooth® Low Energy service
// parameter: uuid (16-bit or 128-bit UUID in String format)
// https://www.arduino.cc/reference/en/libraries/arduinoble/bleservice/
BLEService bsecService(BLE_SENSE_UUID("0000"));

// https://www.arduino.cc/reference/en/libraries/arduinoble/blecharacteristic/

// IAQ value for regular use case
BLEUnsignedIntCharacteristic iaqChar(BLE_SENSE_UUID("1001"), BLERead | BLENotify);

// IAQ value for stationary use case
// BLEUnsignedIntCharacteristic iaqsChar(BLE_SENSE_UUID("1002"), BLERead | BLENotify);

// breath VOC equivalent in ppm
// BLEFloatCharacteristic bVocEqChar(BLE_SENSE_UUID("1003"), BLERead | BLENotify);

// CO2 equivalent in ppm
// BLEUnsignedLongCharacteristic co2EqChar(BLE_SENSE_UUID("1004"), BLERead | BLENotify);

// compensated temperature (Celsius)
BLEFloatCharacteristic compTempChar(BLE_SENSE_UUID("1005"), BLERead | BLENotify);

// compensated humidity
BLEFloatCharacteristic compHumChar(BLE_SENSE_UUID("1006"), BLERead | BLENotify);

// compensated gas resistance (Ohm)
// BLEUnsignedLongCharacteristic compGasResChar(BLE_SENSE_UUID("1007"), BLERead | BLENotify);

// accuracy level (from 0 to 3)
BLECharCharacteristic accuracyChar(BLE_SENSE_UUID("1008"), BLERead | BLENotify);


/**
   BSEC output values - iaq: 0   iaq_s: 0   b_voc_eq: 0.00   co2_eq: 0   accuracy: 0   comp_t: 0.00   comp_h: 0.00   comp_g: 0
*/
void setup() {
  // initialize serial communication and wait for port to open
  Serial.begin(115200);
  // wait for serial port to connect. Needed for native USB port only
  // while (!Serial);

  BHY2.begin();
  bsec.begin();
  // initialize ArduinoBLE library
  if (!BLE.begin()) {
    Serial.println("Bluetooth® Low Energy failed to start!");
    while (1);
  }

  nicla::begin();
  // custom value in mA
  nicla::enableCharge(210);

  Serial.print("Device MAC Address: ");
  Serial.println(BLE.address());

  BLE.address().toUpperCase();

  // calculate the local and device name
  // append last two hex values of MAC address to ble_name
  String ble_name = "Digital Nose ";
//  ble_name += BLE.address()[BLE.address().length() - 5];
//  ble_name += BLE.address()[BLE.address().length() - 4];
//  ble_name += BLE.address()[BLE.address().length() - 2];
//  ble_name += BLE.address()[BLE.address().length() - 1];

  Serial.print("Device name: ");
  Serial.println(ble_name);

  // set the name that will appear when scanning for Bluetooth® devices
  BLE.setLocalName(ble_name.c_str());
  BLE.setDeviceName(ble_name.c_str());

  // set the advertised service UUID used when advertising to the value of the BLEService provided
  BLE.setAdvertisedService(bsecService);

  // add the characteristics to the BLEService
  bsecService.addCharacteristic(iaqChar);
  // bsecService.addCharacteristic(iaqsChar);
  // bsecService.addCharacteristic(bVocEqChar);
  // bsecService.addCharacteristic(co2EqChar);
  bsecService.addCharacteristic(compTempChar);
  bsecService.addCharacteristic(compHumChar);
  // bsecService.addCharacteristic(compGasResChar);
  bsecService.addCharacteristic(accuracyChar);

  // add a BLEService to the set of services the BLE device provides
  BLE.addService(bsecService);

  // start advertising the BLE device
  BLE.advertise();
  Serial.println("Bluetooth® device active, waiting for connections...");
}

void loop() {
  static auto lastCheck = millis();

  // update function should be continuously polled
  BHY2.update();

  // Serial.println(String("BSEC info: ") + bsec.toString());

  // query the central BLE device connected: wait for a BLE central
  BLEDevice central_device = BLE.central();

  // if a central is connected to the peripheral
  if (central_device) {
    Serial.print("Connected to MAC address: ");
    Serial.println(central_device.address());

    // while the central is connected
    while (central_device.connected()) {
      BHY2.update();

      // check characteristic status every 500ms
      if (millis() - lastCheck >= 500) {
        lastCheck = millis();

        /*
          Serial.print("IAQ: ");
          Serial.print(bsec.iaq());
          Serial.print("| accuracy: ");
          Serial.println(bsec.accuracy());
        */
        Serial.println(String("BSEC info: ") + bsec.toString());
        iaqChar.writeValue(bsec.iaq());
        // iaqsChar.writeValue(bsec.iaq_s());
        // bVocEqChar.writeValue(bsec.b_voc_eq());
        // co2EqChar.writeValue(bsec.co2_eq());
        compTempChar.writeValue(bsec.comp_t());
        compHumChar.writeValue(bsec.comp_h());
        // compGasResChar.writeValue(bsec.comp_g());
        accuracyChar.writeValue(bsec.accuracy());
      }
    }

    Serial.println("Disconnected from central.");
  } else {
    // try to reconnect to central while still polling update function
    BHY2.update();


    // query the central BLE device connected: wait for a BLE central
    central_device = BLE.central();
  }
}
