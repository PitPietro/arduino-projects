#include "Nicla_System.h"

/**
   functions are scoped under the "nicla" name
   use this statement to have convenient access
   without repeating explicitly the namespace
   before every function call, i.e. nicla::begin();
*/
// using namespace nicla;

const int delay_time = 1500;

/**
   Loop the RGB LED through many different colors

   Docs:
   - https://docs.arduino.cc/tutorials/nicla-sense-me/cheat-sheet#rgb-led
   - https://github.com/arduino/ArduinoCore-mbed/tree/master/libraries/Nicla_System/src
*/
void setup() {
  nicla::begin();
  nicla::leds.begin();

  // turn the LED off
  nicla::leds.setColor(off);
}

void loop() {
  const char dim = 6;

  // built-in colors
  RGBColors colors[dim] = {red, green, blue, yellow, magenta, cyan};

  for (int i = 0; i < dim; i++) {
    nicla::leds.setColor(colors[i]); // void setColor(RGBColors color);
    delay(delay_time);
  }

  // custom RGB color, use the Arduino logo color #008184
  // void setColor(uint8_t red, uint8_t green, uint8_t blue);
  uint8_t r_comp = 0;
  uint8_t g_comp = 129;
  uint8_t b_comp = 132;

  nicla::leds.setColor(r_comp, g_comp, b_comp);
  delay(5000);
}
