#include "Nicla_System.h"

boolean toggle;

void setup() {
  nicla::begin();
  nicla::leds.begin();

  // custom value in mA
  nicla::enableCharge(210);

  toggle = false;

  // initialize serial communication and wait for port to open
  Serial.begin(115200);
}

void loop() {
  static auto lastCheck = millis();

  if (millis() - lastCheck >= 2000) {
    lastCheck = millis();

    if (toggle) {
      nicla::leds.setColor(green);
    } else {
      nicla::leds.setColor(off);
    }

    toggle != toggle;

    Serial.print("Battery status: ");
    Serial.println(nicla::_pmic.readByte(BQ25120A_ADDRESS, BQ25120A_BATT_MON));

  }
}
