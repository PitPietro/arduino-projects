#include "Arduino_BHY2.h"

SensorQuaternion quaternion(SENSOR_ID_RV);

// A quaternion can represent a 3D rotation and is defined by 4 real numbers.
// x, y and z represent a vector. w is a scalar that stores the rotation around the vector.
// Docs 1: https://scriptinghelpers.org/blog/how-to-think-about-quaternions
// Docs 2: https://docs.arduino.cc/tutorials/nicla-sense-me/cheat-sheet
void setup() {
  Serial.begin(115200);
  BHY2.begin();
  quaternion.begin();
}

void loop() {
  static auto lastCheck = millis();

  // update function should be continuously polled
  BHY2.update();

  // check sensor values every 'delayTime' milliseconds
  if (millis() - lastCheck >= 1000) {
    lastCheck = millis();
    
    Serial.print(quaternion.toString());
  }
}
