#include "Arduino_BHY2.h"

SensorXYZ gyroscope(SENSOR_ID_GYRO);
int16_t valueX;
// int16_t valueY;
// int16_t valueZ;

const short delayTime = 100;

// Docs: https://docs.arduino.cc/tutorials/nicla-sense-me/cheat-sheet
void setup() {
  Serial.begin(115200);
  BHY2.begin();
  gyroscope.begin();
}

void loop() {
  static auto lastCheck = millis();

  // update function should be continuously polled
  BHY2.update();

  // check sensor values every 'delayTime' milliseconds
  if (millis() - lastCheck >= 1000) {
    lastCheck = millis();

    valueX = gyroscope.x();
    // valueY = gyroscope.y();
    // valueZ = gyroscope.z();

    Serial.println(gyroscope.toString());
    // print the individual values
    // Serial.println(valueX);
  }
}
