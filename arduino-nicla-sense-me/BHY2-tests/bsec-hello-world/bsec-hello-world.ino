#include "Arduino_BHY2.h"

SensorBSEC bsec(SENSOR_ID_BSEC);

// Docs: https://docs.arduino.cc/tutorials/nicla-sense-me/cheat-sheet
void setup() {
  Serial.begin(115200);
  BHY2.begin();
  bsec.begin();
}

void loop() {
  static auto lastCheck = millis();

  // update function should be continuously polled
  BHY2.update();

  // check sensor values every 'delayTime' milliseconds
  if (millis() - lastCheck >= 1000) {
    lastCheck = millis();

    Serial.println(bsec.toString());
  }
}
