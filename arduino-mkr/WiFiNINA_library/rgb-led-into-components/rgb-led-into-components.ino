/**
   Arduino MKR - RGB LED Color into 3 components

  <string_obj>.substring(from, to) get a substring of a String
  the starting index is inclusive, but the optional ending index is exclusive
  if the ending index is omitted, the substring continues to the end of the String

  <string_obj>.toCharArray(buf, len) copies the String’s characters to the supplied buffer

  Docs:
  - https://docs.arduino.cc/tutorials/mkr-wifi-1010/built-in-rgb
  - https://www.arduino.cc/reference/en/language/variables/data-types/stringobject/
*/

#include <WiFiNINA.h>
#include <utility/wifi_drv.h>

String payload = "ddaaf2";

// RGB LED pins
const char red_pin = 26;
const char green_pin = 25;
const char blue_pin = 27;
const char color_dim = 3;

void setup() {
  // initialize serial communication and wait for port to open
  Serial.begin(9600);
  // wait for serial port to connect. Needed for native USB port only
  while (!Serial);

  Serial.println("_____________________________________________________________");

  Serial.print("payload: ");
  Serial.println(payload);

  char red_chars[color_dim];
  char green_chars[color_dim];
  char blue_chars[color_dim];

  payload.substring(0, 2).toCharArray(red_chars, color_dim);
  payload.substring(2, 4).toCharArray(green_chars, color_dim);
  payload.substring(4, 6).toCharArray(blue_chars, color_dim);

  int red_value = strtoul(red_chars, NULL, 16);
  int green_value = strtoul(green_chars, NULL, 16);
  int blue_value = strtoul(blue_chars, NULL, 16);

  char msg[50];
  sprintf(msg, "red: %s (%d) | green: %s (%d) | blue: %s (%d)", red_chars, red_value, green_chars, green_value, blue_chars, blue_value);
  Serial.println(msg);

  WiFiDrv::analogWrite(green_pin, green_value);
  WiFiDrv::analogWrite(red_pin, red_value);
  WiFiDrv::analogWrite(blue_pin, blue_value);
}

void loop() {
}
