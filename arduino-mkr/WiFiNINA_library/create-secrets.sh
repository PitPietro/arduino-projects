#!/bin/bash

if test $# -ne 1; then
	echo "Usage create-secrets.sh <directory-name>"
	exit 1
fi

dir="$1"
file=$dir"arduino-secrets.h"

echo '#define WIFI_SSID "" // place your WiFi name inside the quotes' > "$file"
echo '#define WIFI_PWD "" // place your WiFi password inside the quotes' >> "$file"
