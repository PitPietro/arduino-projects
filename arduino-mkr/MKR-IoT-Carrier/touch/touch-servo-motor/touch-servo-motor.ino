#include <Arduino_MKRIoTCarrier.h>
#include <Servo.h>

#define FRONT 0
#define STOP 80
#define REAR 180

MKRIoTCarrier carrier;
Servo servo;
short int servo_value = 0;

/*
   Touch Servo Motor

   Wiring diagram:
   - GND --> GND (brown or black wire)
   - 5V  --> VCC (red wire)
   - D9  --> DATA IN (orange or white wire)
*/
void setup() {
  CARRIER_CASE = false;
  carrier.begin();

  servo.attach(3);

  Serial.begin(9600);
}

void loop() {
  // Each time you want to update the reads from the pads use this
  // It will update all the pads at the same time
  carrier.Buttons.update();

  if (carrier.Buttons.getTouch(TOUCH0)) {
    if (servo_value > 0) {
      servo_value--;
    }
  } else if (carrier.Buttons.getTouch(TOUCH1)) {
    if (servo_value <= 180) {
      servo_value++;
    }
  } else if (carrier.Buttons.getTouch(TOUCH2)) {
    // place the servo to position FRONT
    servo_value = FRONT;
  } else if (carrier.Buttons.getTouch(TOUCH3)) {
    // place the servo to position STOP
    servo_value = STOP;
  } else if (carrier.Buttons.getTouch(TOUCH4)) {
    // place the servo to position REAR
    servo_value = REAR;
  }

  servo.write(servo_value);
  delay(20);
}
