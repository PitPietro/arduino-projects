#include <Arduino_MKRIoTCarrier.h>

MKRIoTCarrier carrier;

void setup() {
  CARRIER_CASE = false;
  carrier.begin();

  // Initialize serial communication at 9600 baud rate
  Serial.begin(9600);
  // Wait for serial port to connect. Needed for native USB port only.
  while (!Serial)
    ;
}

void loop() {
  int red, green, blue, ambient;
  //char color_value[10];
  

  if (carrier.Light.colorAvailable()) {

    // store the RGB values and the ambient light intensity read by the light sensor
    carrier.Light.readColor(red, green, blue, ambient);
    String color = "#";
    color.concat(adgust_color(String(red, HEX)));
    color.concat(adgust_color(String(green, HEX)));
    color.concat(adgust_color(String(blue, HEX)));
    color.concat(adgust_color(String(ambient, HEX)));
    
    //sprintf(color_value, "#%s%s%s%s", String(red, HEX).c_str(), String(green, HEX).c_str(), String(blue, HEX).c_str(), String(ambient, HEX).c_str());

    Serial.print("(INT) red: " + String(red) + " ~ green: " + String(green) + " ~ blue: " + String(blue) + " ~ ambient: " + String(ambient) + " | ");
    Serial.print("(OCT) red: " + String(red, OCT) + " ~ green: " + String(green, OCT) + " ~ blue: " + String(blue, OCT) + " ~ ambient: " + String(ambient, OCT) + " | ");
    Serial.print("(HEX) red: " + String(red, HEX) + " ~ green: " + String(green, HEX) + " ~ blue: " + String(blue, HEX) + " ~ ambient: " + String(ambient, HEX) + " | ");
    Serial.println(color);
  }
  delay(500);
}

/**
 * Adds leading 0 to HEX numbers made by one char
 */
String adgust_color(String color_value) {
  String msg;
  if (color_value.length() == 1) {
    msg = "0";
  }
  msg.concat(color_value);
  return msg;
}
