# Play Melody on MKR IoT Carrier

## Theorical Notes
Assuming a base time of `4/4`, the notes' names and time are:

|  American            |  British             |   time   |
|----------------------|----------------------|----------|
|  whole note          |  semibreve           |  `4/4`   |
|  half note           |  minim               |  `2/4`   |
|  quarter note        |  crotchet            |  `1/4`   |
|  eighth note         |  quaver              |  `1/8`   |
|  sixteenth note      |  semiquaver          |  `1/16`  |
|  thirty-second note  |  demisemiquaver      |  `1/32`  |
|  sixty-fourth note   |  hemidemisemiquaver  |  `1/64`  |


**Please Note**: the hemidemisemiquaver is also known as semidemisemiquaver

Play the melody you want by specifying the note (from the constants in `musical-notes.h`) and the length.
Assuming that:
1. the base time of `4/4`
2. `4'000 ms = 4 seconds = 1 second * 4 musical measures = 60 BMP` (Beats per Minutes)

Then, the notes' length are:

<table>
<tr>
<th>type</th>
<th>computations</th>
<th>length</th>
</tr>
<tr>
<td>whole note</td>
<td> <code> 4000 * 1 = 4000 / 1 </code> </td>
<td> <code> 4000 </code> </td>
</tr>
<tr>
<td>half note</td>
<td> <code> 4000 * (1/2) = 4000 / 2 </code> </td>
<td> <code> 2000 </code> </td>
</tr>
<tr>
<td>quarter note</td>
<td> <code> 4000 * (1/4) = 4000 / 4 </code> </td>
<td> <code> 1000 </code> </td>
</tr>
<tr>
<td>eighth note</td>
<td> <code> 4000 * (1/8) = 4000 / 8 </code> </td>
<td> <code> 500 </code> </td>
</tr>
<tr>
<td>sixteenth note</td>
<td> <code> 4000 * (1/16) = 4000 / 16 </code> </td>
<td> <code> 250 </code> </td>
</tr>
<tr>
<td>thirty-second note</td>
<td> <code> 4000 * (1/32) = 4000 / 32 </code> </td>
<td> <code> 125 </code> </td>
</tr>
<tr>
<td>sixty-fourth note</td>
<td> <code> 4000 * (1/64) = 4000 / 64 </code> </td>
<td> <code> 62.5 </code> </td>
</tr>
</table>

Notes with dot:
<table>
<tr>
<th>type</th>
<th>computations</th>
<th>length</th>
</tr>
<tr>
<td>dotted whole note</td>
<td> <code> 4000 * 3/2 = 4000 / (2/3) = (4000 / 1) + (4000 / 2) </code> </td>
<td> <code> 6000 </code> </td>
</tr>
<tr>
<td>dotted half note</td>
<td> <code> 4000 * 3/4 = 4000 / (4/3) = (4000 / 2) + (4000 / 4) </code> </td>
<td> <code> 3000 </code> </td>
</tr>
<tr>
<td>dotted quarter note</td>
<td> <code> 4000 * 3/8 = 4000 / (8/3) = (4000 / 4) + (4000 / 8) </code> </td>
<td> <code> 1500 </code> </td>
</tr>
<tr>
<td>dotted eighth note</td>
<td> <code> 4000 * 3/16 = 4000 / (16/3) = (4000 / 8) + (4000 / 16) </code> </td>
<td> <code> 750 </code> </td>
</tr>
<tr>
<td>dotted sixteenth note</td>
<td> <code> 4000 * 3/32 = 100 / (32/3) = (4000 / 16) + (4000 / 32) </code> </td>
<td> <code> 375 </code> </td>
</tr>
<tr>
<td>dotted thirty-second note</td>
<td> <code> 4000 * 3/64 = 4000 / (64/3) = (4000 / 32) + (4000 / 64) </code> </td>
<td> <code> 187.5 </code> </td>
</tr>
<tr>
<td>dotted sixty-fourth note</td>
<td> <code> 4000 * 3/128 = 4000 / (128/3) = (4000 / 64) + (4000 / 128) </code> </td>
<td> <code> 93.75 </code> </td>
</tr>
</table>


