/*
   Change fonts in MKR IoT Carrier's display

   the display is controller using the Adafruit-ST7735-Library with a resolution of 240x240
   https://cdn-learn.adafruit.com/downloads/pdf/adafruit-gfx-graphics-library.pdf

   width and height are defined in the same header file as above
   ```
   #define ST7735_TFTWIDTH_128 128  // for 1.44 and mini
   #define ST7735_TFTWIDTH_80 80    // for mini
   #define ST7735_TFTHEIGHT_128 128 // for 1.44" display
   #define ST7735_TFTHEIGHT_160 160 // for 1.8" and mini display
   ```

   And are setted in https://github.com/adafruit/Adafruit-ST7735-Library/blob/master/Adafruit_ST7735.cpp


   Docs: https://www.arduino.cc/reference/en/libraries/arduino_mkriotcarrier/
   Arduino Style Guide: https://www.arduino.cc/en/Reference/StyleGuide
   Adafruit-ST7735-Library: https://github.com/adafruit/Adafruit-ST7735-Library/
   Change font: https://learn.adafruit.com/adafruit-gfx-graphics-library/using-fonts?view=all
*/
#include <Adafruit_GFX.h>  // https://github.com/adafruit/Adafruit-GFX-Library/tree/master/Fonts

// https://github.com/adafruit/Adafruit-GFX-Library/tree/master/Fonts
#include <Fonts/FreeMono12pt7b.h>
#include <Fonts/FreeMono18pt7b.h>
#include <Fonts/FreeMono9pt7b.h>
#include <Fonts/FreeMonoBold12pt7b.h>
#include <Fonts/FreeMonoBold18pt7b.h>
#include <Fonts/FreeMonoBold24pt7b.h>
#include <Fonts/FreeMonoBold9pt7b.h>
#include <Fonts/FreeMonoBoldOblique12pt7b.h>
#include <Fonts/FreeMonoBoldOblique18pt7b.h>
#include <Fonts/FreeMonoBoldOblique24pt7b.h>
#include <Fonts/FreeMonoBoldOblique9pt7b.h>
#include <Fonts/FreeMonoOblique12pt7b.h>
#include <Fonts/FreeMonoOblique18pt7b.h>
#include <Fonts/FreeMonoOblique24pt7b.h>
#include <Fonts/FreeMonoOblique9pt7b.h>
#include <Fonts/FreeSans12pt7b.h>
#include <Fonts/FreeSans18pt7b.h>
#include <Fonts/FreeSans24pt7b.h>
#include <Fonts/FreeSans9pt7b.h>
#include <Fonts/FreeSerifBold12pt7b.h>
#include <Fonts/FreeSerifBold18pt7b.h>
#include <Fonts/FreeSerifBold24pt7b.h>
#include <Fonts/FreeSerifBold9pt7b.h>
#include <Fonts/FreeSerifBoldItalic12pt7b.h>
#include <Fonts/FreeSerifBoldItalic18pt7b.h>
#include <Fonts/FreeSerifBoldItalic24pt7b.h>
#include <Fonts/FreeSerifBoldItalic9pt7b.h>
#include <Fonts/FreeSerifItalic12pt7b.h>
#include <Fonts/FreeSerifItalic18pt7b.h>
#include <Fonts/FreeSerifItalic24pt7b.h>
#include <Fonts/FreeSerifItalic9pt7b.h>
#include <Fonts/Org_01.h>
#include <Fonts/Picopixel.h>
#include <Fonts/Tiny3x3a2pt7b.h>
#include <Fonts/TomThumb.h>

const char font_numbers = 36;

GFXfont fonts[36] = {FreeMono12pt7b, FreeMono18pt7b, FreeMono9pt7b,
FreeMonoBold12pt7b, FreeMonoBold18pt7b, FreeMonoBold24pt7b, FreeMonoBold9pt7b,
FreeMonoBoldOblique12pt7b, FreeMonoBoldOblique18pt7b, FreeMonoBoldOblique24pt7b, FreeMonoBoldOblique9pt7b,
FreeMonoOblique12pt7b, FreeMonoOblique18pt7b, FreeMonoOblique24pt7b, FreeMonoOblique9pt7b,
FreeSans12pt7b, FreeSans18pt7b, FreeSans24pt7b, FreeSans9pt7b,
FreeSerifBold12pt7b, FreeSerifBold18pt7b, FreeSerifBold24pt7b, FreeSerifBold9pt7b,
FreeSerifBoldItalic12pt7b, FreeSerifBoldItalic18pt7b, FreeSerifBoldItalic24pt7b, FreeSerifBoldItalic9pt7b,
FreeSerifItalic12pt7b, FreeSerifItalic18pt7b, FreeSerifItalic24pt7b, FreeSerifItalic9pt7b,
Org_01, Picopixel, Tiny3x3a2pt7b, TomThumb};

#include <Arduino_MKRIoTCarrier.h>
MKRIoTCarrier carrier;

void setup() {
  CARRIER_CASE = false;
  carrier.begin();
  default_display_config();
}

void loop() {
  int i = 0;
  for(i = 0; i < font_numbers; i++) {
    carrier.display.setFont(&fonts[i]);
      default_display_config();
    print_text(60, 120, "Az?:01(){}");
    delay(1000);
  }
}

void default_display_config() {
  carrier.display.fillScreen(ST77XX_WHITE);
  carrier.display.setTextColor(ST77XX_BLACK);
  carrier.display.setRotation(0);
  carrier.display.setTextSize(2);
}

void print_text(unsigned char x, unsigned char y, String message) {
  carrier.display.setCursor(x, y);
  carrier.display.print(message);
}
