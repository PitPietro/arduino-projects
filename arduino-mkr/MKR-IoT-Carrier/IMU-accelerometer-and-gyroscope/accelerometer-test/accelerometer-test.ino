#include <Arduino_MKRIoTCarrier.h>

MKRIoTCarrier carrier;
float x, y, z;

/**
   Acceleromter Test

   <Arduino_MKRIoTCarrier.h> includes <Arduino_LSM6DS3.h> library to control LSM6DSOXTR microcontroller.
   Docs: https://www.arduino.cc/reference/en/libraries/arduino_lsm6ds3/
*/
void setup() {
  // initialize serial communication at 9600 baud rate
  Serial.begin(9600);
  // wait for serial port to connect. Needed for native USB port only.
  while (!Serial);

  // initialize the MKR IoT Carrier and output any errors in the serial monitor
  CARRIER_CASE = false;
  carrier.begin();
}

/*
  Acceleration note:
  One g is the force per unit mass due to gravity at the Earth's surface and is the standard gravity,
  defined as 9.80665 metres per second squared,[4] or equivalently 9.80665 newtons of force per kilogram of mass.
*/
void loop() {
  // query if new acceleration data from the IMU is available
  // returns 0 if no new acceleration data is available, 1 if new acceleration data is available
  if (carrier.IMUmodule.accelerationAvailable()) {

    // query the IMU’s accelerometer and return the acceleration in m/s^2
    carrier.IMUmodule.readAcceleration(x, y, z);

    // print the acceleration information on the serial monitor
    print_acceleration();
  }

  // always place a short delay before closing the loop
  delay(100);
}

void print_acceleration() {
  Serial.print("x: ");
  Serial.print(x);
  Serial.print(", y: ");
  Serial.print(y);
  Serial.print(", z: ");
  Serial.println(z);
}
