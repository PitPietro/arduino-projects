# GPIO Map

According to official [documentation](https://docs.arduino.cc/tutorials/nano-rp2040-connect/rp2040-python-api#gpio-map), **Nano RP2040 Connect** pinout differs from that of **RP2040** microcontroller. The table below compares the pinouts to help you while writing code:

| Arduino | RP2040 | Usage         |
| ------- | ------ | ------------- |
| TX      | GPIO0  | UART/TX       |
| RX      | GPIO1  | UART/RX       |
| D2      | GPIO25 | GPIO          |
| D3      | GPIO15 | GPIO          |
| D4      | GPIO16 | GPIO          |
| D5      | GPIO17 | GPIO          |
| D6      | GPIO18 | GPIO          |
| D7      | GPIO19 | GPIO          |
| D8      | GPIO20 | GPIO          |
| D9      | GPIO21 | GPIO          |
| D10     | GPIO5  | GPIO          |
| D11     | GPIO7  | SPI/COPI      |
| D12     | GPIO4  | SPI/CIPO      |
| D13     | GPIO6  | SPI/SCK       |
| D14/A0  | GPIO26 | ADC/RP2040    |
| D15/A1  | GPIO27 | ADC/RP2040    |
| D16/A2  | GPIO28 | ADC/RP2040    |
| D17/A3  | GPIO29 | ADC/RP2040    |
| D18/A4  | GPIO12 | I2C           |
| D19/A5  | GPIO13 | I2C           |
| D20/A6  | GPIO36 | ADC/NINA-W102 |
| D21/A7  | GPIO35 | ADC/NINA-W102 |