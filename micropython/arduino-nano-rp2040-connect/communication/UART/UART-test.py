from machine import UART, Pin
import time

# TODO: fix it, it does not read anything

# setup Universal Asynchronous Receiver-Transmitter communication
uart = UART(0, baudrate=9600, tx=Pin(0), rx=Pin(1))

while True:
   bytes = uart.write('hello') # write the buffer of bytes to the bus
   print('bytes written over UART communication: {}'.format(bytes))

   # aurt.any() returns the number of chars that can be read without blocking, or 0 if there are no characters available
   if (uart.any() > 0):
        bytes = uart.readline()
        print('bytes read from UART communication: {}'.format(bytes))

   time.sleep(1)

# Docs: https://docs.micropython.org/en/latest/library/machine.UART.html
