import time
import struct
import bluetooth
from random import randint
from machine import Pin
from micropython import const
# https://mpython.readthedocs.io/en/master/library/micropython/ubluetooth.html#id1
from ble_advertising import advertising_payload

#LED_PIN = 6 # GPIO6 is pin 13

# IRQ event codes
_IRQ_CENTRAL_CONNECT             = const(1)
_IRQ_CENTRAL_DISCONNECT          = const(2)
_IRQ_GATTS_WRITE                 = const(3)
_IRQ_GATTS_READ_REQUEST          = const(4)
_IRQ_SCAN_RESULT                 = const(5)
_IRQ_SCAN_DONE                   = const(6)
_IRQ_PERIPHERAL_CONNECT          = const(7)
_IRQ_PERIPHERAL_DISCONNECT       = const(8)
_IRQ_GATTC_SERVICE_RESULT        = const(9)
_IRQ_GATTC_SERVICE_DONE          = const(10)
_IRQ_GATTC_CHARACTERISTIC_RESULT = const(11)
_IRQ_GATTC_CHARACTERISTIC_DONE   = const(12)
_IRQ_GATTC_DESCRIPTOR_RESULT     = const(13)
_IRQ_GATTC_DESCRIPTOR_DONE       = const(14)
_IRQ_GATTC_READ_RESULT           = const(15)
_IRQ_GATTC_READ_DONE             = const(16)
_IRQ_GATTC_WRITE_DONE            = const(17)
_IRQ_GATTC_NOTIFY                = const(18)
_IRQ_GATTC_INDICATE              = const(19)
_IRQ_GATTS_INDICATE_DONE         = const(20)
_IRQ_MTU_EXCHANGED               = const(21)
_IRQ_L2CAP_ACCEPT                = const(22)
_IRQ_L2CAP_CONNECT               = const(23)
_IRQ_L2CAP_DISCONNECT            = const(24)
_IRQ_L2CAP_RECV                  = const(25)
_IRQ_L2CAP_SEND_READY            = const(26)
_IRQ_CONNECTION_UPDATE           = const(27)
_IRQ_ENCRYPTION_UPDATE           = const(28)
_IRQ_GET_SECRET                  = const(29)
_IRQ_SET_SECRET                  = const(30)

# for _IRQ_GATTS_READ_REQUEST event, the available return codes are:
#_GATTS_NO_ERROR                          = const(0x00)
#_GATTS_ERROR_READ_NOT_PERMITTED          = const(0x02)
#_GATTS_ERROR_WRITE_NOT_PERMITTED         = const(0x03)
#_GATTS_ERROR_INSUFFICIENT_AUTHENTICATION = const(0x05)
#_GATTS_ERROR_INSUFFICIENT_AUTHORIZATION  = const(0x08)
#_GATTS_ERROR_INSUFFICIENT_ENCRYPTION     = const(0x0f)

# for _IRQ_PASSKEY_ACTION event, the available actions are:
#_PASSKEY_ACTION_NONE                     = const(0)
#_PASSKEY_ACTION_INPUT                    = const(2)
#_PASSKEY_ACTION_DISPLAY                  = const(3)
#_PASSKEY_ACTION_NUMERIC_COMPARISON       = const(4)

# in order to save space in the firmware, these constants are not included on the bluetooth module
# add the ones that you need from the list above to your program
_IRQ_PASSKEY_ACTION = const(0) # NONE


# from "16-bit UUID Numbers Document" document, the service below has:
# - Allocation Type: GATT Service
# - Allocated UUID: 0x180F
# - Device Information: Battery
_BATTERY_SERVICE_UUID = bluetooth.UUID(0x180F)

# battery level characteristic:
# - UUID assigned number: 0x2A19
# - Uniform Type Identifier: org.bluetooth.characteristic.battery_level
_BATTERY_CHAR_UUID = (bluetooth.UUID(0x2A19), bluetooth.FLAG_READ | bluetooth.FLAG_NOTIFY)
_BATTERY_SERVICE = (_BATTERY_SERVICE_UUID, (_BATTERY_CHAR_UUID,),)

_CUSTOM_SERVICE_UUID = bluetooth.UUID(0x2907)
_USER_HEIGHT = (bluetooth.UUID(0x2A8E), bluetooth.FLAG_WRITE)

_USER_SERVICE = (_CUSTOM_SERVICE_UUID, (_USER_HEIGHT,),)
SERVICES = (_BATTERY_SERVICE, _USER_SERVICE)
SERVICES_UUIDs = [_BATTERY_SERVICE_UUID, _CUSTOM_SERVICE_UUID]


'''
Battery Service has read and notify flags, while User Height Service has write flag.
'''
class BLEMultipleServices:
    def __init__(self, ble, name="Nano RP2040"):
        self._ble = ble
        self._ble.active(True)
        self._ble.irq(self._irq)
        self._usr_height_buffer = bytearray()

        # docs: https://docs.micropython.org/en/latest/library/bluetooth.html#bluetooth.BLE.gatts_register_services
        ((self._battery,), (self._usr_height,), ) = self._ble.gatts_register_services(SERVICES)
        self._connections = set()
        self._handler = None
        self._payload = advertising_payload(name=name, services=SERVICES_UUIDs)
        self._advertise()

    def irq(self, handler):
        self._handler = handler

    def _irq(self, event, data):
        if event == _IRQ_CENTRAL_CONNECT: # a central has connected to this peripheral
            conn_handle, addr_type, addr = data
            self._connections.add(conn_handle)

            print('\nCentral connected: event code = {}'.format(event))
            print('\t conn_handle = {}, addr_type = {}, addr = {}'.format(conn_handle, addr_type, bytes(addr)))
        elif event == _IRQ_CENTRAL_DISCONNECT: # a central has disconnected from this peripheral
            conn_handle, addr_type, addr = data
            self._connections.remove(conn_handle)

            print('\nCentral disconnected: event code = {}\nWaiting for other devices to connect...'.format(event))
            print('\t conn_handle = {}, addr_type = {}, addr = {}'.format(conn_handle, addr_type, bytes(addr)))

            # start advertising again to allow a new connection
            self._advertise()
        elif event == _IRQ_GATTS_WRITE: # a client has written to this characteristic or descriptor
            conn_handle, attr_handle = data

            print('\nGATTS write: event code = {}'.format(event))
            print('\t conn_handle = {}, attr_handle = {}'.format(conn_handle, attr_handle))

            # Docs: https://github.com/micropython/micropython/blob/master/examples/bluetooth/ble_uart_peripheral.py

            if conn_handle in self._connections and attr_handle == self._usr_height:
                self._usr_height_buffer += self._ble.gatts_read(self._usr_height)
                print("\t write to _usr_height has been performed")
                if self._handler:
                    self._handler()

            # https://docs.micropython.org/en/latest/library/bluetooth.html#bluetooth.BLE.gatts_read
            # reads the local value for this handle
            # which has either been written by gatts_write() or by a remote client
            # self._ble.gatts_read(attr_handle)

            # you could manage attr_handle value to light up an LED
            #Pin(LED_PIN, Pin.OUT).value(int(self._ble.gatts_read(data[-1])[0]))
        elif event == _IRQ_GATTS_READ_REQUEST: # a client has issued a read (only supported on STM3)
            # return a non-zero integer to deny the read (see below), or zero (or None) to accept the read
            conn_handle, attr_handle = data

            print('\nGATT read (request): event code = {}'.format(event))
            print('\t conn_handle = {}, attr_handle = {}'.format(conn_handle, attr_handle))
        elif event == _IRQ_SCAN_RESULT: # single scan result
            addr_type, addr, adv_type, rssi, adv_data = data

            print('\nScan result: event code = {}'.format(event))
            print('\t addr_type = {}, addr = {}, adv_type = {}, rssi = {}, adv_data = {}'.format(addr_type, addr, adv_type, rssi, adv_data))
        elif event == _IRQ_SCAN_DONE: # scan duration finished or manually stopped
            print('\nScan done: event code = {}'.format(event))
        elif event == _IRQ_PERIPHERAL_CONNECT: # successful gap_connect()
            conn_handle, addr_type, addr = data

            print('\nSucessful GAP connect: event code = {}'.format(event))
            print('\t conn_handle = {}, attr_type = {}, addr = {}'.format(conn_handle, attr_type, addr))
        elif event == _IRQ_PERIPHERAL_DISCONNECT: # connected peripheral has disconnected
            conn_handle, addr_type, addr = data

            print('\nConnected peripheral has disconnected: event code = {}'.format(event))
            print('\t conn_handle = {}, attr_type = {}, addr = {}'.format(conn_handle, attr_type, addr))
        elif event == _IRQ_GATTC_SERVICE_RESULT: # called for each service found by gattc_discover_services()
            conn_handle, start_handle, end_handle, uuid = data

            print('\nService found: event code = {}'.format(event))
            print('\t conn_handle = {}, start_handle = {}, end_handle = {}, uuid = {}'.format(conn_handle, start_handle, end_handle, uuid))
        elif event == _IRQ_GATTC_SERVICE_DONE: # services discovery is complete
            # 'status' will be zero on success, implementation-specific value otherwise
            conn_handle, status = data

            print('\nServices discovery complete: event code = {}'.format(event))
            print('\t conn_handle = {}, status = {}'.format(conn_handle, status))
        elif event == _IRQ_GATTC_CHARACTERISTIC_RESULT: # called for each characteristic found by gattc_discover_services()
            conn_handle, def_handle, value_handle, properties, uuid = data

            print('\nCharacteristic found: event code = {}'.format(event))
            print('\t conn_handle = {}, def_handle = {}, value_handle = {}, properties = {}, uuid = {}'.format(conn_handle, def_handle, value_handle, properties, uuid))
        elif event == _IRQ_GATTC_CHARACTERISTIC_DONE: # characteristics discovery complete
            # 'status' will be zero on success, implementation-specific value otherwise
            conn_handle, status = data

            print('\nCharacteristics discovery complete: event code = {}'.format(event))
            print('\t conn_handle = {}, status = {}'.format(conn_handle, status))
        elif event == _IRQ_GATTC_DESCRIPTOR_RESULT: # called for each descriptor found by gattc_discover_descriptors()
            conn_handle, dsc_handle, uuid = data

            print('\nDescriptor found: event code = {}'.format(event))
            print('\t conn_handle = {}, dsc_handle = {}, uuid = {}'.format(conn_handle, dsc_handle, uuid))
        elif event == _IRQ_GATTC_DESCRIPTOR_DONE: # descriptors discovery complete
            # 'status' will be zero on success, implementation-specific value otherwise
            conn_handle, status = data

            print('\nDescriptors discovery complete: event code = {}'.format(event))
            print('\t conn_handle = {}, status = {}'.format(conn_handle, status))
        elif event == _IRQ_GATTC_READ_RESULT: # gattc_read() has completed
            conn_handle, value_handle, char_data = data

            print('\nGATT read (result): event code = {}'.format(event))
            print('\t conn_handle = {}, value_handle = {}, char_data = {}'.format(conn_handle, value_handle, bytes(char_data)))
        elif event == _IRQ_GATTC_READ_DONE: # gattc_read() has completed
            # 'value_handle' will be zero on btstack (but present on NimBLE)
            # 'status' will be zero on success, implementation-specific value otherwise
            conn_handle, value_handle, status = data

            print('\nGATT read (done): event code = {}'.format(event))
            print('\t conn_handle = {}, value_handle = {}, status = {}'.format(conn_handle, value_handle, status))
        elif event == _IRQ_GATTC_WRITE_DONE: # gattc_write() has completed
            # 'value_handle' will be zero on btstack (but present on NimBLE)
            # 'status' will be zero on success, implementation-specific value otherwise
            conn_handle, value_handle, status = data

            print('\nGATT write (done) complete: event code = {}'.format(event))
            print('\t conn_handle = {}, value_handle = {}, status = {}'.format(conn_handle, value_handle, status))
        elif event == _IRQ_GATTC_NOTIFY: # a server has sent a notify request
            conn_handle, value_handle, notify_data = data

            print('\nNotify request: event code = {}'.format(event))
            print('\t conn_handle = {}, value_handle = {}, notify_data = {}'.format(conn_handle, value_handle, notify_data))
        elif event == _IRQ_GATTC_INDICATE: # a server has sent an indicate request
            conn_handle, value_handle, notify_data = data

            print('\nIndicate request: event code = {}'.format(event))
            print('\t conn_handle = {}, value_handle = {}, notify_data = {}'.format(conn_handle, value_handle, notify_data))
        elif event == _IRQ_GATTS_INDICATE_DONE: # client has acknowledged the indication
            # 'status' will be zero on successful acknowledgment, implementation-specific value otherwise
            conn_handle, value_handle, status = data

            print('\nIndicate request acknowledge: event code = {}'.format(event))
            print('\t conn_handle = {}, value_handle = {}, status = {}'.format(conn_handle, value_handle, status))
        elif event == _IRQ_MTU_EXCHANGED: # ATT MTU exchange complete (either initiated by the board or the remote device)
            conn_handle, mtu = data

            print('\nATT MTU exchange complete: event code = {}'.format(event))
            print('\t conn_handle = {}, mtu = {}'.format(conn_handle, mtu))
        elif event == _IRQ_L2CAP_ACCEPT: # a new channel has been accepted
            # return a non-zero integer to reject the connection, zero (or None) to accept
            conn_handle, cid, psm, our_mtu, peer_mtu = data

            print('\nNew channel accepted: event code = {}'.format(event))
            print('\t conn_handle = {}, cid = {}, psm = {}, our_mtu = {}, peer_mtu = {}'.format(conn_handle, cid, psm, our_mtu, peer_mtu))
        elif event == _IRQ_L2CAP_CONNECT: # a new channel is now connected (either as a result of connecting or accepting)
            conn_handle, cid, psm, our_mtu, peer_mtu = data

            print('\nNew channel connected: event code = {}'.format(event))
            print('\t conn_handle = {}, cid = {}, psm = {}, our_mtu = {}, peer_mtu = {}'.format(conn_handle, cid, psm, our_mtu, peer_mtu))
        elif event == _IRQ_L2CAP_DISCONNECT: # existing channel has disconnected (status is zero)
            # or a connection attempt failed (non-zero status)
            conn_handle, cid, psm, status = data

            print('\nExisting channel disconnected: event code = {}'.format(event))
            print('\t conn_handle = {}, cid = {}, psm = {}, status = {}'.format(conn_handle, cid, psm, status))
        elif event == _IRQ_L2CAP_RECV: # new data is available on the channel
            # use l2cap_recvinto() to read
            conn_handle, cid = data

            print('\nNew data is available: event code = {}'.format(event))
            print('\t conn_handle = {}, cid = {}'.format(conn_handle, cid))
        elif event == _IRQ_L2CAP_SEND_READY: # a previous l2cap_send() that returned False has now completed
            # the channel is ready to send again
            # If status is non-zero, then the transmit buffer
            # overflowed and the application should re-send the data
            conn_handle, cid, status = data

            print('\nChannel ready to send again: event code = {}'.format(event))
            print('\t conn_handle = {}, cid = {}, status = {}'.format(conn_handle, cid, status))
        elif event == _IRQ_CONNECTION_UPDATE: # remote device has updated connection parameters
            conn_handle, conn_interval, conn_latency, supervision_timeout, status = data

            print('\nRemote device updated connection parameters: event code = {}'.format(event))
            print('\t conn_handle = {}, conn_interval = {}, conn_latency = {}, supervision_timeout = {}, status = {}'.format(conn_handle, conn_interval, conn_latency, supervision_timeout, status))
        elif event == _IRQ_ENCRYPTION_UPDATE: # encryption state has changed
            # the change could be caused as a result of pairing or bonding
            conn_handle, encrypted, authenticated, bonded, key_size = data

            print('\nEncryption state changed: event code = {}'.format(event))
            print('\t conn_handle = {}, encrypted = {}, authenticated = {}, bonded = {}, key_size = {}'.format(conn_handle, encrypted, authenticated, bonded, key_size))
        elif event == _IRQ_GET_SECRET: # return a stored secret
            # if key is None, return the index'th value of 'sec_type'
            # otherwise return the corresponding value for 'sec_type' and 'key'
            sec_type, index, key = data

            print('\nGet stored secret: event code = {}'.format(event))
            print('\t sec_type = {}, index = {}, key = {}'.format(sec_type, index, key))

            return value
        elif event == _IRQ_SET_SECRET: # save a secret to the store for 'sec_type' and 'key'
            sec_type, key, value = data

            print('\nSet secret: event code = {}'.format(event))
            print('\t sec_type = {}, key = {}, value = {}'.format(sec_type, key, value))

            return True
        elif event == _IRQ_PASSKEY_ACTION: # respond to a passkey request during pairing
            # see gap_passkey() for details
            # action will be an action that is compatible with the configured "io" config
            # passkey will be non-zero if action is "numeric comparison"
            conn_handle, action, passkey = data

            print('\nReply to passkey request: event code = {}'.format(event))
            print('\t conn_handle = {}, action = {}, passkey = {}'.format(conn_handle, action, passkey))


    def _advertise(self, interval_us=50000):
        self._ble.gap_advertise(interval_us, adv_data=self._payload)

    '''
    Data is sint16 in percentage with a resolution of 0.01%
    Write the local value, ready for a central to read
    '''
    def set_battery_level(self, level_c, notify=False):
        battery_level = struct.pack('<h', int(level_c * 100))
        self._ble.gatts_write(self._battery, battery_level)
        if notify:
            for conn_handle in self._connections:
                # notify all connected centrals to issue a read
                self._ble.gatts_notify(conn_handle, self._battery)

    def read_usr_height(self, sz=None):
        if not sz:
            sz = len(self._usr_height_buffer)
        result = self._usr_height_buffer[0:sz]
        self._usr_height_buffer = self._usr_height_buffer[sz:]
        return result

    '''
    Registers a callback for events from the BLE stack.

    It track connections so that you can send notifications
    Docs: https://docs.micropython.org/en/latest/library/bluetooth.html#bluetooth.BLE.irq

    def _irq(self, event, data):
        if event == _IRQ_CENTRAL_CONNECT:
            conn_handle, _, _ = data
            self._connections.add(conn_handle)
            print('>> central connected')
            #print('MAC address of central: {}'.format(data))
        elif event == _IRQ_CENTRAL_DISCONNECT:
            conn_handle, _, _ = data
            self._connections.remove(conn_handle)
            print('>> central disconnected\n>>> waiting for other devices to connect...')
            # start advertising again to allow a new connection
            self._advertise()
        elif event == _IRQ_GATTS_WRITE:
            Pin(LED_PIN, Pin.OUT).value(int(self._ble.gatts_read(data[-1])[0]))
        elif event == _IRQ_GATTS_READ_REQUEST:
            # get random value from 0 t 100
            x = randint(1, 100)
            print(x)
        else:
            print('>> event: {}'.format(event))
    '''


def demo():
    ble = bluetooth.BLE()
    ble_device = BLEMultipleServices(ble)

    def on_usr_height():
        print("user height: ", ble_device.read_usr_height().decode().strip())

    ble_device.irq(handler=on_usr_height)

    random_int = 0
    i = 0

    while True:
        # write every second, notify every 10 seconds
        i = (i + 1) % 10
        ble_device.set_battery_level(random_int, notify=i == 0)
        # ble_device.set_battery_level(random_int) # uncomment this line and comment the one above to stop notify for debugging reason

        # change the battery level with a random value
        random_int = randint(1, 100)
        time.sleep_ms(1000)



'''
Docs:
  1. https://docs.micropython.org/en/latest/library/bluetooth.html
  2. https://axodyne.com/2020/08/ble-uuids/ (UUIDs clarifier)

GATT (General Attribute Profile)
  1. services & characteristics https://www.bluetooth.com/specifications/assigned-numbers/
  2. https://btprodspecificationrefs.blob.core.windows.net/assigned-values/16-bit%20UUID%20Numbers%20Document.pdf
'''
if __name__ == "__main__":
    demo()
