import time
from machine import Pin

# setup the built-in LED as output
# built-in LED is n°13, which is RP2040's GPIO6
led = Pin(6, Pin.OUT)

# loop
while True:
    led.on()           # turn on the led
    time.sleep_ms(500) # the argument tells how many milliseconds the delay lasts
    led.off()          # turn off the led
    time.sleep_ms(500)

# Docs: https://docs.micropython.org/en/latest/library/machine.Pin.html
