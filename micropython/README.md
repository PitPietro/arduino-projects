# MicroPython on Arduino boards

> [Official Documentation](https://docs.micropython.org/en/latest/)

## Introduction

### OpenMV IDE

[OpenMV](https://openmv.io) project is about creating low-cost, extensible, Python powered, machine vision modules.
It aims to become the *Arduino of Machine Vision*.

Go to [Download](https://openmv.io/pages/download) page and select the installer based on your operating system and architecture version.
It will download a `.run` file. Open a Terminal and prompt the following commands:

```bash
$ cd ~/Downloads
$ mv openmv-ide-*.run openmv-ide.run # rename: long file names are bad to visualize
$ chmod +x openmv-ide.run # give the file execute permission
$ ./openmv-ide.run # launch, follow the installer instructions
$ rm openmv-ide.run # when done, remove the installer
```

---

## Compatible boards

Here is a list of links for ad hock tutorials about the Arduino compatible boards:

- [Nano 33 BLE](https://docs.arduino.cc/tutorials/nano-33-ble/ble-python-api)
- [Nano 33 BLE Sense](https://docs.arduino.cc/tutorials/nano-33-ble-sense/ble-sense-python-api)
- [Nano RP2040 Connect](https://docs.arduino.cc/tutorials/nano-rp2040-connect/rp2040-python-api)
- [Portenta H7](https://docs.arduino.cc/tutorials/portenta-h7/getting-started-openmv-micropython)

## Documentation

Useful links:

- [Python with Arduino Boards](https://docs.arduino.cc/learn/programming/arduino-and-python)