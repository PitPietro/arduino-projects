char VRX_pin = A2;
char VRY_pin = A3;
char push_btn = 7;

short int x_value = 0;
short int y_value = 0;
boolean push_value = 0;

/*
 * Joystick Values
 * 
 * Wiring diagram:
 * - GND --> GND
 * -  5V --> VCC
 * -  A2 --> VRX (analog, horizontal position) X-coordinate
 * -  A3 --> VRY (analog, vertical position) Y-coordinate
 * -  D7 --> SW (digital, push button) normally open
 * 
 * Docs: https://pietropoluzzi.it/blog/hardware/joystick/
 * Reference: https://arduinogetstarted.com/tutorials/arduino-joystick
 */
void setup() {
  Serial.begin(9600);
  Serial.println("X\tY\tbtn");

  pinMode(push_btn, INPUT);
}

void loop() {
  x_value = analogRead(VRX_pin);
  y_value = analogRead(VRY_pin);
  push_value = digitalRead(push_btn);

  
  Serial.print(x_value);
  Serial.print("\t");
  Serial.print(y_value);
  Serial.print("\t");
  Serial.println(push_value);
  delay(50);
}
