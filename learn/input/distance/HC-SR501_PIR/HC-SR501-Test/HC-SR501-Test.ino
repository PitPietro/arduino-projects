char sensor_pin = 8;

// avoid notifications if the state does not change
bool motion_state = false;

/**
   HC-SR501 PIR Sensor Test

   Suggestion: Set the sensor to H mode (repeat trigger)
*/
void setup() {
  pinMode(sensor_pin , INPUT);
  pinMode(LED_BUILTIN, OUTPUT);

  // Bìbegin serial communication at a baud rate of 9600
  Serial.begin(9600);
}

void loop() {
  int sensor_value = digitalRead(sensor_pin);

  // turn ON or OFF the built-in LED
  digitalWrite(LED_BUILTIN, sensor_value);

  // if motion_state changes, print on the Serial Monitor
  if (sensor_value == HIGH) {
    if (motion_state == false) {
      motion_state = true;
      Serial.println("#HC-SR501: Motion detected");
    }
  } else if (sensor_value == LOW) {
    if (motion_state == true) {
      motion_state = false;
      Serial.println("#HC-SR501: Motion ended");
    }
  }
}
