/*
   Read personal data from a MIFARE RFID card using a RFID-RC522 reader

   Example sketch showing how to read personal data to a PICC (an RFID Tag or Card) using a MFRC522 based RFID Reader on the Arduino SPI interface.

   Open the Serial Monitor with Ctrl+Shft+M shortcut or by clicking Tools menu, then Serial Monitor.
   Follow the instructions given on the serial monitor

   Typical pin layout used:
   -----------------------------------------------------------------------------------------
               MFRC522      Arduino       Arduino   Arduino    Arduino          Arduino
               Reader/PCD   Uno/101       Mega      Nano v3    Leonardo/Micro   Pro Micro
   Signal      Pin          Pin           Pin       Pin        Pin              Pin
   -----------------------------------------------------------------------------------------
   RST/Reset   RST          9             5         D9         RESET/ICSP-5     RST
   SPI SS      SDA(SS)      10            53        D10        10               10
   SPI MOSI    MOSI         11 / ICSP-4   51        D11        ICSP-4           16
   SPI MISO    MISO         12 / ICSP-1   50        D12        ICSP-1           14
   SPI SCK     SCK          13 / ICSP-3   52        D13        ICSP-3           15
*/

#include <SPI.h>
#include <MFRC522.h>

#define SS_PIN 10
#define RST_PIN 9

// single block dimension
const byte SINGLE_B = 16;

// two blocks dimension
const byte DOUBLE_B = 32;

// instance of MFRC522 class
MFRC522 rfid(SS_PIN, RST_PIN);
MFRC522::MIFARE_Key key;

typedef struct {
  byte last_name[DOUBLE_B];  // 2 blocks
  byte first_name[DOUBLE_B]; // 2 blocks
  byte birth_date[SINGLE_B]; // 1 block: dd/mm/yyyy date format
} personal_data;

typedef struct {
  String last_name = "Last name: ";
  String first_name = "First name: ";
  String birth_date = "Birth date: "; // in dd/mm/yyyy format
} personal_reply;

void read_two_block_data(int start_block, byte * data_buffer) {
  for (int i = 0; i < 2; i++) {
    read_one_block_data(start_block + i, data_buffer, SINGLE_B * i);
  }

  Serial.print("\n");
}

void read_one_block_data(byte block, byte * buffer, int buffer_position) {
  MFRC522::StatusCode status;
  byte len = 18;
  byte data_buffer[len];

  status = rfid.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, block, &key, &(rfid.uid));
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("Authentication failed: "));
    Serial.println(rfid.GetStatusCodeName(status));
    return;
  }

  status = rfid.MIFARE_Read(block, data_buffer, &len);
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("Reading failed: "));
    Serial.println(rfid.GetStatusCodeName(status));
    return;
  }

  // print data_buffer on Serial Monitor
  for (int i = 0; i < SINGLE_B; i++) {
    // 32 in ASCII is space character
    if (data_buffer[i] != 32) {
      Serial.write(data_buffer[i]);
      buffer[buffer_position + i] = data_buffer[i];
    }
  }
}

void setup() {
  Serial.begin(9600); // init Serial communication
  SPI.begin();        // init SPI bus
  rfid.PCD_Init();    // init MFRC522

  Serial.println(F("Read personal data on a MIFARE PICC:"));
}

void loop() {
  for (byte i = 0; i < 6; i++) key.keyByte[i] = 0xFF;

  // reset the loop if no new card present on the sensor/reader
  // this saves the entire process when IDLE
  if ( ! rfid.PICC_IsNewCardPresent())
    return; // restart loop()

  // verify if the NUID has been readed
  if ( ! rfid.PICC_ReadCardSerial())
    return; // restart loop()

  // dump UID
  Serial.print(F("\nCard UID:"));

  // dump PICC type
  Serial.print(F(" PICC type: "));
  MFRC522::PICC_Type piccType = rfid.PICC_GetType(rfid.uid.sak);
  Serial.println(rfid.PICC_GetTypeName(piccType));

  personal_data user_data;
  personal_reply user_reply;

  Serial.print(user_reply.first_name);
  read_two_block_data(4, user_data.first_name); // first name
  
  Serial.print(user_reply.last_name);
  read_two_block_data(1, user_data.last_name);  // last name
  
  Serial.print(user_reply.birth_date);
  read_one_block_data(6, user_data.birth_date, 0); // birth date

  // your information have been stored in user_data
  // you can now perform actions like
  // updating a database, write them on a file, ...
  // Serial.println((char *) user_data.first_name);
  // Serial.println((char *) user_data.last_name);
  // Serial.println((char *) user_data.birth_date);
  
  delay(1000);

  // put PICC in HALT state
  rfid.PICC_HaltA();

  // stop encryption on PCD
  rfid.PCD_StopCrypto1();
}
