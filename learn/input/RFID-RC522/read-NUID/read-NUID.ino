/*
   Read NUID with RFID-RC522 sensor
   Example sketch showing how to the read data from a PICC (an RFID Tag or Card) using a MFRC522 based RFID Reader on the Arduino SPI interface.

   Open the Serial Monitor with Ctrl+Shft+M shortcut or by clicking Tools menu, then Serial Monitor.

   When you present a PICC at reading distance of the MFRC522 reader, the serial output will show the type, and the NUID if a new card has been detected.
   Note: you may see "Timeout in communication" messages when removing the PICC from reading distance too early.

   Docs: https://github.com/miguelbalboa/rfid

   Typical pin layout used:
   -----------------------------------------------------------------------------------------
               MFRC522      Arduino       Arduino   Arduino    Arduino          Arduino
               Reader/PCD   Uno/101       Mega      Nano v3    Leonardo/Micro   Pro Micro
   Signal      Pin          Pin           Pin       Pin        Pin              Pin
   -----------------------------------------------------------------------------------------
   RST/Reset   RST          9             5         D9         RESET/ICSP-5     RST
   SPI SS      SDA(SS)      10            53        D10        10               10
   SPI MOSI    MOSI         11 / ICSP-4   51        D11        ICSP-4           16
   SPI MISO    MISO         12 / ICSP-1   50        D12        ICSP-1           14
   SPI SCK     SCK          13 / ICSP-3   52        D13        ICSP-3           15

   More pin layouts for other boards can be found here: https://github.com/miguelbalboa/rfid#pin-layout
*/

#include <SPI.h>
#include <MFRC522.h>

#define SS_PIN 10
#define RST_PIN 9

// instance of MFRC522 class
MFRC522 rfid(SS_PIN, RST_PIN);

MFRC522::MIFARE_Key key = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};

String string_NUID;

// Init array that will store new NUID
byte nuidPICC[4];

void setup() {
  Serial.begin(9600); // init Serial communication
  SPI.begin();        // init SPI bus
  rfid.PCD_Init();    // init MFRC522

  // override the value of key.keyByte byte array with 0xFF values
  // for (byte i = 0; i < 6; i++) key.keyByte[i] = 0xFF;

  Serial.println(F("This code scan the MIFARE Classsic NUID."));
  Serial.print(F("Using the following key:"));
  printHex(key.keyByte, MFRC522::MF_KEY_SIZE);
}

void loop() {

  // reset the loop if no new card present on the sensor/reader
  // this saves the entire process when IDLE
  if ( ! rfid.PICC_IsNewCardPresent())
    return; // restart loop()

  // verify if the NUID has been readed
  if ( ! rfid.PICC_ReadCardSerial())
    return; // restart loop()

  Serial.print(F("PICC type: "));
  MFRC522::PICC_Type piccType = rfid.PICC_GetType(rfid.uid.sak);
  Serial.println(rfid.PICC_GetTypeName(piccType));

  // check if the PICC is of Classic MIFARE type
  if (piccType != MFRC522::PICC_TYPE_MIFARE_MINI &&
      piccType != MFRC522::PICC_TYPE_MIFARE_1K &&
      piccType != MFRC522::PICC_TYPE_MIFARE_4K) {
    Serial.println(F("Your tag is not of type MIFARE Classic."));
    return; // restart loop()
  }

  // if at least a byte of rfid.uid.uidByte is not equal to the corresponding byte of nuidPICC
  // (compare corresponding positions)
  if (rfid.uid.uidByte[0] != nuidPICC[0] ||
      rfid.uid.uidByte[1] != nuidPICC[1] ||
      rfid.uid.uidByte[2] != nuidPICC[2] ||
      rfid.uid.uidByte[3] != nuidPICC[3] ) {
    Serial.println(F("A new card has been detected."));

    // store NUID into nuidPICC array
    for (byte i = 0; i < 4; i++) {
      nuidPICC[i] = rfid.uid.uidByte[i];
    }
  } else {
    Serial.println(F("Card read previously."));
  }

  Serial.println(F("The NUID tag is:"));
  Serial.print(F("\tHEX value: "));
  printHex(rfid.uid.uidByte, rfid.uid.size);
  Serial.println();
  Serial.print(F("\tDEC value: "));
  printDec(rfid.uid.uidByte, rfid.uid.size);
  Serial.println();
  printString(rfid.uid.uidByte, rfid.uid.size);
  Serial.print(F("\tString conversion: "));
  Serial.println(string_NUID);
  Serial.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");

  // put PICC in HALT state
  rfid.PICC_HaltA();

  // stop encryption on PCD
  rfid.PCD_StopCrypto1();
}


/**
   Helper routine to dump a byte array as hex values to Serial.
*/
void printHex(byte *buffer, byte bufferSize) {
  for (byte i = 0; i < bufferSize; i++) {
    Serial.print(buffer[i] < 0x10 ? " 0" : " ");
    Serial.print(buffer[i], HEX);
  }
}

/**
   Helper routine to dump a byte array as dec values to Serial.
*/
void printDec(byte *buffer, byte bufferSize) {
  for (byte i = 0; i < bufferSize; i++) {
    Serial.print(buffer[i] < 0x10 ? " 0" : " ");
    Serial.print(buffer[i], DEC);
  }
}

/**
   Helper routine to dump a byte array as hex values to String variable.
*/
void printString(byte *buffer, byte bufferSize) {
  string_NUID = "";

  for (byte i = 0; i < bufferSize; i++) {
    string_NUID  += String(buffer[i], HEX);
  }
}
