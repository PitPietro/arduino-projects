/*
   Dump Information about PICC Tag/Card

   Example sketch showing how to read data from a PICC using a MFRC522 based RFID Reader on the Arduino SPI interface.

   The program will not start until the Serial Monitor is closed.

   Typical pin layout used:
   -----------------------------------------------------------------------------------------
               MFRC522      Arduino       Arduino   Arduino    Arduino          Arduino
               Reader/PCD   Uno/101       Mega      Nano v3    Leonardo/Micro   Pro Micro
   Signal      Pin          Pin           Pin       Pin        Pin              Pin
   -----------------------------------------------------------------------------------------
   RST/Reset   RST          9             5         D9         RESET/ICSP-5     RST
   SPI SS      SDA(SS)      10            53        D10        10               10
   SPI MOSI    MOSI         11 / ICSP-4   51        D11        ICSP-4           16
   SPI MISO    MISO         12 / ICSP-1   50        D12        ICSP-1           14
   SPI SCK     SCK          13 / ICSP-3   52        D13        ICSP-3           15

   More pin layouts for other boards can be found here: https://github.com/miguelbalboa/rfid#pin-layout
*/

#include <SPI.h>
#include <MFRC522.h>

#define SS_PIN 10
#define RST_PIN 9

// instance of MFRC522 class
MFRC522 rfid(SS_PIN, RST_PIN);

void setup() {
  Serial.begin(9600); // init Serial communication
  while (!Serial);    // do nothing if no Serial port is opened
  SPI.begin();        // init SPI bus
  rfid.PCD_Init();    // init MFRC522

  // show MFRC522 Card Reader details
  rfid.PCD_DumpVersionToSerial();
  Serial.println(F("Scan PICC to see UID, SAK, type and data blocks"));
}

void loop() {
  // reset the loop if no new card present on the sensor/reader
  // this saves the entire process when IDLE
  if ( ! rfid.PICC_IsNewCardPresent())
    return; // restart loop()

  // verify if the NUID has been readed
  if ( ! rfid.PICC_ReadCardSerial())
    return; // restart loop()

  // dump debug info about the card; PICC_HaltA() is automatically called
  rfid.PICC_DumpToSerial(&(rfid.uid));

  // add a small delay to remove the PICC card from the reader
  delay(2000);
}
