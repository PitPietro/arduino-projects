/*
  MQ9 Gas Sensor Detector
  
  wiring diagram:
   - GND --> GND
   - VCC --> 5V
   - D0  --> D8
   - A0  --> A0
*/

const short int LED = LED_BUILTIN, DO_pin = 8, A0_pin = A0;

void setup() {
  Serial.begin(9600);
  pinMode(LED, OUTPUT);
  pinMode(DO_pin, INPUT);
}

void loop() {
  int alarm = 0;
  float sensor_volt;
  float RS_gas;
  float ratio;
  // replace this value with the one you get from calibration
  float R0 = -0.10;
  
  int sensorValue = analogRead(A0_pin);
  sensor_volt = ((float)sensorValue / 1024) * 5.0;
  RS_gas = (5.0 - sensor_volt) / sensor_volt; // Depend on RL on yor module
  ratio = RS_gas / R0; // ratio = RS/R0
  
  
  Serial.print("sensor_volt = ");
  Serial.println(sensor_volt);
  Serial.print("RS_ratio = ");
  Serial.println(RS_gas);
  Serial.print("Rs/R0 = ");
  Serial.println(ratio);
  Serial.print("\n\n");
  
  alarm = digitalRead(DO_pin);
  digitalWrite(LED, alarm);
  
  delay(1000);
}
