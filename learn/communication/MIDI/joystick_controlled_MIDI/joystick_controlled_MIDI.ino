#define MIN_THRESHOLD 400
#define MAX_THRESHOLD 800
#include "MIDIUSB.h"
const unsigned short int MIN_VALUE = 0;
const unsigned short int MIDDLE_VALUE = 510; // from 509 to 511
const unsigned short int MAX_VALUE = 1023;

char VRX_pin = A2;
char VRY_pin = A3;
char push_btn = 7;

unsigned short int x_value = 0;
unsigned short int y_value = 0;
boolean push_value = 0;

const unsigned char min_distance = 10;
const unsigned char max_distance = 180;
const unsigned char min_pitch = 48;
const unsigned char max_pitch = 61;
int current_pitch;

enum position {
  NONE,        // x =  510 ; y =  510
  UP,          // x =  510 ; y =    0
  DOWN,        // x =  510 ; y = 1023
  LEFT,        // x =    0 ; y =  510
  RIGHT,       // x = 1023 ; y =  510
  UP_RIGHT,    // x = 1023 ; y =    0
  UP_LEFT,     // x =    0 ; y =    0
  DOWN_RIGHT,  // x = 1023 ; y = 1023
  DOWN_LEFT,   // x =    0 ; y = 1023
};

enum axis_approximation {
  MIDDLE, // MIN_THRESHOLD <= value <= MAX_THRESHOLD
  MIN,    // value < MIN_THRESHOLD
  MAX,    // MAX_THRESHOLD > value
};

axis_approximation x_axis, y_axis;
position joystick_position;

/*
   Joystick Positions

   Map the movements you can make with a joystick:
   NONE, UP, DOWN, LEFT, RIGHT, DIAGONALS (4 movements)

   Wiring diagram:
   - GND --> GND
   -  5V --> VCC
   -  A2 --> VRX (analog, horizontal position) X-coordinate
   -  A3 --> VRY (analog, vertical position) Y-coordinate
   -  D7 --> SW (digital, push button) normally open
*/
void setup() {
  Serial.begin(9600);

  pinMode(push_btn, INPUT);
}

void loop() {
  x_value = analogRead(VRX_pin);
  y_value = analogRead(VRY_pin);
  push_value = digitalRead(push_btn);

  appoximate_position(x_value, &x_axis);
  appoximate_position(y_value, &y_axis);

  if (x_axis == axis_approximation::MIDDLE) {
    if (y_axis == x_axis) {
      joystick_position = position::NONE;
      // Serial.print("NONE");
    } else if (y_axis == axis_approximation::MIN) {
      joystick_position = position::UP;
      writeSingleNote(0, 48, 64);
    } else if (y_axis == axis_approximation::MAX) {
      joystick_position = position::DOWN;
      writeSingleNote(0, 49, 64);
    }
  } else if (x_axis == axis_approximation::MIN) {
    if (y_axis == x_axis) {
      joystick_position = position::UP_LEFT;
      writeSingleNote(0, 50, 64);
    } else if (y_axis == axis_approximation::MIDDLE) {
      joystick_position = position::LEFT;
      writeSingleNote(0, 51, 64);
    } else if (y_axis == axis_approximation::MAX) {
      joystick_position = position::DOWN_LEFT;
      writeSingleNote(0, 52, 64);
    }
  } else if (x_axis == axis_approximation::MAX) {
    if (y_axis == x_axis) {
      joystick_position = position::DOWN_RIGHT;
      writeSingleNote(0, 53, 64);
    } else if (y_axis == axis_approximation::MIDDLE) {
      joystick_position = position::RIGHT;
      writeSingleNote(0, 54, 64);
    } else if (y_axis == axis_approximation::MIN) {
      joystick_position = position::UP_RIGHT;
      writeSingleNote(0, 55, 64);
    }
  }

  // you could send this value via wireless
  Serial.println(joystick_position);

  delay(50);
}

void appoximate_position(int axis_value, axis_approximation* approx) {
  if (axis_value < MIN_THRESHOLD) {
    *approx = axis_approximation::MIN;
  } else if (axis_value > MAX_THRESHOLD) {
    *approx = axis_approximation::MAX;
  } else if (MIN_THRESHOLD <= axis_value && axis_value <= MAX_THRESHOLD) {
    *approx = axis_approximation::MIDDLE;
  }
}

// First parameter is the event type (0x09 = note on, 0x08 = note off).
// Second parameter is note-on/note-off, combined with the channel.
// Channel can be anything between 0-15. Typically reported to the user as 1-16.
// Third parameter is the note number (48 = middle C).
// Fourth parameter is the velocity (64 = normal, 127 = fastest).

void noteOn(byte channel, byte pitch, byte velocity) {
  midiEventPacket_t noteOn = {0x09, 0x90 | channel, pitch, velocity};
  MidiUSB.sendMIDI(noteOn);
}

void noteOff(byte channel, byte pitch, byte velocity) {
  midiEventPacket_t noteOff = {0x08, 0x80 | channel, pitch, velocity};
  MidiUSB.sendMIDI(noteOff);
}

void writeSingleNote(byte channel, byte pitch, byte velocity) {
  Serial.print("Sending note on: ");
  Serial.println(pitch);
  noteOn(channel, pitch, velocity);
  MidiUSB.flush();
  delay(50);

  Serial.print("Sending note off: ");
  Serial.println(pitch);

  noteOff(channel, pitch, velocity);
  MidiUSB.flush();
  delay(50);
}
