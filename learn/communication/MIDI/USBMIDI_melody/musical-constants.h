// notes constants beased on a single beat
// assuming a time of four-quarter

// i.e. the whole is 4 beats
#define WHOLE 4
#define HALF 2
#define QUARTER 1
#define EIGHTH 0.5
#define SIXTEENTH 0.25
#define THIRTY_SECOND 0.125
#define SIXTY_FOURTH 0.0625

// dotted constants starts with capital 'D'
#define D_WHOLE 6
#define D_HALF 3
#define D_QUARTER 1.5
#define D_EIGHTH 0.75
#define D_SIXTEENTH 0.375
#define D_THIRTY_SECOND 0.1875
#define D_SIXTY_FOURTH 0.093752
