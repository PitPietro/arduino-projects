/**
 * HC-SR04 MIDI Theremin
 * 
 * Pinout:
 * Sensor <---> Arduino board
 * - VCC  <---> 5V
 * - Trig <---> digital pin 3
 * - Echo <---> digital pin 2
 * - GND  <---> GND
 * 
 * Docs: https://www.arduino.cc/reference/en/language/functions/math/map/
 */
#include "MIDIUSB.h"

#define TRIG_PIN 3
#define ECHO_PIN 2

long duration, distance;

const unsigned char min_distance = 10;
const unsigned char max_distance = 180;
const unsigned char min_pitch = 48;
const unsigned char max_pitch = 61;
int current_pitch;

// First parameter is the event type (0x09 = note on, 0x08 = note off).
// Second parameter is note-on/note-off, combined with the channel.
// Channel can be anything between 0-15. Typically reported to the user as 1-16.
// Third parameter is the note number (48 = middle C).
// Fourth parameter is the velocity (64 = normal, 127 = fastest).

void noteOn(byte channel, byte pitch, byte velocity) {
  midiEventPacket_t noteOn = {0x09, 0x90 | channel, pitch, velocity};
  MidiUSB.sendMIDI(noteOn);
}

void noteOff(byte channel, byte pitch, byte velocity) {
  midiEventPacket_t noteOff = {0x08, 0x80 | channel, pitch, velocity};
  MidiUSB.sendMIDI(noteOff);
}

void setup() {
  // define I/O pins
  pinMode(TRIG_PIN, OUTPUT);
  pinMode(ECHO_PIN, INPUT);

  // initialize serial communication and wait for port to open
  // open the Serial Monitor with the shortcut Ctrl + Shift + M
  Serial.begin(9600);
  while (!Serial);

  Serial.println("~~~ HC-SR04 MIDI Theremin ~~~");
}

void loop() {
  // clear TRIG_PIN by setting it LOW
  digitalWrite(TRIG_PIN, LOW);
  delayMicroseconds(5);

  // trigger the sensor by setting TRIG_PIN ti HIGH for 10 microseconds
  digitalWrite(TRIG_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN, LOW);

  // read the ECHO_PIN. 'pulseIn()' returns the duration (length of the pulse) in microseconds
  duration = pulseIn(ECHO_PIN, HIGH);
  // Calculate the distance:
  distance = duration * 0.0343 / 2;

  // given the short distance available, let's only an octave
  // from C3 to C4# there are 14 pitches
  // (13 was a terrible number)

  // let's map the distance between the arbitrary taken thresholds
  current_pitch = map(distance, min_distance, max_distance, min_pitch, max_pitch);
  writeSingleNote(0, current_pitch, 64);

  // print the distance on the Serial Monitor
  Serial.print("distance = ");
  Serial.print(distance);
  Serial.print(" cm\t");
  
  Serial.print("pitch = ");
  Serial.println(current_pitch);
  
  delay(100);
}

void writeSingleNote(byte channel, byte pitch, byte velocity) {
  Serial.print("Sending note on: ");
  Serial.println(pitch);
  noteOn(channel, pitch, velocity);
  MidiUSB.flush();
  delay(500);

  Serial.print("Sending note off: ");
  Serial.println(pitch);

  noteOff(channel, pitch, velocity);
  MidiUSB.flush();
  delay(50);
}
