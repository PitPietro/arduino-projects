/*
    USBMIDI Read Test

   Tested with mido python library as input MIDI device
*/

#include "MIDIUSB.h"

void setup() {
  Serial.begin(115200);
  Serial.println("USBMIDI Read Test\n");
}

void loop() {
  midiEventPacket_t rx;
  do {
    rx = MidiUSB.read();
    if (rx.header != 0) {

      // verbose debugging information
      Serial.print("(HEX) Received: ");
      Serial.print(rx.header, HEX);
      Serial.print("-");
      Serial.print(rx.byte1, HEX);
      Serial.print("-");
      Serial.print(rx.byte2, HEX);
      Serial.print("-");
      Serial.println(rx.byte3, HEX);

      Serial.print("(DEC) Received: ");
      Serial.print(rx.header, DEC);
      Serial.print("-");
      Serial.print(rx.byte1, DEC);
      Serial.print("-");
      Serial.print(rx.byte2, DEC);
      Serial.print("-");
      Serial.println(rx.byte3, DEC);

      Serial.print("(BIN) Received: ");
      Serial.print(rx.header, BIN);
      Serial.print("-");
      Serial.print(rx.byte1, BIN);
      Serial.print("-");
      Serial.print(rx.byte2, BIN);
      Serial.print("-");
      Serial.println(rx.byte3, BIN);

      Serial.println("~~~~~~~~~~~~~~~");

      // do something like lighting up some LEDs
    }
  } while (rx.header != 0);
}
