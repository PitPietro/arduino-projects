/*
  Madgwick orientation calculation
  Uses Arduino MadgwickAHRS library to calculate heading, pitch, and roll
  on an Arduino Nano 33 IoT, using the onboard LSM6DS3 IMU.

  For big fun, connect this to p5.js sketch MadgwickVisualizer

  created 4 Aug 2019
  converted to webUSB 5 Jun 2020
  by Tom Igoe
*/
#include <WebUSB.h> // https://github.com/webusb/arduino
#include <Arduino_LSM6DS3.h> // https://github.com/arduino-libraries/Arduino_LSM6DS3
#include <MadgwickAHRS.h> // https://github.com/arduino-libraries/MadgwickAHRS

WebUSB WebUSBSerial(1, "");

// initialize a Madgwick filter:
Madgwick filter;
// sensor's sample rate is fixed at 104 Hz:
const float sensorRate = 104.00;

// values for orientation:
float roll = 0.0;
float pitch = 0.0;
float heading = 0.0;

void setup() {
  WebUSBSerial.begin(9600);
  Serial.begin(9600);
  // attempt to start the IMU:
  if (!IMU.begin()) {
    WebUSBSerial.println("Failed to initialize IMU");
    // stop here if you can't access the IMU:
    while (true);
  }
  // start the filter to run at the sample rate:
  filter.begin(sensorRate);
}

void loop() {
  // values for acceleration & rotation:
  float xAcc, yAcc, zAcc;
  float xGyro, yGyro, zGyro;

  // check if the IMU is ready to read:
  if (IMU.accelerationAvailable() &&
      IMU.gyroscopeAvailable()) {
    // read accelerometer & gyrometer:
    IMU.readAcceleration(xAcc, yAcc, zAcc);
    IMU.readGyroscope(xGyro, yGyro, zGyro);

    // update the filter, which computes orientation:
    filter.updateIMU(xGyro, yGyro, zGyro, xAcc, yAcc, zAcc);

    // print the heading, pitch and roll
    roll = filter.getRoll();
    pitch = filter.getPitch();
    heading = filter.getYaw();
  }

  // if you get a byte in the serial port,
  // send the latest heading, pitch, and roll:
  if (WebUSBSerial.available()) {
    char input = WebUSBSerial.read();
    Serial.println(input);
    WebUSBSerial.print(heading);
    WebUSBSerial.print(",");
    WebUSBSerial.print(pitch);
    WebUSBSerial.print(",");
    WebUSBSerial.println(roll);
  }
}

/**
   WebUSB installation guide:
   # move to the correct directory
   $ cd ~/Arduino/libraries

   # clone the repository
   $ https://github.com/webusb/arduino.git

   # or use SSH
   $ git clone git@github.com:webusb/arduino.git

   $ mv arduino/library/WebUSB ./
   $ rm -rf arduino
   $ ls
   # readme.txt  WebUSB
   $ pwd
   # $HOME/Arduino/libraries

   # the device must be upgraded from USB 2.0 to USB 2.1
   # move into the SDK installation directory
   $ cd /opt/arduino/hardware/arduino/avr/cores/arduino

   Edit USBCore.h header file:
   change this: #define USB_VERSION 0x200
   into this: #define USB_VERSION 0x210

   In Linux, enable USB device to be accessible to web application:
   $ lsusb
   # Bus 001 Device 009: ID 2341:8054 Arduino SA Arduino MKR WiFi 1010

   $ cd /etc/udev/rules.d

   $ sudo nano 70-arduino-wifi.rules

   Edit as follow:
   SUBSYSTEM=="usb", ATTRS{idVendor}=="2341", ATTR{idProduct}=="8054", MODE="0660", GROUP="plugdev"

   Enable changes:
   $ sudo udevadm control --reload-rules
   $ rules.d sudo udevadm trigger
*/
