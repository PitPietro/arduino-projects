/*
   Bluetooth Low Energy - Potentiometer Read

   Wiring Diagram: 10k Ohm Potentiometer connected to pin A1

   The major steps are:
   1. Create a new service
   2. Create an analog pin characteristic
   3. Set the name for the device
   4. Start advertising the device
   5. Create a conditional that works only if an external device is connected
   6. Read the analog pin over Bluetooth®

   Please Note: ArduinoBLE.h library is required.
   https://github.com/arduino-libraries/ArduinoBLE

   Docs:
   1. https://docs.arduino.cc/tutorials/mkr-wifi-1010/enabling-ble (tutorial)
   2. https://www.arduino.cc/reference/en/libraries/arduinoble/ (Arduino reference)
   3. https://axodyne.com/2020/08/ble-uuids/ (UUIDs clarifier)

   GATT (General Attribute Profile)
   1. services & characteristics https://www.bluetooth.com/specifications/assigned-numbers/
   2. https://btprodspecificationrefs.blob.core.windows.net/assigned-values/16-bit%20UUID%20Numbers%20Document.pdf
*/

#include <ArduinoBLE.h>

// from "16-bit UUID Numbers Document" document, the service below has:
// - Allocation Type: GATT Service
// - Allocated UUID: 0x180A
// - Device Information: Device Information

// create a new Bluetooth® Low Energy service
// parameter: uuid (16-bit or 128-bit UUID in String format)
// https://www.arduino.cc/reference/en/libraries/arduinoble/bleservice/
BLEService sensorService("180A");

// create the Analog Value characteristic
// parameters:
// 1. uuid: 16-bit or 128-bit UUID in String format
// 2. properties: mask of the properties (BLEBroadcast, BLERead, BLEWriteWithoutResponse, BLEWrite, BLENotify, BLEIndicate)
// https://www.arduino.cc/reference/en/libraries/arduinoble/blecharacteristic/

// UUID for Analog Output has
// 1. assigned number: 0x2A59
// 2. Uniform Type Identifier: org.bluetooth.characteristic.analog_output

BLEUnsignedCharCharacteristic potentiometerReading("2A59", BLERead | BLENotify);

long previous_millis = 0;

// define thepotentiometer's pin number
const char pot_pin = A1;

void setup() {
  // initialize serial communication and wait for port to open
  Serial.begin(9600);
  // wait for serial port to connect. Needed for native USB port only
  while (!Serial);

  // initialize led_pin to indicate when a central is connected
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(pot_pin, INPUT);

  // initialize ArduinoBLE library
  if (!BLE.begin()) {
    Serial.println("Bluetooth® Low Energy failed to start!");
    while (1);
  }

  // set the name that will appear when scanning for Bluetooth® devices
  BLE.setLocalName("MKR WiFi 1010");

  // set the advertised service UUID used when advertising to the value of the BLEService provided
  BLE.setAdvertisedService(sensorService);

  // add the characteristic to the BLEService
  sensorService.addCharacteristic(potentiometerReading);

  // add a BLEService to the set of services the BLE device provides
  BLE.addService(sensorService);

  // start advertising the BLE device
  BLE.advertise();
  Serial.println("Bluetooth® device active, waiting for connections...");
}

void loop() {
  // query the central BLE device connected: wait for a BLE central
  BLEDevice central_device = BLE.central();

  // save the Bluetooth® MAC address of the BLE device
  String central_address = central_device.address();

  // if a central is connected to the peripheral
  if (central_device) {
    Serial.print("MAC address of central: ");
    Serial.println(central_address);

    // turn on the built-in LED to indicate the connection
    digitalWrite(LED_BUILTIN, HIGH);

    // while the central is connected
    while (central_device.connected()) {
      unsigned long current_millis = millis();

      // check characteristic status every 200ms
      if (current_millis - previous_millis >= 200) {
        previous_millis = current_millis;

        int pot_value = analogRead(A1);
        potentiometerReading.writeValue(pot_value);
        Serial.println(pot_value);
      }
    }

    // when the central disconnects, turn off the built-in LED
    digitalWrite(LED_BUILTIN, LOW);
    Serial.print("Disconnected from central: ");
    Serial.println(central_address);
  }
}
