/**
   Device to Device Communication LED Test - Central

   Upload this sketch on a Bluetooth® Low Energy compatible board.
   Most MKR family boards and most Nano boards are BLE compatible.

   Wiring Diagram: built-in LED required (nothing attached)

   Tutorial: https://docs.arduino.cc/tutorials/nano-rp2040-connect/rp2040-ble-device-to-device
*/

#include <ArduinoBLE.h> // https://www.arduino.cc/reference/en/libraries/arduinoble/

void setup() {  
  // initialize serial communication and wait for port to open
  Serial.begin(9600);
  // wait for serial port to connect. Needed for native USB port only
  while (!Serial);

  pinMode(LED_BUILTIN, OUTPUT);
  
  // initialize the BLE hardware
  BLE.begin();
  Serial.println("BLE Central - LED control");
  
  // start scanning for Button Device BLE peripherals
  BLE.scanForUuid("19b10000-e8f2-537e-4f6c-d104768a1214");
}

void loop() {
  // check if a peripheral has been discovered
  BLEDevice peripheral = BLE.available();
  
  if (peripheral) {
    // discovered a peripheral, print out address, local name, and advertised service
    Serial.print("Found ");
    Serial.print(peripheral.address());
    Serial.print(" '");
    Serial.print(peripheral.localName());
    Serial.print("' ");
    Serial.print(peripheral.advertisedServiceUuid());
    Serial.println();
    
    if (peripheral.localName().indexOf("Button Device") < 0) {
      Serial.println("No 'Button Device' in name");
      return; // if the name doesn't have "Button Device" in it then ignore it
    }
    
    // stop scanning
    BLE.stopScan();
    controlLed(peripheral);
    
    // peripheral disconnected, start scanning again
    BLE.scanForUuid("19b10000-e8f2-537e-4f6c-d104768a1214");
  }
}

void controlLed(BLEDevice peripheral) {
  // connect to the peripheral
  Serial.println("Connecting ...");
  
  if (peripheral.connect()) {
    Serial.println("Connected");
  } else {
    Serial.println("Failed to connect!");
    return;
  }
  
  // discover peripheral attributes
  Serial.println("Discovering attributes ...");
  
  if (peripheral.discoverAttributes()) {
    Serial.println("Attributes discovered");
  } else {
    Serial.println("Attribute discovery failed!");
    peripheral.disconnect();
    return;
  }
  
  // retrieve the LED characteristic
  BLECharacteristic LEDCharacteristic = peripheral.characteristic("19b10001-e8f2-537e-4f6c-d104768a1214");
  
  if (!LEDCharacteristic) {
    Serial.println("Peripheral does not have LED characteristic!");
    peripheral.disconnect();
    return;
  }

  // while the peripheral is connected
  while (peripheral.connected()) {
    if (LEDCharacteristic.canRead()) {
      byte value = LEDCharacteristic.read();
      LEDCharacteristic.readValue(value);
      //Serial.println(LEDCharacteristic.readValue(value));
      
      if (value == 0x01) {
        digitalWrite(LED_BUILTIN, HIGH);
        Serial.println("ON");
      }
      else if (value == 0x00) {
        digitalWrite(LED_BUILTIN, LOW);
        Serial.println("OFF");
      }
    }

    delay(50);
  }
  
  Serial.println("Peripheral disconnected");
}
