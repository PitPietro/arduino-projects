/**
   Device to Device Communication LED Test - Peripheral

   Upload this sketch on a Bluetooth® Low Energy compatible board.
   Most MKR family boards and most Nano boards are BLE compatible.

   Wiring Diagram:
   - pushbutton attached to pin 2 (option 10k Ohm resistor)

   Tutorial: https://docs.arduino.cc/tutorials/nano-rp2040-connect/rp2040-ble-device-to-device
*/

#include <ArduinoBLE.h> // https://www.arduino.cc/reference/en/libraries/arduinoble/

char button_pin = 6;

// BLE LED Service
BLEService LEDService("19B10000-E8F2-537E-4F6C-D104768A1214");

// BLE LED Switch Characteristic - custom 128-bit UUID, read and writable by central
BLEByteCharacteristic LEDCharacteristic("19B10001-E8F2-537E-4F6C-D104768A1214", BLERead | BLENotify | BLEWrite);

void setup() {
  // initialize serial communication and wait for port to open
  Serial.begin(9600);
  // wait for serial port to connect. Needed for native USB port only
  while (!Serial);

  pinMode(button_pin, INPUT);

  // begin initialization

  if (!BLE.begin()) {
    Serial.println("Starting Bluetooth® Low Energy failed!");
  }

  // set advertised local name and service UUID:
  BLE.setLocalName("Button Device");
  BLE.setAdvertisedService(LEDService);

  // add the characteristic to the service
  LEDService.addCharacteristic(LEDCharacteristic);

  // add service
  BLE.addService(LEDService);

  // start advertising
  BLE.advertise();

  Serial.println("BLE LED Peripheral, waiting for connections....");
}

void loop() {
  // listen for BLE peripherals to connect
  BLEDevice central = BLE.central();

  // if a central is connected to peripheral
  if (central) {
    Serial.print("Connected to central: ");

    // print the central's MAC address
    Serial.println(central.address());

    // while the central is still connected to peripheral
    while (central.connected()) {
      bool button_state = digitalRead(button_pin);
      
      if (button_state == HIGH) {
        LEDCharacteristic.writeValue((byte)0x01);
        Serial.println("ON");
      } else if (button_state == LOW) {
        LEDCharacteristic.writeValue((byte)0x00);
        Serial.println("OFF");
      }
      
      delay(50);
    }

    /*
      if (buttonState == LOW) {
      led_switch = !led_switch;

      if (led_switch) {
        LEDCharacteristic.writeValue((byte)0x01);
        Serial.println("ON");
      } else {
        LEDCharacteristic.writeValue((byte)0x00);
        Serial.println("OFF");
      }
      }
    */

    // when the central disconnects, print it out
    Serial.print(F("Disconnected from central: "));
    Serial.println(central.address());
  }
}
