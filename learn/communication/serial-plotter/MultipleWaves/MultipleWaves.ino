/*
   MultipleWaves
   Click on 'Tools' -> 'Serial Plotter' to take a look at the waves being plotted.
   Click on 'Tools' -> 'Serial Monitor' to take a look at the waves' values being printed.
   
   Tutorial: https://arduinogetstarted.com/tutorials/arduino-serial-plotter
*/

void setup() {
  Serial.begin(9600);
  // wait for serial port to connect. Needed for native USB port only.
  while (!Serial)
    ;
}

void loop() {
  for (int i = 0; i < 360; i += 5) {
    // convert deg in radians
    // i * pi / 180

    float sin_wave = sin(i * M_PI / 180);
    float cosin_wave = cos(i * M_PI / 180);
    float custom_wave = 5 * sin((i + 180)* M_PI / 180);

    Serial.print(sin_wave);
    // a space ' ' or  tab '\t' character is printed between the two values
    Serial.print("\t");
    Serial.print(cosin_wave);
    // a space ' ' or  tab '\t' character is printed between the two values
    Serial.print("\t");
    // the last value is followed by a carriage return and a newline characters
    Serial.println(custom_wave);

    delay(100);
  }
}
