/*
   MultipleInputsWaves
   Click on 'Tools' -> 'Serial Plotter' to take a look at the waves being plotted.
   Click on 'Tools' -> 'Serial Monitor' to take a look at the waves' values being printed.

   Since analog values goes from 0 to 1023 while digital values goes from 0 to 1, it is very hard
   to plot them togheter.
*/

// digital inputs
const short int pot_pin = A0;
const short int photores_pin = A1;

// initialize the values
int pot_value = 0;
int photores_value = 0;

void setup() {
  Serial.begin(9600);
  // wait for serial port to connect. Needed for native USB port only.
  while (!Serial)
    ;
}

void loop() {
  // read the values from the inputs
  pot_value = analogRead(pot_pin);
  photores_value = analogRead(photores_pin);

  Serial.print(pot_value);
  // a space ' ' or  tab '\t' character is printed between the two values
  Serial.print("\t");
  // the last value is followed by a carriage return and a newline characters
  Serial.println(photores_value);
  
  delay(100);
}
