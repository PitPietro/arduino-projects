/*
   DigitalInputsWaves
   Click on 'Tools' -> 'Serial Plotter' to take a look at the waves being plotted.
   Click on 'Tools' -> 'Serial Monitor' to take a look at the waves' values being printed.

   Since analog values goes from 0 to 1023 while digital values goes from 0 to 1, it is very hard
   to plot them togheter.
*/
// analog inputs
const short int button_pin = 8;
const short int button_pullup_pin = 9;

// initialize the values
int button_value = 0;
int button_pullup_value = 0;

void setup() {
  Serial.begin(9600);
  // wait for serial port to connect. Needed for native USB port only.
  while (!Serial)
    ;

  pinMode(button_pin, INPUT);
  pinMode(button_pullup_pin, INPUT_PULLUP);
}

void loop() {
  // read the values from the inputs
  button_value = digitalRead(button_pin);
  button_pullup_value = digitalRead(button_pullup_pin);

  // a space ' ' or  tab '\t' character is printed between the two values
  Serial.print(button_value);
  Serial.print("\t");
  // the last value is followed by a carriage return and a newline characters
  Serial.println(button_pullup_value);

  delay(100);
}
