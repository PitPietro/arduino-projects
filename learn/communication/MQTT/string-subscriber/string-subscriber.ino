/*
   MQTT String Subscriber

   Run this code sketch on any WiFi compatible board.
   Most MKR family boards and most Nano boards are compatible.

   Press Ctrl + Shift + N and create 'arduino-secrets.h' file as follow:

   ```c
   #define WIFI_SSID "" // place your WiFi name inside the quotes
   #define WIFI_PWD "" // place your WiFi password inside the quotes
   #define BROKER_IP "192.168.X.X" // place MQTT Broker IP address inside the quotes
   #define BROKER_PORT XXXX // place MQTT Broker PORT without quotes (should be 1883 by default)
   #define DEV_NAME  "" // place MQTT Client ID name inside the quotes
   #define MQTT_USER "" // place MQTT username inside the quotes (leave empty if MQTT is unauthenticated)
   #define MQTT_PWD  "" // place MQTT password inside the quotes (leave empty if MQTT is unauthenticated)
   ```

   Note: 'arduino-secrets.h' files are indicated in '.gitignore' to avoid loose of sensitive data.

   On the parent directory has been placed a template file called 'arduino-secrets.txt'.
   Copy that file in the same directory of this Sketch and change its extension to '.h'.

   To test this Sketch follow my blog post about MQTT protocol:
   https://pietropoluzzi.it/blog/iot/mqtt-protocol/

   ```shell
   mosquitto_pub -t Hello -m "Message to send" -u "MQTT_USERNAME" -P "MQTT_PPASSWORD"
   ```
*/
#include <WiFiNINA.h> // https://www.arduino.cc/en/Reference/WiFiNINA
#include <MQTT.h>     // https://www.arduino.cc/reference/en/libraries/mqtt/
#include "arduino-secrets.h"

MQTTClient mqtt_client;
WiFiClient wifi_client;

unsigned long last_millis = 0;
const short int delay_time = 2000;
char* mqtt_topic = "Hello";

// Wifi radio's status
int status = WL_IDLE_STATUS;

void setup() {
  // initialize serial communication and wait for port to open
  Serial.begin(9600);
  // wait for serial port to connect. Needed for native USB port only
  while (!Serial);

  // check for the WiFi module
  if (WiFi.status() == WL_NO_MODULE) {
    Serial.println("Communication with WiFi module failed!");
    while (true); // do not continue
  }

  // attempt to connect to Wifi network
  while (status != WL_CONNECTED) {
    Serial.print("Attempting to connect to network: ");
    Serial.println(WIFI_SSID);

    // connect to WPA/WPA2 network
    status = WiFi.begin(WIFI_SSID, WIFI_PWD);

    // wait for connection
    delay(500);
  }

  // the board is now connected, print out the data
  Serial.println("You're connected to the network");
  print_wifi_status();

  // MQTT brokers usually use port 8883 for secure connections
  mqtt_client.begin(BROKER_IP, BROKER_PORT, wifi_client);
  mqtt_client.onMessage(message_handler);
  connect();
}

void loop() {
  mqtt_client.loop();
  if (!mqtt_client.connected()) {
    // connect to the MQTT broker and subscribe to the given topic
    connect();
  }
}

void connect() {
  Serial.print("Checking WiFi Status");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print("#");
    delay(500);
  }
  Serial.print("\nConnecting to MQTT queue");

  // try to connect to MQTT queue every second
  while (!mqtt_client.connect(DEV_NAME, MQTT_USER, MQTT_PWD)) {
    Serial.print("#");
    delay(500);
  }

  char msg[30];
  sprintf(msg, "\nConnected to %s", BROKER_IP);
  Serial.println(msg);


  // subscribe to the given topic
  mqtt_client.subscribe(mqtt_topic);
}

void message_handler(String &topic, String &payload) {
  Serial.println("# MQTT message_handler: topic = " + topic + "\t payload = " + payload);
}

void print_wifi_status() {
  Serial.println("Board Information:");

  // print the board's IP address
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  Serial.println();
  Serial.println("Network Information:");
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print the received signal strength
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.println(rssi);

  byte encryption = WiFi.encryptionType();
  Serial.print("Encryption Type:");
  Serial.println(encryption, HEX);
  Serial.println();
}
