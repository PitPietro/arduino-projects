#include "Keyboard.h"

// set pin numbers for the buttons
const char pin_d7 = 7;
const char pin_d6 = 6;
const char pin_d5 = 5;
const char pin_d4 = 4;
const char pin_d3 = 3;
const char pin_d2 = 2;

char d2_key = KEY_F14;

/*
Keyboard Modifiers are: https://www.arduino.cc/reference/en/language/functions/usb/keyboard/keyboardmodifiers/
*/
void setup() {
  // initialize inputs
  pinMode(pin_d7, INPUT);
  pinMode(pin_d6, INPUT);
  pinMode(pin_d5, INPUT);
  pinMode(pin_d4, INPUT);
  pinMode(pin_d3, INPUT);
  pinMode(pin_d2, INPUT);

  Serial.begin(9600);

  // initialize keyboard control
  Keyboard.begin();
}

void loop() {
  // use the push buttons to control the keyboard modifiers
  if (digitalRead(pin_d2) == HIGH) {
    Keyboard.press(d2_key);
    
    Serial.print("key pressed: ");
    Serial.println(d2_key);
  } else {
    Keyboard.release(d2_key);
  }

  delay(100);

}
