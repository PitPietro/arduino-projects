#include "Keyboard.h"
#define NUM 3 // notes in a chord

// pin numbers for the buttons
const char pin_d9 = 9;
const char pin_d8 = 8;
const char pin_d7 = 7;
const char pin_d6 = 6;
const char pin_d5 = 5;
const char pin_d4 = 4;
const char pin_d3 = 3;
const char pin_d2 = 2;

const char key_e3   = 'p'; // E3
const char key_d3_d = '0'; // D#3
const char key_d3   = 'o'; // D3
const char key_c3_d = '9'; // C#3
const char key_c3   = 'i'; // C3

const char key_b2   = 'u'; // B2
const char key_a2_d = '7'; // A#2
const char key_a2   = 'y'; // A2
const char key_g2_d = '6'; // G#2
const char key_g2   = 't'; // G2
const char key_f2_d = '5'; // F#2
const char key_f2   = 'r'; // F2
const char key_e2   = 'e'; // E2
const char key_d2_d = '3'; // D#2
const char key_d2   = 'w'; // D2
const char key_c2_d = '2'; // C#2
const char key_c2   = 'q'; // C2

const char key_b1   = 'm'; // B1
const char key_a1_d = 'j'; // A#1
const char key_a1   = 'n'; // A1
const char key_g1_d = 'h'; // G#1
const char key_g1   = 'b'; // G1
const char key_f1_d = 'g'; // F#1
const char key_f1   = 'v'; // F1
const char key_e1   = 'c'; // E1
const char key_d1_d = 'd'; // D#1
const char key_d1   = 'x'; // D1
const char key_c1_d = 's'; // C#1
const char key_c1   = 'z'; // C1

/*
  Keyboard control for Virtual MIDI Piano

  The comments assume: Base Octave = 1, Transpose = 0.

  Virtual MIDI Piano Keyboard: https://vmpk.sourceforge.io/
  Links: https://www.arduino.cc/reference/en/language/functions/usb/keyboard/keyboardmodifiers/
*/
void setup() {
  // initialize inputs
  pinMode(pin_d9, INPUT);
  pinMode(pin_d8, INPUT);
  pinMode(pin_d7, INPUT);
  pinMode(pin_d6, INPUT);
  pinMode(pin_d5, INPUT);
  pinMode(pin_d4, INPUT);
  pinMode(pin_d3, INPUT);
  pinMode(pin_d2, INPUT);

  Serial.begin(9600);

  // initialize keyboard control
  Keyboard.begin();

  //  delay(10000);
}

/*
   use the push buttons to control the keyboard modifiers
*/
void loop() {
  char test[3] = {key_c1, key_c1_d, key_d1};
  const short int custom_delay = 3840;

  char b_chord[NUM] = {key_b1, key_d2_d, key_f2_d};
  char f_diesis_chord[NUM] = {key_f1_d, key_a1_d, key_c2_d};
  char g_diesis_minor_chord[NUM] = {key_g1_d, key_b1, key_d2_d};
  char e_chord[NUM] = {key_e1, key_g1_d, key_b1};

  while (digitalRead(pin_d9) == HIGH) {
    while (digitalRead(pin_d2) == HIGH) {
      press_letters(b_chord, NUM);
    }

    while (digitalRead(pin_d3) == HIGH) {
      press_letters(f_diesis_chord, NUM);
    }

    while (digitalRead(pin_d4) == HIGH) {
      press_letters(g_diesis_minor_chord, NUM);
    }

    while (digitalRead(pin_d5) == HIGH) {
      press_letters(e_chord, NUM);
    }

    Keyboard.releaseAll();
  }

  // delay(10);



  //  while (digitalRead(pin_d2) == HIGH) {
  //    press_letters(b_chord, NUM);
  //    delay(custom_delay);
  //    release_letters(b_chord, NUM);
  //
  //    press_letters(f_diesis_chord, NUM);
  //    delay(custom_delay);
  //    release_letters(f_diesis_chord, NUM);
  //
  //    press_letters(g_diesis_minor_chord, NUM);
  //    delay(custom_delay);
  //    release_letters(g_diesis_minor_chord, NUM);
  //
  //    press_letters(e_chord, NUM);
  //    delay(custom_delay);
  //    release_letters(e_chord, NUM);
  //  }

}

void press_letters(char notes[], int dimension) {
  for (int i = 0; i < dimension; i++) {
    Keyboard.press(notes[i]);
    // Serial.print("press: ");
    // Serial.println(notes[i]);
  }
}

void release_letters(char notes[], int dimension) {
  for (int i = 0; i < dimension; i++) {
    Keyboard.release(notes[i]);
    // Serial.print("release: ");
    // Serial.println(notes[i]);
  }
}
