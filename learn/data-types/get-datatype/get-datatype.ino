#define MY_DEFINED_CHAR 5

// generic catch-all implementation.
template <typename T_ty> struct TypeInfo {
  static const char * name;
};
template <typename T_ty> const char * TypeInfo<T_ty>::name = "unknown";

// Handy macro to make querying stuff easier.
#define TYPE_NAME(var) TypeInfo< typeof(var) >::name

// Handy macro to make defining stuff easier.
#define MAKE_TYPE_INFO(type)  template <> const char * TypeInfo<type>::name = #type;

// type-specific implementations
MAKE_TYPE_INFO(char)
MAKE_TYPE_INFO(int)
MAKE_TYPE_INFO(short)
MAKE_TYPE_INFO(long)
MAKE_TYPE_INFO(float)
MAKE_TYPE_INFO(double)

/**
   Get the data type of a variable
   Docs: https://arduino.stackexchange.com/questions/3079/how-to-retrieve-the-data-type-of-a-variable
*/
void setup() {
  // initialize serial communication and wait for port to open
  Serial.begin(9600);
  // do not wait for serial port to connect. Needed for native USB port only
  while (!Serial);

  char my_char = 'a';
  short int my_short = 123;
  long int my_long = 123;
  double my_double = 1.23;
  
  Serial.println(TYPE_NAME(my_double));
  // Serial.println(TYPE_NAME(MY_DEFINED_CHAR));
}

void loop() {
}
