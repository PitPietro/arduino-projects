/*
   Node-RED - MQTT Hello World

   Please read the README.md file placed in the same directory of this file.

   Open Node-RED and import flows.json file.

   Connect the Arduino MKR to a servo motor using digital pin 02.

   Press Ctrl + Shift + N and create 'arduino-secrets.h' file as follow:

   ```c
   #define WIFI_SSID "" // place your WiFi name inside the quotes
   #define WIFI_PWD "" // place your WiFi password inside the quotes
   #define BROKER_IP "192.168.x.x" // place MQTT Broker IP address inside the quotes
   #define DEV_NAME  "" // place MQTT Client ID name inside the quotes
   #define MQTT_USER "" // place MQTT username inside the quotes (leave empty if MQTT is unauthenticated)
   #define MQTT_PWD  "" // place MQTT password inside the quotes (leave empty if MQTT is unauthenticated)
   ```

   Note: 'arduino-secrets.h' files are indicated in '.gitignore' to avoid loose of sensitive data.
*/
#include <WiFiNINA.h> // https://www.arduino.cc/en/Reference/WiFiNINA
#include <Servo.h>    // https://www.arduino.cc/reference/en/libraries/servo/
#include <MQTT.h>     // https://www.arduino.cc/reference/en/libraries/mqtt/
#include "arduino-secrets.h"

#define CLOSE 0
#define OPEN 180

MQTTClient mqtt_client;
WiFiClient wifi_client;
Servo door_servo;

// check is have been past more than 'delay_time' milliseconds
unsigned long last_millis = 0;

// MQTT broker port is 1883 by default, but you can change it
const short int broker_port = 1883;

// send "world" message to /hello topic every 'delay_time' milliseconds
const short int delay_time = 2000;

// pin of the servo motor that opens/closes the door
const char servo_pin = 2;

// set to false to avoid sending "world" message to /hello topic every 'delay_time' milliseconds
const bool is_publisher = true;

// Wifi radio's status
int status = WL_IDLE_STATUS;

void setup() {
  // initialize serial communication and wait for port to open
  Serial.begin(9600);
  // wait for serial port to connect. Needed for native USB port only
  while (!Serial);

  // check for the WiFi module
  if (WiFi.status() == WL_NO_MODULE) {
    Serial.println("Communication with WiFi module failed!");
    while (true); // do not continue
  }

  // attempt to connect to Wifi network
  while (status != WL_CONNECTED) {
    Serial.print("Attempting to connect to network: ");
    Serial.println(WIFI_SSID);

    // connect to WPA/WPA2 network
    status = WiFi.begin(WIFI_SSID, WIFI_PWD);

    // wait for connection
    delay(5000);
  }

  // the board is now connected, print out the data
  Serial.println("You're connected to the network");
  print_wifi_status();

  // MQTT brokers usually use port 8883 for secure connections
  mqtt_client.begin(BROKER_IP, broker_port, wifi_client);
  mqtt_client.onMessage(message_handler);
  connect();

  // set servo_pin an output
  door_servo.attach(servo_pin);
}

void loop() {
  mqtt_client.loop();
  if (!mqtt_client.connected()) {
    connect();
  }

  // publish a message every 'delay_time' milliseconds if is_publisher is set to 'true'
  if (is_publisher && millis() - last_millis > delay_time) {
    last_millis = millis();
    mqtt_client.publish("/hello", "world"); //publish to topic /hello the message
  }
}

void connect() {
  Serial.print("Checking WiFi Status");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print("#");
    delay(1000);
  }
  Serial.print("\nConnecting to MQTT queue");

  // try to connect to MQTT queue every second
  while (!mqtt_client.connect(DEV_NAME, MQTT_USER, MQTT_PWD)) {
    Serial.print("#");
    delay(1000);
  }

  char msg[30];
  sprintf(msg, "\nConnected to %s", BROKER_IP);
  Serial.println(msg);


  // subscribe to topic /hello
  mqtt_client.subscribe("/hello");
}

void message_handler(String &topic, String &payload) {
  Serial.println("incoming: " + topic + " - " + payload);
  if (topic == "/hello") {
    if (payload == "open") {
      Serial.println("open");
      digitalWrite(LED_BUILTIN, HIGH);
      door_servo.write(OPEN);
    } else if (payload == "close") {
      Serial.println("close");
      digitalWrite(LED_BUILTIN, LOW);
      door_servo.write(CLOSE);
    }
  }
}

void print_wifi_status() {
  Serial.println("Board Information:");

  // print the board's IP address
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  Serial.println();
  Serial.println("Network Information:");
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print the received signal strength
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.println(rssi);

  byte encryption = WiFi.encryptionType();
  Serial.print("Encryption Type:");
  Serial.println(encryption, HEX);
  Serial.println();
}
