#!/bin/bash

if test $# -ne 1; then
	echo "Usage create-secrets.sh <directory-name>"
	exit 1
fi

dir="$1"
file=$dir"arduino-secrets.h"

echo '#define WIFI_SSID "" // place your WiFi name inside the quotes' > "$file"
echo '#define WIFI_PWD  "" // place your WiFi password inside the quotes' >> "$file"
echo '#define BROKER_IP "" // place MQTT Broker IP address inside the quotes' >> "$file"
echo '#define DEV_NAME  "" // place MQTT Client ID name inside the quotes' >> "$file"
echo '#define MQTT_USER "" // place MQTT username inside the quotes (leave empty if MQTT is unauthenticated)' >> "$file"
echo '#define MQTT_PWD  "" // place MQTT password inside the quotes (leave empty if MQTT is unauthenticated)' >> "$file"