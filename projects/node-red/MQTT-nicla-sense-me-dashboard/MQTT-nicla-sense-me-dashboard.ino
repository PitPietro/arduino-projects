/*
   Node-RED - MQTT Nicla Sense ME Dashboard

   Please read the README.md file placed in the same directory of this file.

   Open Node-RED and import flows.json file.

   Press Ctrl + Shift + N and create 'arduino-secrets.h' file as follow:

   ```c
   #define WIFI_SSID "" // place your WiFi name inside the quotes
   #define WIFI_PWD "" // place your WiFi password inside the quotes
   #define BROKER_IP "192.168.x.x" // place MQTT Broker IP address inside the quotes
   #define DEV_NAME  "" // place MQTT Client ID name inside the quotes
   #define MQTT_USER "" // place MQTT username inside the quotes (leave empty if MQTT is unauthenticated)
   #define MQTT_PWD  "" // place MQTT password inside the quotes (leave empty if MQTT is unauthenticated)
   ```

   Note: 'arduino-secrets.h' files are indicated in '.gitignore' to avoid loose of sensitive data.

   BSEC stands for Bosch Sensortec Environmental Cluster.
   SensorBSEC class provides the following parameters:
   - iaq(): IAQ value for regular use case (unsigned 16 bit)
   - iaq_s(): IAQ value for stationary use cases (unsigned 16 bit)
   - b_voc_eq(): breath VOC equivalent in ppm (float)
   - co2_eq(): CO2 equivalent (ppm) [400,] (unsigned 32 bit)
   - comp_t(): compensated temperature in Celsius (float)
   - comp_h(): compensated humidity (float)
   - comp_g(): compensated gas resistance in Ohms (unsigned 32 bit)
   - accuracy(): accuracy level: [0-3] (unsigned 8 bit)
*/

// need to install ArduinoBLE.h https://reference.arduino.cc/reference/en/libraries/arduinoble/
#include <ArduinoJson.h> // https://arduinojson.org/
#include <WiFiNINA.h>    // https://www.arduino.cc/en/Reference/WiFiNINA
#include <MQTT.h>        // https://www.arduino.cc/reference/en/libraries/mqtt/

#include "Arduino_BHY2Host.h" // https://reference.arduino.cc/reference/en/libraries/arduino_bhy2host/
#include "arduino-secrets.h"

MQTTClient mqtt_client;
WiFiClient wifi_client;

/* MQTT topics named with '_t' suffix  */
// inputs
// const char pres_t[10] = "/pressure";

// SensorXYZ source: https://github.com/arduino-libraries/Arduino_BHY2Host/blob/main/src/sensors/SensorXYZ.h
SensorXYZ acc(SENSOR_ID_ACC);
SensorXYZ gyro(SENSOR_ID_GYRO);
SensorXYZ mag(SENSOR_ID_MAG);
SensorBSEC bsec(SENSOR_ID_BSEC);

// MQTT broker port is 1883 by default, but you can change it
const short int broker_port = 1883;

// Wifi radio's status
int status = WL_IDLE_STATUS;

void setup() {
  // initialize serial communication and wait for port to open
  Serial.begin(9600);
  // do not wait for serial port to connect. Needed for native USB port only
  // while (!Serial);

  // check for the WiFi module
  if (WiFi.status() == WL_NO_MODULE) {
    Serial.println("Communication with WiFi module failed!");
    while (true); // do not continue
  }

  // attempt to connect to Wifi network
  while (status != WL_CONNECTED) {
    Serial.print("Attempting to connect to network: ");
    Serial.println(WIFI_SSID);

    // connect to WPA/WPA2 network
    status = WiFi.begin(WIFI_SSID, WIFI_PWD);

    // wait for connection
    delay(5000);
  }

  // the board is now connected, print out the data
  Serial.println("You're connected to the network");
  print_wifi_status();

  // configure BHY host connection
  BHY2Host.begin(false, NICLA_VIA_ESLOV);
  acc.begin();
  gyro.begin();
  mag.begin();
  bsec.begin();

  // MQTT brokers usually use port 8883 for secure connections
  mqtt_client.begin(BROKER_IP, broker_port, wifi_client);
  mqtt_client.onMessage(message_handler);
  connect();
}

void loop() {
  mqtt_client.loop();
  static auto lastCheck = millis();

  if (!mqtt_client.connected()) {
    connect();
  }

  // update function should be continuously polled
  Serial.println("~~~~ BHY2Host update() ~~~~");
  BHY2Host.update();

  // check sensor values every second
  if (millis() - lastCheck >= 500) {
    lastCheck = millis();

    boolean rc;

    // int acc_val[3] = {acc.x(), acc.y(), acc.z()};
    // int gyro_val[3] = {gyro.x(), gyro.y(), gyro.z()};

    int values[9] = {acc.x(), acc.y(), acc.z(), gyro.x(), gyro.y(), gyro.z(), mag.x(), mag.y(), mag.z()};


    // passing the whole SensorXYZ to the function caused issues: the values were not updating
    send_xyz_json_sensors_to_mqtt(values, "xyz");

    send_bsec_to_mqtt(bsec.iaq(), bsec.b_voc_eq(), bsec.co2_eq(), bsec.comp_t(), bsec.comp_h(), bsec.comp_g(), bsec.accuracy());

    // start send JSON function
    /*   
        StaticJsonDocument<256> acc_doc;
        acc_doc["x"] = acc.x();
        acc_doc["y"] = acc.y();
        acc_doc["z"] = acc.z();

        // generate the minified JSON
        char acc_out[128];
        int bytes_dim = serializeJson(acc_doc, acc_out);

        Serial.print("bytes = ");
        Serial.println(bytes_dim, DEC); // JSON's bytes weight

        // returns 1 if the MQTT client publish the messagge without errors
        boolean rc = mqtt_client.publish("/acc", acc_out);

        Serial.print("rc = ");
        Serial.println(rc);
    */
    // end send JSON function

    // perform operations
    // mqtt_client.publish("/hello", "world"); // publish to topic /hello the message
  }
}

boolean send_xyz_json_sensors_to_mqtt(int* values, char* topic) {
  StaticJsonDocument<256> doc;

  doc["acc_x"] = values[0];
  doc["acc_y"] = values[1];
  doc["acc_z"] = values[2];

  doc["gyro_x"] = values[3];
  doc["gyro_y"] = values[4];
  doc["gyro_z"] = values[5];

  doc["mag_x"] = values[6];
  doc["mag_y"] = values[7];
  doc["mag_z"] = values[8];

  // generate the minified JSON
  char out[128];
  int bytes_dim = serializeJson(doc, out);

  Serial.print("bytes = ");
  Serial.println(bytes_dim, DEC); // JSON's bytes weight

  // returns 1 if the MQTT client publish the messagge without errors
  return mqtt_client.publish(topic, out);

  // Serial.print("rc = ");
  // Serial.println(rc);
}

boolean send_bsec_to_mqtt(int iaq, float b_voc, uint32_t co2, float temp, float hum, uint32_t gas_res, uint8_t acc) {
  StaticJsonDocument<256> doc;

  doc["iaq"] = iaq;
  doc["b_voc"] = b_voc;
  doc["co2"] = co2;
  doc["temp"] = temp;
  doc["hum"] = hum;
  doc["gas_res"] = gas_res;
  doc["acc"] = acc;
  
  // generate the minified JSON
  char out[128];
  int bytes_dim = serializeJson(doc, out);

  Serial.print("bytes = ");
  Serial.println(bytes_dim, DEC); // JSON's bytes weight

  // returns 1 if the MQTT client publish the messagge without errors
  return mqtt_client.publish("bsec", out);
}


void connect() {
  static auto lastCheck = millis();

  Serial.print("Checking WiFi Status");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print("#");
    delay(1000);
  }
  Serial.print("\nConnecting to MQTT queue");

  // try to connect to MQTT queue every second
  while (!mqtt_client.connect(DEV_NAME, MQTT_USER, MQTT_PWD) && (millis() - lastCheck >= 1000)) {
    Serial.print("#");
    delay(1000);
  }

  char msg[30];
  sprintf(msg, "\nConnected to %s", BROKER_IP);
  Serial.println(msg);


  // subscribe to topic /hello
  // mqtt_client.subscribe("/hello");
}

void message_handler(String &topic, String &payload) {
  Serial.println("incoming: " + topic + " - " + payload);
  //  if (topic == "/hello") {
  //    if (payload == "open") {
  //      Serial.println("open");
  //      digitalWrite(LED_BUILTIN, HIGH);
  //      door_servo.write(OPEN);
  //    } else if (payload == "close") {
  //      Serial.println("close");
  //      digitalWrite(LED_BUILTIN, LOW);
  //      door_servo.write(CLOSE);
  //    }
  //  }
}

void print_wifi_status() {
  Serial.println("Board Information:");

  // print the board's IP address
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  Serial.println();
  Serial.println("Network Information:");
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print the received signal strength
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.println(rssi);

  byte encryption = WiFi.encryptionType();
  Serial.print("Encryption Type:");
  Serial.println(encryption, HEX);
  Serial.println();
}
