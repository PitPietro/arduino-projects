#!/usr/bin/python3

import paho.mqtt.client as mqtt
import random # https://pynative.com/python-get-random-float-numbers/
import json

# This is the Publisher

client = mqtt.Client()
client.connect("localhost", 1883, 60)
MQTT_MSG=json.dumps({"x": "6.4", "y": "3.2", "z": "4.5"});
client.publish("/acc", MQTT_MSG);
client.disconnect();
