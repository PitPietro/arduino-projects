# Nicla Sense ME Node-RED Dashboard



## Flow

Describe each component, from the sensors to the cloud.

### Server

The server can be a <u>Raspberry Pi computer</u> during the testing phase. To allow accessing the dashboard from anywhere, you need a VPN pointing on your local network or a small server with a static public IP address.

Let's focus on the "local" setup and configure the Raspberry Pi with a few Docker containers.



https://stackoverflow.com/questions/44687123/how-to-store-mqtt-mosquitto-publish-events-into-mysql

https://www.howtogeek.com/devops/how-to-monitor-mysql-server-activity-with-a-grafana-dashboard/

https://flows.nodered.org/node/node-red-node-mysql



#### Mosquitto

MQTT broker that manage the data flow.

#### Node-RED



#### InfluxDB

#### Grafana

### Bridge

### Nicla Sense

