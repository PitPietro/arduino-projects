# Motion & Environment Dashboard

## Goal

The goal of the project is to get Arduino Nicla Sense ME sensors on the Node Red dashboard and optionally control the on-board LED color.

## Requirements

The required boards are:

1. Arduino Nicla Sense ME
2. Arduino Nano or MKR familify board with WiFi & BLE connectivity
3. Raspberry Pi (optional) where to host **Node-RED**

Software required: **Node-RED** with `dashboard` node.

