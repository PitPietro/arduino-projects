/*
   Node-RED - MQTT IoT Carrier - Remote Alarm

   Please read the README.md file placed in the same directory of this file.

   Open Node-RED and import flows.json file.

   Mount an Arduino board from MKR family on top of MKR IoT Carrier.

   This code allow the MKR to:
   1. Send temperature, pressure and humidity data (to different topics)
   2. Set an alarm from the dashboard and snooze/stop from the carrier

   Real Time Clock (RTC): This code access one Time Server on the internet
   and get from it the correct Time, using the Network Time Protocol (NTP)
   builtin in the used WiFi module. The time is then used to configure the
   internal RTC (UTC format) of the board using the linux epoch format.

   Docs:
   1. Blog post: https://pietropoluzzi.it/blog/iot/arduino-mkr-iot-carrier-cheat-sheet/
   2. Reference: https://www.arduino.cc/reference/en/libraries/arduino_mkriotcarrier/

   Press Ctrl + Shift + N and create 'arduino-secrets.h' file as follow:

   ```c
   #define WIFI_SSID "" // place your WiFi name inside the quotes
   #define WIFI_PWD "" // place your WiFi password inside the quotes
   #define BROKER_IP "192.168.x.x" // place MQTT Broker IP address inside the quotes
   #define DEV_NAME  "" // place MQTT Client ID name inside the quotes
   #define MQTT_USER "" // place MQTT username inside the quotes (leave empty if MQTT is unauthenticated)
   #define MQTT_PWD  "" // place MQTT password inside the quotes (leave empty if MQTT is unauthenticated)
   ```

   Note: 'arduino-secrets.h' files are indicated in '.gitignore' to avoid loose of sensitive data.
*/
#include <Arduino_MKRIoTCarrier.h>
#include <WiFiNINA.h>  // https://www.arduino.cc/en/Reference/WiFiNINA
#include <Servo.h>     // https://www.arduino.cc/reference/en/libraries/servo/
#include <MQTT.h>      // https://www.arduino.cc/reference/en/libraries/mqtt/
#include <utility/wifi_drv.h>
#include "arduino-secrets.h"

MKRIoTCarrier carrier;
MQTTClient mqtt_client;
WiFiClient wifi_client;

// check if have passed more than 'delay_time' milliseconds
unsigned long last_millis = 0;

// MQTT broker port is 1883 by default, but you can change it
const short int broker_port = 1883;

// check /rgbtopic every 'delay_time' milliseconds
const short int delay_time = 2000;

// topics
const char alarm_topic[7] = "/alarm";
const char timezone_topic[10] = "/timezone";
const char temp_topic[13] = "/temperature";
const char hum_topic[10] = "/humidity";

char r = 0, g = 0, b = 0;

float temperature = 0;
float humidity = 0;

int timezone_delay = 0;

// Wifi radio's status
int status = WL_IDLE_STATUS;

int current_time[2];
int alarm_time[2];

void setup() {
  CARRIER_CASE = false;

  // initialize serial communication and wait for port to open
  Serial.begin(9600);
  // do not wait for serial port to connect. Needed for native USB port only
  // while (!Serial);

  // initialize the MKR IoT Carrier and output any errors in the serial monitor
  carrier.begin();
  carrier.display.setRotation(0);

  // check for the WiFi module
  if (WiFi.status() == WL_NO_MODULE) {
    Serial.println("Communication with WiFi module failed!");
    while (true)
      ;  // do not continue
  }

  // attempt to connect to Wifi network
  while (status != WL_CONNECTED) {
    Serial.print("Attempting to connect to network: ");
    Serial.println(WIFI_SSID);

    // connect to WPA/WPA2 network
    status = WiFi.begin(WIFI_SSID, WIFI_PWD);

    // wait for connection
    delay(5000);
  }

  // the board is now connected, print out the data
  Serial.println("You're connected to the network");
  print_wifi_status();

  // MQTT brokers usually use port 8883 for secure connections
  mqtt_client.begin(BROKER_IP, broker_port, wifi_client);
  mqtt_client.onMessage(message_handler);
  connect();
}

void loop() {
  mqtt_client.loop();
  if (!mqtt_client.connected()) {
    connect();
  }

  // publish a message every 'delay_time' milliseconds
  if (millis() - last_millis > delay_time) {
    last_millis = millis();

    // publish temperature, pressure, humidity and light (?)
    // read values from sensors
    temperature = carrier.Env.readTemperature();
    humidity = carrier.Env.readHumidity();

    mqtt_client.publish(temp_topic, (String)temperature);
    mqtt_client.publish(hum_topic, (String)humidity);
  }
}

void connect() {
  Serial.print("Checking WiFi Status");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print("#");
    delay(1000);
  }
  Serial.print("\nConnecting to MQTT queue");

  // try to connect to MQTT queue every second
  while (!mqtt_client.connect(DEV_NAME, MQTT_USER, MQTT_PWD)) {
    Serial.print("#");
    delay(1000);
  }

  char msg[30];
  sprintf(msg, "\nConnected to %s", BROKER_IP);
  Serial.println(msg);


  // subscribe to alarm topic
  mqtt_client.subscribe(alarm_topic);
  mqtt_client.subscribe(timezone_topic);

  // ask for an update about timezone
  mqtt_client.publish(timezone_topic, "ask-timezone");
}

void message_handler(String &topic, String &payload) {
  Serial.println("incoming: " + topic + " - " + payload);
  if (topic == alarm_topic) {
    // TODO Set alarm based on payload

    // Unix time starts on Jan 1 1970
    // get current time from internet in UTC time
    // https://www.arduino.cc/reference/en/libraries/wifinina/wifi.gettime/
    unsigned long epoch = WiFi.getTime();

    // an hour is 86400 equals secs per day
    unsigned short int hours_utc = (epoch % 86400L) / 3600;
    // String str_hours_utc = String(hours_utc);

    // minutes in UTC time saved as int and String
    // a minute is 3600 equals secs per minute
    unsigned short int minutes_utc = (epoch % 3600) / 60;
    String str_minutes_utc = "";

    // in the first 10 minutes of each hour, we'll want a leading '0' (i.e. '9:5:30' becomes '9:05:30')
    if (minutes_utc < 10) {
      str_minutes_utc.concat("0");
    }

    str_minutes_utc.concat(String(minutes_utc));

    // Uncomment the code below to get the seconds from UTC time
    // seconds in UTC time saved as int and String
    // unsigned short int seconds_utc = epoch % 60;
    // String str_seconds_utc = "";
    // in the first 10 seconds of each minute, the monitor need a leading '0' (i.e. '9:5:6' becomes '9:05:06')
    // if (seconds_utc < 10) str_seconds_utc.concat("0");
    // str_seconds_utc.concat(String(seconds_utc));

    // UTC is the time at Greenwich Meridian (GMT)
    // CET is the time at Central European Time, which is UTC + 1
    // CEST is the time at Central European Summer Time, which is UTC + 2
    // and so on: add the timezone "delay" to 'hours_utc' variable

    String str_current_hours = "";

    int current_hours = hours_utc + timezone_delay;

    // check if an overflow timezone_delay
    // if 'current_hours' is not in range from 0 to 23
    if (current_hours < 0) {
      current_hours = 23 - (timezone_delay - hours_utc);
    } else if (current_hours > 23) {
      current_hours = (hours_utc + timezone_delay) - 24;
    }

    if (current_hours < 10) {
      str_current_hours.concat("0");
    }

    str_current_hours.concat(String(current_hours));

    // update time global array
    current_time[0] = current_hours;
    current_time[1] = minutes_utc;

    String time = str_current_hours;
    time.concat(":");
    time.concat(str_minutes_utc);
    // time.concat(":");
    // time.concat(str_seconds_utc);
    if (payload == "ask-time") {
      Serial.print("Time: ");
      Serial.println(time);

      display_config(4);
      print_text(60, 120, time);

      mqtt_client.publish(alarm_topic, time);
    } else {
      Serial.print("Alarm: ");
      Serial.println(payload.hours);

      //alarm_time = {payload["hours"] + timezone_delay, payload["minutes"]};
    }
  } else if (topic == timezone_topic) {
    timezone_delay = payload.toInt();
  }
}

void display_config(unsigned char text_size) {
  carrier.display.fillScreen(ST77XX_WHITE);
  carrier.display.setTextColor(ST77XX_BLACK);
  carrier.display.setRotation(0);
  carrier.display.setTextSize(text_size);
}

void print_text(unsigned char x, unsigned char y, String message) {
  carrier.display.setCursor(x, y);
  carrier.display.print(message);
}

void print_wifi_status() {
  Serial.println("Board Information:");

  // print the board's IP address
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  Serial.println();
  Serial.println("Network Information:");
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print the received signal strength
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.println(rssi);

  byte encryption = WiFi.encryptionType();
  Serial.print("Encryption Type:");
  Serial.println(encryption, HEX);
  Serial.println();
}

void rainbow_loop() {
  short int wait_time = 500;
  unsigned long rainbow_last_millis = 0;

  // red
  r = 255;
  g = 0;
  b = 0;
  lights_on();
  delay(wait_time);

  // yellow
  r = 240;
  g = 255;
  b = 0;
  lights_on();
  delay(wait_time);

  // green
  r = 15;
  g = 255;
  b = 0;
  lights_on();
  delay(wait_time);

  // indaco
  r = 0;
  g = 70;
  b = 206;
  lights_on();
  delay(wait_time);

  // violette
  r = 153;
  g = 0;
  b = 201;
  lights_on();
  delay(wait_time);
}

void change_color() {
  // TODO colors could fade
}

void lights_on() {
  carrier.leds.setPixelColor(0, g, r, b);
  carrier.leds.setPixelColor(1, g, r, b);
  carrier.leds.setPixelColor(2, g, r, b);
  carrier.leds.setPixelColor(3, g, r, b);
  carrier.leds.setPixelColor(4, g, r, b);
  carrier.leds.show();
}



void lights_off() {
  carrier.leds.setPixelColor(0, 0, 0, 0);
  carrier.leds.setPixelColor(1, 0, 0, 0);
  carrier.leds.setPixelColor(2, 0, 0, 0);
  carrier.leds.setPixelColor(3, 0, 0, 0);
  carrier.leds.setPixelColor(4, 0, 0, 0);
  carrier.leds.show();
}