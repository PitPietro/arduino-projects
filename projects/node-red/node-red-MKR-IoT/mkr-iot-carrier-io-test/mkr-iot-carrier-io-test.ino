/*
   Node-RED - MQTT IoT Carrier - I/O Test

   Please read the README.md file placed in the same directory of this file.

   Open Node-RED and import flows.json file.

   Mount an Arduino board from MKR family on top of MKR IoT Carrier.

   This code allow the MKR board to:
   1. send inputs data to different MQTT topics
   2. control outputs from MQTT topics

   Real Time Clock (RTC): This code access one Time Server on the internet
   and get from it the correct Time, using the Network Time Protocol (NTP)
   builtin in the used WiFi module. The time is then used to configure the
   internal RTC (UTC format) of the board using the linux epoch format.

   Docs:
   1. Blog post: https://pietropoluzzi.it/blog/iot/arduino-mkr-iot-carrier-cheat-sheet/
   2. Reference: https://www.arduino.cc/reference/en/libraries/arduino_mkriotcarrier/

   Press Ctrl + Shift + N and create 'arduino-secrets.h' file as follow:

   ```c
   #define WIFI_SSID "" // place your WiFi name inside the quotes
   #define WIFI_PWD "" // place your WiFi password inside the quotes
   #define BROKER_IP "192.168.x.x" // place MQTT Broker IP address inside the quotes
   #define DEV_NAME  "" // place MQTT Client ID name inside the quotes
   #define MQTT_USER "" // place MQTT username inside the quotes (leave empty if MQTT is unauthenticated)
   #define MQTT_PWD  "" // place MQTT password inside the quotes (leave empty if MQTT is unauthenticated)
   ```

   Note: 'arduino-secrets.h' files are indicated in '.gitignore' to avoid loose of sensitive data.
*/
#include <Arduino_MKRIoTCarrier.h>
#include <WiFiNINA.h>  // https://www.arduino.cc/en/Reference/WiFiNINA
#include <Servo.h>     // https://www.arduino.cc/reference/en/libraries/servo/
#include <MQTT.h>      // https://www.arduino.cc/reference/en/libraries/mqtt/
#include "arduino-secrets.h"

MKRIoTCarrier carrier;
MQTTClient mqtt_client;
WiFiClient wifi_client;

// check if have passed more than 'delay_time' milliseconds
unsigned long last_millis = 0;

// MQTT broker port is 1883 by default, but you can change it
const short int broker_port = 1883;

// check /rgbtopic every 'delay_time' milliseconds
const short int delay_time = 2000;

/* MQTT topics named with '_t' suffix  */
// inputs
const char pres_t[10] = "/pressure";
const char temp_t[13] = "/temperature";
const char hum_t[10] = "/humidity";
const char accx_t[6] = "/accx";
const char accy_t[6] = "/accy";
const char accz_t[6] = "/accz";
const char light_t[7] = "/light";

// outputs
const char relay1_t[8] = "/relay1";
const char relay2_t[8] = "/relay2";
const char led0_t[6] = "/led0";
const char led1_t[6] = "/led1";
const char led2_t[6] = "/led2";
const char led3_t[6] = "/led3";
const char led4_t[6] = "/led4";

const char color_dim = 3;

unsigned char r = 0, g = 0, b = 0;

float temperature = 0;
float humidity = 0;

// Wifi radio's status
int status = WL_IDLE_STATUS;

void setup() {
  CARRIER_CASE = false;

  // initialize serial communication and wait for port to open
  Serial.begin(9600);
  // do not wait for serial port to connect. Needed for native USB port only
  // while (!Serial);

  // initialize the MKR IoT Carrier and output any errors in the serial monitor
  carrier.begin();
  carrier.display.setRotation(0);

  // check for the WiFi module
  if (WiFi.status() == WL_NO_MODULE) {
    Serial.println("Communication with WiFi module failed!");
    while (true)
      ;  // do not continue
  }

  // attempt to connect to Wifi network
  while (status != WL_CONNECTED) {
    Serial.print("Attempting to connect to network: ");
    Serial.println(WIFI_SSID);

    // connect to WPA/WPA2 network
    status = WiFi.begin(WIFI_SSID, WIFI_PWD);

    // wait for connection
    delay(5000);
  }

  // the board is now connected, print out the data
  Serial.println("You're connected to the network");
  print_wifi_status();

  // MQTT brokers usually use port 8883 for secure connections
  mqtt_client.begin(BROKER_IP, broker_port, wifi_client);
  mqtt_client.onMessage(message_handler);
  connect();
}

void loop() {
  mqtt_client.loop();
  if (!mqtt_client.connected()) {
    connect();
  }

  // publish a message every 'delay_time' milliseconds
  if (millis() - last_millis > delay_time) {
    last_millis = millis();

    // publish input values
    if (carrier.IMUmodule.accelerationAvailable()) {
      float acc[3] = {0, 0, 0};
      carrier.IMUmodule.readAcceleration(acc[0], acc[1], acc[2]);

      mqtt_client.publish(accx_t, (String)acc[0]);
      mqtt_client.publish(accy_t, (String)acc[1]);
      mqtt_client.publish(accz_t, (String)acc[2]);
    }


    if (carrier.Light.colorAvailable()) {
      // store the RGB values and the ambient light intensity read by the light sensor
      int red, green, blue, ambient;
      carrier.Light.readColor(red, green, blue, ambient);

      String color = "#";
      color.concat(adgust_color(String(red, HEX)));
      color.concat(adgust_color(String(green, HEX)));
      color.concat(adgust_color(String(blue, HEX)));
      mqtt_client.publish(light_t, color);
    }

    mqtt_client.publish(pres_t, (String)carrier.Pressure.readPressure());
    mqtt_client.publish(temp_t, (String)carrier.Env.readTemperature());
    mqtt_client.publish(hum_t, (String)carrier.Env.readHumidity());
  }
}

void connect() {
  Serial.print("Checking WiFi Status");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print("#");
    delay(1000);
  }
  Serial.print("\nConnecting to MQTT queue");

  // try to connect to MQTT queue every second
  while (!mqtt_client.connect(DEV_NAME, MQTT_USER, MQTT_PWD)) {
    Serial.print("#");
    delay(1000);
  }

  char msg[30];
  sprintf(msg, "\nConnected to %s", BROKER_IP);
  Serial.println(msg);

  // subscribe to (outpus) topics
  // mqtt_client.subscribe(_t); // uncomment
}

void message_handler(String &topic, String &payload) {
  Serial.println("incoming: " + topic + " - " + payload);
  /*
    if (topic == _t) {

    } else if (topic == _t) {

    }
  */
}

/*
  void display_config(unsigned char text_size) {
  carrier.display.fillScreen(ST77XX_WHITE);
  carrier.display.setTextColor(ST77XX_BLACK);
  carrier.display.setRotation(0);
  carrier.display.setTextSize(text_size);
  }

  void print_text(unsigned char x, unsigned char y, String message) {
  carrier.display.setCursor(x, y);
  carrier.display.print(message);
  }
*/

void print_wifi_status() {
  Serial.println("Board Information:");

  // print the board's IP address
  Serial.print("IP Address: ");
  Serial.println(WiFi.localIP());

  Serial.println("\nNetwork Information:");
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print the received signal strength
  Serial.print("signal strength (RSSI):");
  Serial.println(WiFi.RSSI());

  Serial.print("Encryption Type:");
  Serial.println(WiFi.encryptionType(), HEX);
  Serial.println();
}

void lights_on() {
  carrier.leds.setPixelColor(0, g, r, b);
  carrier.leds.setPixelColor(1, g, r, b);
  carrier.leds.setPixelColor(2, g, r, b);
  carrier.leds.setPixelColor(3, g, r, b);
  carrier.leds.setPixelColor(4, g, r, b);
  carrier.leds.show();
}



void lights_off() {
  carrier.leds.setPixelColor(0, 0, 0, 0);
  carrier.leds.setPixelColor(1, 0, 0, 0);
  carrier.leds.setPixelColor(2, 0, 0, 0);
  carrier.leds.setPixelColor(3, 0, 0, 0);
  carrier.leds.setPixelColor(4, 0, 0, 0);
  carrier.leds.show();
}

/**
   Adds leading 0 to HEX numbers made by one char
*/
String adgust_color(String color_value) {
  String msg;
  if (color_value.length() == 1) {
    msg = "0";
  }
  msg.concat(color_value);
  return msg;
}
