#include <WebUSB.h>

/**
 * Creating an instance of WebUSBSerial will add an additional USB interface to
 * the device that is marked as vendor-specific (rather than USB CDC-ACM) and
 * is therefore accessible to the browser.
 *
 * The URL here provides a hint to the browser about what page the user should
 * navigate to to interact with the device.
 * 
 * This sketch is temporally only compatible with Arrduino MKR WiFi1010 & MKR IoT Carrier
 * 
 * Docs: https://github.com/webusb/arduino/tree/gh-pages/demos/rgb
 */
WebUSB WebUSBSerial(1 /* https:// */, "localhost:8000");

#define Serial WebUSBSerial

#include <Arduino_MKRIoTCarrier.h>
MKRIoTCarrier carrier;

uint8_t r = 0, g = 0, b = 0;

int color[3];
int colorIndex;

void setup() {
  CARRIER_CASE = false;
  
  // initialize the MKR IoT Carrier and output any errors in the serial monitor
  carrier.begin();
  carrier.display.setRotation(0);


  // wait for Serial monitor to open
  while (!Serial) {
    ;
  }
  
  Serial.begin(9600);
  Serial.write("Sketch begins.\r\n");
  Serial.flush();
  colorIndex = 0;
}

void loop() {
  if (Serial && Serial.available()) {
    color[colorIndex++] = Serial.read();
    if (colorIndex == 3) {
      r = color[0];
      g = color[1];
      b = color[2];
      lightsOn();
      
      Serial.print("Set LED to ");
      Serial.print(color[0]);
      Serial.print(", ");
      Serial.print(color[1]);
      Serial.print(", ");
      Serial.print(color[2]);
      Serial.print(".\r\n");
      Serial.flush();
      colorIndex = 0;
    }
  }
}

void lightsOn() {
  carrier.leds.setPixelColor(0, r, g, b);
  carrier.leds.setPixelColor(1, r, g, b);
  carrier.leds.setPixelColor(2, r, g, b);
  carrier.leds.setPixelColor(3, r, g, b);
  carrier.leds.setPixelColor(4, r, g, b);
  carrier.leds.show();
}
  
 
  
void lightsOff() {
  carrier.leds.setPixelColor(0, 0, 0, 0);
  carrier.leds.setPixelColor(1, 0, 0, 0);
  carrier.leds.setPixelColor(2, 0, 0, 0);
  carrier.leds.setPixelColor(3, 0, 0, 0);
  carrier.leds.setPixelColor(4, 0, 0, 0); 
  carrier.leds.show();
}
