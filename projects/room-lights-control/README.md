# Room Lights Control

## Installation
WebUSB installation guide:
```shell
# move to the correct directory
$ cd ~/Arduino/libraries

# clone the repository
$ https://github.com/webusb/arduino.git

# or use SSH
# $ git clone git@github.com:webusb/arduino.git

$ mv arduino/library/WebUSB ./
$ rm -rf arduino
$ ls
# readme.txt  WebUSB
$ pwd
# $HOME/Arduino/libraries

# the device must be upgraded from USB 2.0 to USB 2.1
# move into the SDK installation directory
$ cd /opt/arduino/hardware/arduino/avr/cores/arduino

# edit USBCore.h header file:
# change this: #define USB_VERSION 0x200
# into this: #define USB_VERSION 0x210

# in Linux, enable USB device to be accessible to web application:
$ lsusb
# Bus 001 Device 009: ID 2341:8054 Arduino SA Arduino MKR WiFi 1010

$ cd /etc/udev/rules.d
$ sudo nano 70-arduino-wifi.rules

# edit as follow:
# SUBSYSTEM=="usb", ATTRS{idVendor}=="2341", ATTR{idProduct}=="8054", MODE="0660", GROUP="plugdev"

# enable changes:
$ sudo udevadm control --reload-rules
$ rules.d sudo udevadm trigger
```

## Deploy

Upload the `room-lights-control.ino` sketch into your board.
Run the web server with the following commands:
```shell
# 1) change directory into this directory
$ cd $HOME/arduino-projects/projects/room-lights-control # in my case

# 2) with npm installed, install http-server package globally
$ sudo npm install http-server -g

# 3) run the web server
$ http-server
```

The output will be:
```txt
Starting up http-server, serving ./

http-server version: 14.1.1

http-server settings: 
CORS: disabled
Cache: 3600 seconds
Connection Timeout: 120 seconds
Directory Listings: visible
AutoIndex: visible
Serve GZIP Files: false
Serve Brotli Files: false
Default File Extension: none

Available on:
  http://127.0.0.1:8080
  http://192.168.1.85:8080
Hit CTRL-C to stop the server
```

Another way to serve the webapp with *live reload* is by using `browser-sync` package:
```shell
sudo npm install -g browser-sync

browser-sync start -s -f . --no-notify --port 8000
```

In this way, you can navigate to port `8000` to visit the served page.
Moreover, on port `3000` has started a monitor server.