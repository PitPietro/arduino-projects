#include <Arduino_LSM6DSOX.h> // https://www.arduino.cc/reference/en/libraries/arduino_lsm6dsox/

#define CONVERT_G_TO_MS2    9.80665f
#define FREQUENCY_HZ        104
#define INTERVAL_MS         (1000 / (FREQUENCY_HZ + 1))

static unsigned long last_interval_ms = 0;

/**
 * Data forwarder - Arduino Nano RP2040 Connect
 *
 * Docs: https://docs.edgeimpulse.com/docs/edge-impulse-cli/cli-data-forwarder
 */
void setup() {
    Serial.begin(115200);
    
    // wait for serial port to connect. Needed for native USB port only
    while (!Serial);
    
    // initialize the IMU, return 1 on success and 0 on failure
    // de-initialize the IMU with 'IMU.end()' void function
    if (!IMU.begin()) {
      Serial.println("### Failed to initialize IMU ###");
      while (1);
    }

    // print on Serial Monitor IMU's accelerometer sample rate in Hz (Hertz)
    Serial.print("Accelerometer sample rate = ");
    Serial.print(IMU.accelerationSampleRate());
    Serial.println("Hz");
    Serial.println();

    // print on Serial Monitor IMU's gyroscope sample rate in Hz (Hertz)
    Serial.print("Gyroscope sample rate = ");
    Serial.print(IMU.gyroscopeSampleRate());
    Serial.println("Hz");
    Serial.println();
}

void loop() {
    float x, y, z;

    if (millis() > last_interval_ms + INTERVAL_MS) {
        last_interval_ms = millis();
        
        IMU.readAcceleration(x, y, z);

        Serial.print(x * CONVERT_G_TO_MS2);
        Serial.print('\t');
        Serial.print(y * CONVERT_G_TO_MS2);
        Serial.print('\t');
        Serial.println(z * CONVERT_G_TO_MS2);
    }
}