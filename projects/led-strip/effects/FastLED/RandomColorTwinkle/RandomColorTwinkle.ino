#include "FastLED.h"

// digital output pin of the board
#define PIN 6
// number of LEDs of the strip
#define NUM_LEDS 100
// brightness threshold
#define BRIGHT 255

CRGB leds[NUM_LEDS];

void setup() {
  FastLED.addLeds<WS2811, PIN, GRB>(leds, NUM_LEDS).setCorrection(TypicalLEDStrip);
}

/**
 * This is a variation on the Twinkle effect.
 * The only difference is that the colors are now randomly generated, and therefore the first 3 color parameters are no longer of use and have been removed.
 * So we use only 3 parameters:
 * 'count' determines how many pixels will be done in one run, where as the second parameter determines how much time will be paused between individual pixels (speed).
 * 'onlyOne' should be true if you want to see only one LED at a time.
 * If it's set to false then all 'count' number of LEDs will be visible (added one at a time).
 */
void loop() {
  TwinkleRandom(60, 100, false);
}

void TwinkleRandom(int count, int speedDelay, boolean onlyOne) {
  setAll(0, 0, 0);

  for (int i = 0; i < count; i++) {
    setPixel(random(NUM_LEDS), random(0, 255), random(0, 255), random(0, 255));
    showStrip();
    delay(speedDelay);
    if (onlyOne) {
      setAll(0, 0, 0);
    }
  }

  delay(speedDelay);
}

void showStrip() {
  FastLED.show();
}

void setPixel(int pixelNumber, byte red, byte green, byte blue) {
  leds[pixelNumber].r = red;
  leds[pixelNumber].g = green;
  leds[pixelNumber].b = blue;
}

void setAll(byte red, byte green, byte blue) {
  for (int i = 0; i < NUM_LEDS; i++) {
    setPixel(i, red, green, blue);
  }
  showStrip();
}
