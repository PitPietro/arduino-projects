#include "FastLED.h"

// digital output pin of the board
#define PIN 6
// number of LEDs of the strip
#define NUM_LEDS 100
// brightness threshold
#define BRIGHT 255

CRGB leds[NUM_LEDS];

void setup() {
  FastLED.addLeds<WS2811, PIN, GRB>(leds, NUM_LEDS).setCorrection(TypicalLEDStrip);
}

void loop() {
  // Slower:
  // strobe(0xff, 0x77, 0x00, 10, 100, 1000);
  // Fast:
  strobe(0xff, 0xff, 0xff, 10, 50, 1000);
}

void strobe(byte red, byte green, byte blue, int strobeCount, int flashDelay, int endPause) {
  for (int j = 0; j < strobeCount; j++) {
    setAll(red, green, blue);
    showStrip();
    delay(flashDelay);
    setAll(0, 0, 0);
    showStrip();
    delay(flashDelay);
  }

  delay(endPause);
}

void showStrip() {
  FastLED.show();
}

void setPixel(int pixelNumber, byte red, byte green, byte blue) {
  leds[pixelNumber].r = red;
  leds[pixelNumber].g = green;
  leds[pixelNumber].b = blue;
}

void setAll(byte red, byte green, byte blue) {
  for (int i = 0; i < NUM_LEDS; i++) {
    setPixel(i, red, green, blue);
  }
  showStrip();
}
