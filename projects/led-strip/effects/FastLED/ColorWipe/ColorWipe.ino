#include "FastLED.h"
// digital output pin of the board
#define PIN 6
// number of LEDs of the strip
#define NUM_LEDS 100

CRGB leds[NUM_LEDS];

// increase this value to slow down the effect
unsigned short int delayTime = 5;

void setup() {
  FastLED.addLeds<WS2811, PIN, GRB>(leds, NUM_LEDS).setCorrection(TypicalLEDStrip);
}

/**
 * Colors are taken from Google's Material Palette: https://material.io/resources/color/#!/
*/
void loop() {
  colorWipe(0xdd, 0x2c, 0x00, 50);
  colorWipe(0x30, 0x4f, 0xff, 50);
  colorWipe(0x00, 0xbf, 0xa5, 50);
  reverseColorWipe(0xdd, 0x2c, 0x00, 50);
  reverseColorWipe(0x30, 0x4f, 0xff, 50);
  reverseColorWipe(0x00, 0xbf, 0xa5, 50);
}

void colorWipe(byte red, byte green, byte blue, int delayTime) {
  for (int i = 0; i < NUM_LEDS; i++) {
    setPixel(i, red, green, blue);
    showStrip();
    delay(delayTime);
  }
}

void reverseColorWipe(byte red, byte green, byte blue, int delayTime) {
  for (int i = NUM_LEDS-1; i >= 0; i--) {
    setPixel(i, red, green, blue);
    showStrip();
    delay(delayTime);
  }
}

void showStrip() {
  FastLED.show();
}

void setPixel(int pixelNumber, byte red, byte green, byte blue) {
  leds[pixelNumber].r = red;
  leds[pixelNumber].g = green;
  leds[pixelNumber].b = blue;
}

void setAll(byte red, byte green, byte blue) {
  for (int i = 0; i < NUM_LEDS; i++) {
    setPixel(i, red, green, blue);
  }
  showStrip();
}
