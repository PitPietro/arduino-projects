#include "FastLED.h"

// digital output pin of the board
#define PIN 6
// number of LEDs of the strip
#define NUM_LEDS 100
// brightness threshold
#define BRIGHT 255

CRGB leds[NUM_LEDS];

void setup() {
  FastLED.addLeds<WS2811, PIN, GRB>(leds, NUM_LEDS).setCorrection(TypicalLEDStrip);
}

/**
 * This is a more complex variant of the Bouncing Balls effect.
 * Instead of just one color, it allows the use of multiple colors, each defined by you.
 * This makes the function call a little bit more complex, since it works for any number of balls you'd like set.
 * The problem would be how to pass the color for each ball.
 * To accomplish this you will have to define a multi dimensional array filled with the RGB backhoe of each balls
 */
void loop() {
  byte colors[4][3] = {
    { 0xff, 0xff, 0 },
    { 0xff, 0, 0 },
    { 0xff, 0xff, 0xff },
    { 0, 0, 0xff }
  };

  BouncingColoredBalls(3, colors);
}

void BouncingColoredBalls(int ballCount, byte colors[][3]) {
  float gravity = -9.81;
  int startHeight = 1;

  float height[ballCount];
  float impactVelocityStart = sqrt(-2 * gravity * startHeight);
  float impactVelocity[ballCount];
  float timeSinceLastBounce[ballCount];
  int position[ballCount];
  long clockTimeSinceLastBounce[ballCount];
  float dampening[ballCount];

  for (int i = 0; i < ballCount; i++) {
    clockTimeSinceLastBounce[i] = millis();
    height[i] = startHeight;
    position[i] = 0;
    impactVelocity[i] = impactVelocityStart;
    timeSinceLastBounce[i] = 0;
    dampening[i] = 0.90 - float(i) / pow(ballCount, 2);
  }

  while (true) {
    for (int i = 0; i < ballCount; i++) {
      timeSinceLastBounce[i] = millis() - clockTimeSinceLastBounce[i];
      height[i] = 0.5 * gravity * pow(timeSinceLastBounce[i] / 1000, 2.0) + impactVelocity[i] * timeSinceLastBounce[i] / 1000;

      if (height[i] < 0) {
        height[i] = 0;
        impactVelocity[i] = dampening[i] * impactVelocity[i];
        clockTimeSinceLastBounce[i] = millis();

        if (impactVelocity[i] < 0.01) {
          impactVelocity[i] = impactVelocityStart;
        }
      }
      position[i] = round(height[i] * (NUM_LEDS - 1) / startHeight);
    }

    for (int i = 0; i < ballCount; i++) {
      setPixel(position[i], colors[i][0], colors[i][1], colors[i][2]);
    }

    showStrip();
    setAll(0, 0, 0);
  }
}

void showStrip() {
  FastLED.show();
}

void setPixel(int pixelNumber, byte red, byte green, byte blue) {
  leds[pixelNumber].r = red;
  leds[pixelNumber].g = green;
  leds[pixelNumber].b = blue;
}

void setAll(byte red, byte green, byte blue) {
  for (int i = 0; i < NUM_LEDS; i++) {
    setPixel(i, red, green, blue);
  }
  showStrip();
}
