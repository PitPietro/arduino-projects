#include "FastLED.h"

// digital output pin of the board
#define PIN 6
// number of LEDs of the strip
#define NUM_LEDS 100
// brightness threshold
#define BRIGHT 255

CRGB leds[NUM_LEDS];

void setup() {
  FastLED.addLeds<WS2811, PIN, GRB>(leds, NUM_LEDS).setCorrection(TypicalLEDStrip);
}

void loop() {
  forward(0x30, 0x20, 0xFF);
  downward(0x30, 0x20, 0xFF);
}

/**
 * Add '0x' in front of each of these hex values when using them.
 * '0x' designates a hexadecimal value, with which a color is composed.
 * Those bytes are ment to be hexadecimal values that compose the RBG LED parts.
*/
void forward(byte red, byte green, byte blue) {
  for(int i = 0; i < NUM_LEDS; i++) {
    setAll(0x00, 0x00, 0x00);
    setPixel(i-2, green, blue, red);
    setPixel(i-1, blue, red, green);
    setPixel(i, red, green, blue);
    showStrip();
    delay(delayTime);
  }
}

void downward(byte red, byte green, byte blue) {
  for(int i = NUM_LEDS-1; i >= 0; i--) {
    setAll(0x00, 0x00, 0x00);
    setPixel(i-2, green, blue, red);
    setPixel(i-1, blue, red, green);
    setPixel(i, red, green, blue);
    showStrip();
    delay(delayTime);
  }
}

void showStrip() {
  FastLED.show();
}

void setPixel(int pixelNumber, byte red, byte green, byte blue) {
  leds[pixelNumber].r = red;
  leds[pixelNumber].g = green;
  leds[pixelNumber].b = blue;
}

void setAll(byte red, byte green, byte blue) {
  for (int i = 0; i < NUM_LEDS; i++) {
    setPixel(i, red, green, blue);
  }
  showStrip();
}
