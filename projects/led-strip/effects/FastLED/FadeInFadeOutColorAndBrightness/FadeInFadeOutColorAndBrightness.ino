#include "FastLED.h"

// digital output pin of the board
#define PIN 6
// number of LEDs of the strip
#define NUM_LEDS 100
// brightness threshold
#define BRIGHT 255

CRGB leds[NUM_LEDS];

void setup() {
  FastLED.addLeds<WS2811, PIN, GRB>(leds, NUM_LEDS).setCorrection(TypicalLEDStrip);
}

void loop() {
  FadeInOut(0xff, 0x77, 0x00);
  FadeInOut(0x77, 0x00, 0xff);
  FadeInOut(0x00, 0xff, 0x77);
}

void FadeInOut(byte red, byte green, byte blue) {
  float r, g, b;
    for (int k = 0; k < 256; k++) {
      FastLED.setBrightness(k);
      r = (k / 256.0) * red;
      g = (k / 256.0) * green;
      b = (k / 256.0) * blue;
      setAll(r, g, b);
      showStrip();
    }
    delay(20);

    for (int k = 255; k >= 0; k--) {
      FastLED.setBrightness(k);
      r = (k / 256.0) * red;
      g = (k / 256.0) * green;
      b = (k / 256.0) * blue;
      setAll(r, g, b);
      showStrip();
    }
}

void showStrip() {
  FastLED.show();
}

void setPixel(int pixelNumber, byte red, byte green, byte blue) {
  leds[pixelNumber].r = red;
  leds[pixelNumber].g = green;
  leds[pixelNumber].b = blue;
}

void setAll(byte red, byte green, byte blue) {
  for (int i = 0; i < NUM_LEDS; i++) {
    setPixel(i, red, green, blue);
  }
  showStrip();
}
