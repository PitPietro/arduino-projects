#include <Adafruit_NeoPixel.h>

// digital output pin of the board
#define PIN 6
// number of LEDs of the strip
#define NUM_LEDS 100

// Parameter 1 = number of pixels in strip
// Parameter 2 = pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, PIN, NEO_GRB + NEO_KHZ800);

void setup() {
  // initialize the strip
  strip.begin();
  // initialize all pixels to 'off'
  strip.show();
  // brightness value is in a range from 0 to 255
  strip.setBrightness(255);
}

/**
 * This is a variation on the Twinkle effect.
 * The only difference is that the colors are now randomly generated, and therefore the first 3 color parameters are no longer of use and have been removed.
 * So we use only 3 parameters:
 * 'count' determines how many pixels will be done in one run, where as the second parameter determines how much time will be paused between individual pixels (speed).
 * 'onlyOne' should be true if you want to see only one LED at a time.
 * If it's set to false then all 'count' number of LEDs will be visible (added one at a time).
 */
void loop() {
  TwinkleRandom(60, 100, false);
}

void TwinkleRandom(int count, int speedDelay, boolean onlyOne) {
  setAll(0, 0, 0);

  for (int i = 0; i < count; i++) {
    setPixel(random(NUM_LEDS), random(0, 255), random(0, 255), random(0, 255));
    showStrip();
    delay(speedDelay);
    if (onlyOne) {
      setAll(0, 0, 0);
    }
  }

  delay(speedDelay);
}

void showStrip() {
  strip.show();
}

void setPixel(int pixelNumber, byte red, byte green, byte blue) {
  strip.setPixelColor(pixelNumber, strip.Color(red, green, blue));
}

void setAll(byte red, byte green, byte blue) {
  for (int i = 0; i < NUM_LEDS; i++) {
    setPixel(i, red, green, blue);
  }
  showStrip();
}
