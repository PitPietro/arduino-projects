#include <Adafruit_NeoPixel.h>

// digital output pin of the board
#define PIN 6
// number of LEDs of the strip
#define NUM_LEDS 100

// Parameter 1 = number of pixels in strip
// Parameter 2 = pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, PIN, NEO_GRB + NEO_KHZ800);

void setup() {
  // initialize the strip
  strip.begin();
  // initialize all pixels to 'off'
  strip.show();
  // brightness value is in a range from 0 to 255
  strip.setBrightness(255);
}

/**
 * The first tree parameters are needed to set the meteor's color. 
 * Then set the meteor size – the number of LEDs that represent the meteor, not counting the tail of the meteor.
 * The 5th parameter sets how fast the meteor tail decays/ disappears. A larger number makes the tail short and/or disappear faster. Theoretically a value of 64 should reduce the brightness by 25% for each time the meteor gets drawn.
 * Since meteors are not perfect, I’ve added the 6th parameter to mimic some sorts of difference in debris by making the decay a little random. If this value is set to "true" then some randomness is added to the rail. If you set the value to "false" then the tail will be very smooth.
 * Finally there is the last parameter, which basically indicates how much the drawing speed has to be delayed. A value of zero (0) means maximum speed. Any value above zero indicates how many milliseconds (1000 milliseconds in a second) the drawing will be delayed.
 */
void loop() {
  meteorRain(0xff, 0xff, 0xff, 10, 64, true, 30);
  meteorRain(0xaa, 0xbb, 0xff, 5, 30, true, 40);  
}

void meteorRain(byte red, byte green, byte blue, byte meteorSize, byte meteorTrailDecay, boolean meteorRandomDecay, int speedDelay) {  
  setAll(0, 0, 0);
 
  for(int i = 0; i < NUM_LEDS+NUM_LEDS; i++) {
    // fade brightness all LEDs one step
    for(int j=0; j<NUM_LEDS; j++) {
      if( (!meteorRandomDecay) || (random(10)>5) ) {
        fadeToBlack(j, meteorTrailDecay );        
      }
    }
   
    // draw meteor
    for(int j = 0; j < meteorSize; j++) {
      if( ( i-j <NUM_LEDS) && (i-j>=0) ) {
        setPixel(i-j, red, green, blue);
      }
    }
   
    showStrip();
    delay(speedDelay);
  }
}

void fadeToBlack(int ledNo, byte fadeValue) {
    uint32_t oldColor;
    uint8_t r, g, b;
    int value;
   
    oldColor = strip.getPixelColor(ledNo);
    r = (oldColor & 0x00ff0000UL) >> 16;
    g = (oldColor & 0x0000ff00UL) >> 8;
    b = (oldColor & 0x000000ffUL);

    r = (r<=10)? 0 : (int) r-(r*fadeValue/256);
    g = (g<=10)? 0 : (int) g-(g*fadeValue/256);
    b = (b<=10)? 0 : (int) b-(b*fadeValue/256);
   
    strip.setPixelColor(ledNo, r,g,b);
}

void showStrip() {
  strip.show();
}

void setPixel(int pixelNumber, byte red, byte green, byte blue) {
  strip.setPixelColor(pixelNumber, strip.Color(red, green, blue));
}

void setAll(byte red, byte green, byte blue) {
  for (int i = 0; i < NUM_LEDS; i++) {
    setPixel(i, red, green, blue);
  }
  showStrip();
}
