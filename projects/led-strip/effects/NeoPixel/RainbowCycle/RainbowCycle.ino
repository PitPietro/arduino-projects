#include <Adafruit_NeoPixel.h>

// digital output pin of the board
#define PIN 6
// number of LEDs of the strip
#define NUM_LEDS 64

// Parameter 1 = number of pixels in strip
// Parameter 2 = pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, PIN, NEO_GRB + NEO_KHZ800);

void setup() {
  // initialize the strip
  strip.begin();
  // initialize all pixels to 'off'
  strip.show();
  // brightness value is in a range from 0 to 255
  strip.setBrightness(255);
}

void loop() {
  rainbowCycle(20);
}

void rainbowCycle(int speedDelay) {
  byte* c;
  uint16_t i, j;

  // 5 cycles of all colors on wheel
  for (j = 0; j < 256 * 5; j++) {
    for (i = 0; i < NUM_LEDS; i++) {
      c = wheel(((i * 256 / NUM_LEDS) + j) & 255);
      setPixel(i, *c, *(c + 1), *(c + 2));
    }
    showStrip();
    delay(speedDelay);
  }
}

byte* wheel(byte wheelPosition) {
  static byte c[3];

  if (wheelPosition < 85) {
    c[0] = wheelPosition * 3;
    c[1] = 255 - wheelPosition * 3;
    c[2] = 0;
  } else if (wheelPosition < 170) {
    wheelPosition -= 85;
    c[0] = 255 - wheelPosition * 3;
    c[1] = 0;
    c[2] = wheelPosition * 3;
  } else {
    wheelPosition -= 170;
    c[0] = 0;
    c[1] = wheelPosition * 3;
    c[2] = 255 - wheelPosition * 3;
  }

  return c;
}

void showStrip() {
  strip.show();
}

void setPixel(int pixelNumber, byte red, byte green, byte blue) {
  strip.setPixelColor(pixelNumber, strip.Color(red, green, blue));
}

void setAll(byte red, byte green, byte blue) {
  for (int i = 0; i < NUM_LEDS; i++) {
    setPixel(i, red, green, blue);
  }
  showStrip();
}
