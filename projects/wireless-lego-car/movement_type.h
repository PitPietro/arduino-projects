#define NO_MOVEMENT 0
#define CLOCKWISE 0
#define COUNTER_CLOCKWISE 1

// the type of those constants is 'int'
#define STOP 0
#define FORWARD 1
#define DOWNWARD 2
#define LEFT 3
#define RIGHT 4
