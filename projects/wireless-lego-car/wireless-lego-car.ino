/**
   Wireless LEGO car
*/

#include <Servo.h>      // https://www.arduino.cc/reference/en/libraries/servo/
#include <ArduinoBLE.h> // https://www.arduino.cc/reference/en/libraries/arduinoble/
#include "movement_type.h"
#define BLE_CUSTOM_UUID(val) ("6fbe1da7-" val "-44de-92c4-bb6e04fb0212")


// takes as parameter a UUID: 16-bit or 128-bit UUID in String format
// https://www.arduino.cc/reference/en/libraries/arduinoble/bleservice/
BLEService carService(BLE_CUSTOM_UUID("0000"));

// movement type characteristic
BLEUnsignedCharCharacteristic movementTypeChar(BLE_CUSTOM_UUID("1001"), BLERead | BLEWrite);

// check for updates every 'update_time' milliseconds
const char update_time = 20;

void setup() {
  // initialize serial communication and wait for port to open
  Serial.begin(9600);
  // do not wait for serial port to connect. Needed for native USB port only
  while (!Serial);

  // initialize ArduinoBLE library
  if (!BLE.begin()) {
    Serial.println("Bluetooth® Low Energy failed to start!");
    while (1);
  }

  // device MAC Address
  String mac_address = BLE.address();

  Serial.print("Device MAC Address: ");
  Serial.println(mac_address);

  mac_address.toUpperCase();

  // String to calculate the local and device name
  // Append last two hex values of MAC address to ble_name
  String ble_name = "Arduino Car ";
  ble_name += mac_address[mac_address.length() - 5];
  ble_name += mac_address[mac_address.length() - 4];
  ble_name += mac_address[mac_address.length() - 2];
  ble_name += mac_address[mac_address.length() - 1];

  Serial.print("Device name: ");
  Serial.println(ble_name);

  // set the name that will appear when scanning for Bluetooth® devices
  BLE.setLocalName(ble_name.c_str());
  BLE.setDeviceName(ble_name.c_str());

  // set the advertised service UUID used when advertising to the value of the BLEService provided
  BLE.setAdvertisedService(carService);

  // add the characteristic to the BLEService
  carService.addCharacteristic(movementTypeChar);

  // assign event handlers for characteristic
  movementTypeChar.setEventHandler(BLEWritten, movementTypeCharWritten);

  // add a BLEService to the set of services the BLE device provides
  BLE.addService(carService);

  // start advertising the BLE device
  BLE.advertise();
  Serial.println("Bluetooth® device active, waiting for connections...");
}

void loop() {
  // static auto lastCheck = millis();

  // query the central BLE device connected: wait for a BLE central
  BLEDevice central_device = BLE.central();

  // save the Bluetooth® MAC address of the BLE device
  String central_address = central_device.address();

  // if a central is connected to the peripheral
  if (central_device) {
    Serial.print("MAC address of central: ");
    Serial.println(central_address);

    // turn on the built-in LED to indicate the connection
    digitalWrite(LED_BUILTIN, HIGH);

    // while the central is connected
    while (central_device.connected()) {

      // check for updates every 'update_time' milliseconds
      // if (millis() - lastCheck >= update_time) {}
    }

    // when the central disconnects, turn off the built-in LED
    digitalWrite(LED_BUILTIN, LOW);
    Serial.print("Disconnected from central: ");
    Serial.println(central_address);
  } else {
    // try to recconnect to central

    // query the central BLE device connected: wait for a BLE central
    central_device = BLE.central();

    // save the Bluetooth® MAC address of the BLE device
    central_address = central_device.address();
  }
}

/**
   Set the event handler (callback) function that will be called when the specified event occurs.

   Parameters:
   > eventType: event type (BLESubscribed, BLEUnsubscribed, BLERead, BLEWritten)
   > callback: function to call when the event occurs

   Docs: https://www.arduino.cc/reference/en/libraries/arduinoble/blecharacteristic.seteventhandler/
*/
void movementTypeCharWritten(BLEDevice central, BLECharacteristic characteristic) {
  // central wrote new value to characteristic, update LED
  Serial.println("Characteristic event: BLEWritten");

  unsigned char movement_value = 0;
  movementTypeChar.readValue(movement_value);

  // Serial.print("movement_value: ");
  // Serial.println(movement_value);

  switch (movement_value) {
    case STOP:
      Serial.println("movement: STOP");
      break;
    case FORWARD:
      Serial.println("movement: FORWARD");
      break;
    case DOWNWARD:
      Serial.println("movement: DOWNWARD");
      break;
    case LEFT:
      Serial.println("movement: LEFT");
      break;
    case RIGHT:
      Serial.println("movement: RIGHT");
      break;
    default:
      Serial.println("NO movement recognized");
      break;
  }
}
