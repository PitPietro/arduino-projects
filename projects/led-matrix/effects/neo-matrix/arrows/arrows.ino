#include <Adafruit_NeoPixel.h>
#include <Adafruit_GFX.h>
#include <Adafruit_NeoMatrix.h>

#define PIN 6 // pin on the Arduino connected to the NeoPixels
#define M_WIDTH 8 // matrix width
#define M_HEIGHT 8 // matrix height

// The 8x8 matrix uses 800 KHz (v2) pixels that expect GRB color data.
Adafruit_NeoMatrix matrix = Adafruit_NeoMatrix(8, 8, PIN, NEO_MATRIX_TOP + NEO_MATRIX_RIGHT + NEO_MATRIX_ROWS + NEO_MATRIX_ZIGZAG, NEO_GRB + NEO_KHZ800);

const unsigned char middle_screen = M_HEIGHT / 2; // 4

uint32_t red = matrix.Color(255, 0, 0); // red
uint32_t green = matrix.Color(0, 255, 0); // green
uint32_t blue = matrix.Color(0, 0, 255); // blue

/**
   Test Graphics Primitives
   This sketch is based on Adafruit NeoPixel NeoMatrix 8x8 - 64 RGB LED Pixel Matrix: https://www.adafruit.com/product/1487
   Adafruit_NeoMatrix's drawing functions are taken from Adafruit_GFX library: https://learn.adafruit.com/adafruit-gfx-graphics-library/graphics-primitives
*/
void setup(void) {
  matrix.begin();
  matrix.setBrightness(25); // from 0 to 255

  matrix.fillScreen(0);
  matrix.show(); // send updated pixel colors to the hardware
}

void loop() {
  matrix.fillScreen(0);

  int i;
  for (i = 0; i < 3; i++) {
    right_arrow();
  }

  for (i = 0; i < 3; i++) {
    left_arrow();
  }

  matrix.show();
  delay(50);
}

void right_arrow() {
  int i, j;
  const short int effect_lenght = M_WIDTH;
  const unsigned char arrow_dim = 2;

  for (i = -arrow_dim; i < effect_lenght; i++) {
    matrix.fillScreen(0);

    for (j = i; j < i + arrow_dim; j++) {
      matrix.drawLine(i + j - middle_screen + 1, 0, i + j, middle_screen - 1, green); // x0, y0, x1, y1, color
      matrix.drawLine(i + j - middle_screen + 1, M_HEIGHT - 1, i + j, middle_screen, green);
    }

    for (j = i - arrow_dim; j < i; j++) {
      matrix.drawLine(i + j - middle_screen + 1, 0, i + j, middle_screen - 1, blue);
      matrix.drawLine(i + j - middle_screen + 1, M_HEIGHT - 1, i + j, middle_screen, blue);
    }

    matrix.show();
    delay(80);
  }
}

void left_arrow() {
  int i, j;
  const short int effect_lenght = M_WIDTH;
  const unsigned char arrow_dim = 2;

  for (i = effect_lenght; i >= -arrow_dim; i--) {
    matrix.fillScreen(0);

    // reverse the colors in the inner j-loops
    for (j = i; j < i + arrow_dim; j++) {
      matrix.drawLine(i + j + middle_screen - 1, 0, i + j, middle_screen - 1, blue); // x0, y0, x1, y1, color
      matrix.drawLine(i + j + middle_screen - 1, M_HEIGHT - 1, i + j, middle_screen, blue);
    }

    for (j = i - arrow_dim; j < i; j++) {
      matrix.drawLine(i + j + middle_screen - 1, 0, i + j, middle_screen - 1, green);
      matrix.drawLine(i + j + middle_screen - 1, M_HEIGHT - 1, i + j, middle_screen, green);
    }

    matrix.show();
    delay(80);
  }
}
