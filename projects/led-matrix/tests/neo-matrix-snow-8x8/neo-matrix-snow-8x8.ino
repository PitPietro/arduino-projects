/**
   Docs: https://learn.adafruit.com/neopixel-matrix-snowflake-sweater/basic-test
*/

// SPDX-FileCopyrightText: 2019 Anne Barela for Adafruit Industries
// SPDX-License-Identifier: MIT

/*********************************************************************
  Basic snowflake code

  Accompanies the Adafruit guide
  https://learn.adafruit.com/neopixel-matrix-snowflake-sweater

  Adafruit invests time and resources providing this open source code,
  please support Adafruit and open-source hardware by purchasing
  products from Adafruit!

  MIT license, check LICENSE for more information
  All text above, and the splash screen below must be included in
  any redistribution
*********************************************************************/
#include <Adafruit_NeoPixel.h>
#include <Adafruit_GFX.h>
#include <Adafruit_NeoMatrix.h>

#define PIN 6 // pin on the Arduino connected to the NeoPixels

// Adafruit_NeoMatrix matrix = Adafruit_NeoMatrix(8, 8, PIN, NEO_MATRIX_TOP + NEO_MATRIX_LEFT + NEO_MATRIX_COLUMNS + NEO_MATRIX_ZIGZAG, NEO_GRB + NEO_KHZ800);
Adafruit_NeoMatrix matrix = Adafruit_NeoMatrix(
                              8, 8, PIN,
                              NEO_MATRIX_TOP + NEO_MATRIX_RIGHT + NEO_MATRIX_ROWS + NEO_MATRIX_ZIGZAG, /* https://www.adafruit.com/product/1487 */
                              NEO_GRB + NEO_KHZ800
                            );

void setup() {
  matrix.begin();
  matrix.setBrightness(40);

  matrix.fillScreen(0);
  matrix.show(); // This sends the updated pixel colors to the hardware.
}

void loop(void) {
  matrix.fillScreen(0);
  SnowFlake1(matrix.Color(200, 200, 200));
  matrix.show(); // This sends the updated pixel colors to the hardware.
  delay(500);

  matrix.fillScreen(0);
  SnowFlake2(matrix.Color(255, 0, 0));
  matrix.show(); // This sends the updated pixel colors to the hardware.
  delay(500);

  matrix.fillScreen(0);
  SnowFlake3(matrix.Color(0, 255, 0));
  matrix.show(); // This sends the updated pixel colors to the hardware.
  delay(500);

  matrix.fillScreen(0);
  SnowFlake4(matrix.Color(200, 200, 200));
  matrix.show(); // This sends the updated pixel colors to the hardware.
  delay(500);

  matrix.fillScreen(0);
  SnowFlake5(matrix.Color(255, 0, 0));
  matrix.show(); // This sends the updated pixel colors to the hardware.
  delay(500);

  matrix.fillScreen(0);
  SnowFlake6(matrix.Color(0, 255, 0));
  matrix.show(); // This sends the updated pixel colors to the hardware.
  delay(500);

  matrix.fillScreen(0);
  SnowFlake7(matrix.Color(200, 200, 200));
  matrix.show(); // This sends the updated pixel colors to the hardware.
  delay(500);

  matrix.fillScreen(0);
  SnowFlake8(matrix.Color(255, 0, 0));
  matrix.show(); // This sends the updated pixel colors to the hardware.
  delay(500);

  matrix.fillScreen(0);
  SnowFlake9(matrix.Color(200, 200, 200));
  matrix.show(); // This sends the updated pixel colors to the hardware.
  delay(500);

  matrix.fillScreen(0);
  SnowFlake10(matrix.Color(200, 200, 200));
  matrix.show(); // This sends the updated pixel colors to the hardware.
  delay(500);

  matrix.fillScreen(0);
  SnowFlake11(matrix.Color(200, 200, 200));
  matrix.show(); // This sends the updated pixel colors to the hardware.
  delay(500);

  matrix.fillScreen(0);
  SnowFlake12(matrix.Color(200, 200, 200));
  matrix.show(); // This sends the updated pixel colors to the hardware.
  delay(500);
}

void SnowFlake1(uint32_t c) {
  matrix.drawLine(0, 2, 2, 0, c); // x0, y0, x1, y1, color
  matrix.drawLine(4, 0, 6, 2, c); // x0, y0, x1, y1, color
  matrix.drawLine(0, 4, 2, 6, c); // x0, y0, x1, y1, color
  matrix.drawLine(4, 6, 6, 4, c); // x0, y0, x1, y1, color
  matrix.drawFastVLine(3, 0, 7, c); // x0, y0, length, color
  matrix.drawFastHLine(0, 3, 7, c); // x0, y0, length, color
  matrix.drawPixel(3, 3, 0); // x, y, color
}

void SnowFlake2(uint32_t c) {
  matrix.drawFastVLine(3, 0, 7, c); // x0, y0, length, color
  matrix.drawFastHLine(0, 3, 7, c); // x0, y0, length, color
  matrix.drawLine(1, 1, 5, 5, c); // x0, y0, x1, y1, color
  matrix.drawLine(1, 5, 5, 1, c); // x0, y0, x1, y1, color
  matrix.drawPixel(3, 3, 0); // x, y, color
}

void SnowFlake3(uint32_t c) {
  matrix.drawLine(0, 2, 2, 0, c); // x0, y0, x1, y1, color
  matrix.drawLine(0, 4, 4, 0, c); // x0, y0, x1, y1, color
  matrix.drawLine(1, 5, 5, 1, c); // x0, y0, x1, y1, color
  matrix.drawPixel(3, 3, 0); // x, y, color
  matrix.drawRect(2, 2, 3, 3, c); // x0, y0, width, height
  matrix.drawLine(2, 6, 6, 2, c); // x0, y0, x1, y1, color
  matrix.drawLine(4, 6, 6, 4, c); // x0, y0, x1, y1, color
}

void SnowFlake4(uint32_t c) {
  matrix.drawRect(2, 2, 3, 3, c); // x0, y0, width, height
  matrix.drawLine(0, 3, 3, 0, c); // x0, y0, x1, y1, color
  matrix.drawLine(3, 6, 6, 3, c); // x0, y0, x1, y1, color
  matrix.drawLine(4, 1, 5, 2, c); // x0, y0, x1, y1, color
  matrix.drawLine(1, 4, 2, 5, c); // x0, y0, x1, y1, color
  matrix.drawPixel(0, 0, c); // x, y, color
  matrix.drawPixel(0, 6, c); // x, y, color
  matrix.drawPixel(6, 0, c); // x, y, color
  matrix.drawPixel(6, 6, c); // x, y, color

}

void SnowFlake5(uint32_t c) {
  matrix.fillRect(1, 1, 5, 5, c); // x0, y0, width, height
  matrix.drawLine(0, 3, 3, 0, c); // x0, y0, x1, y1, color
  matrix.drawLine(3, 6, 6, 3, c); // x0, y0, x1, y1, color
  matrix.drawLine(1, 3, 3, 1, 0); // x0, y0, x1, y1, color
  matrix.drawLine(3, 5, 5, 3, 0); // x0, y0, x1, y1, color
  matrix.drawPixel(2, 4, 0); // x, y, color
  matrix.drawPixel(4, 2, 0); // x, y, color

}

void SnowFlake6(uint32_t c) {
  matrix.fillRect(1, 1, 5, 5, c); // x0, y0, width, height
  matrix.drawLine(1, 1, 5, 5, 0); // x0, y0, x1, y1, color
  matrix.drawLine(1, 5, 5, 1, 0); // x0, y0, x1, y1, color
  matrix.drawPixel(0, 3, c); // x, y, color
  matrix.drawPixel(3, 0, c); // x, y, color
  matrix.drawPixel(3, 6, c); // x, y, color
  matrix.drawPixel(6, 3, c); // x, y, color
}

void SnowFlake7(uint32_t c) {
  matrix.drawRect(2, 2, 3, 3, c); // x0, y0, width, height
  matrix.drawFastVLine(3, 0, 7, c); // x0, y0, length, color
  matrix.drawFastHLine(0, 3, 7, c); // x0, y0, length, color
  matrix.drawFastVLine(3, 0, 7, c); // x0, y0, length, color
  matrix.drawFastHLine(0, 3, 7, c); // x0, y0, length, color
  matrix.drawPixel(3, 3, 0); // x, y, color
  matrix.drawFastHLine(2, 0, 3, c); // x0, y0, length, color
  matrix.drawFastHLine(2, 6, 3, c); // x0, y0, length, color
  matrix.drawFastVLine(0, 2, 3, c); // x0, y0, length, color
  matrix.drawFastVLine(6, 2, 3, c); // x0, y0, length, color
}

void SnowFlake8(uint32_t c) {
  //four corners
  matrix.drawPixel(0, 0, c); // x, y, color
  matrix.drawPixel(0, 6, c); // x, y, color
  matrix.drawPixel(6, 0, c); // x, y, color
  matrix.drawPixel(6, 6, c); // x, y, color

  //upper left corner
  matrix.drawLine(0, 2, 2, 0, c); // x0, y0, x1, y1, color
  matrix.drawLine(0, 3, 3, 0, c); // x0, y0, x1, y1, color

  //center dot
  matrix.drawPixel(3, 3, c); // x, y, color

  //upper right corner
  matrix.drawLine(4, 0, 6, 2, c); // x0, y0, x1, y1, color
  matrix.drawLine(3, 0, 6, 3, c); // x0, y0, x1, y1, color

  //lower left corner
  matrix.drawLine(0, 3, 3, 6, c); // x0, y0, x1, y1, color
  matrix.drawLine(0, 4, 2, 6, c); // x0, y0, x1, y1, color

  //lower right corner
  matrix.drawLine(3, 6, 6, 3, c); // x0, y0, x1, y1, color
  matrix.drawLine(4, 6, 6, 4, c); // x0, y0, x1, y1, color
}

void SnowFlake9(uint32_t c) {
  //four corners
  matrix.drawPixel(0, 0, c); // x, y, color
  matrix.drawPixel(0, 6, c); // x, y, color
  matrix.drawPixel(6, 0, c); // x, y, color
  matrix.drawPixel(6, 6, c); // x, y, color

  //center dot
  matrix.drawPixel(3, 3, c); // x, y, color

  //four boxes near center
  matrix.drawRect(1, 1, 2, 2, c); // x0, y0, width, height
  matrix.drawRect(4, 1, 2, 2, c); // x0, y0, width, height
  matrix.drawRect(1, 4, 2, 2, c); // x0, y0, width, height
  matrix.drawRect(4, 4, 2, 2, c); // x0, y0, width, height

  //clear out corner pixel of boxes
  matrix.drawPixel(1, 1, 0); // x, y, color
  matrix.drawPixel(5, 1, 0); // x, y, color
  matrix.drawPixel(1, 5, 0); // x, y, color
  matrix.drawPixel(5, 5, 0); // x, y, color
}

void SnowFlake10(uint32_t c) {
  //lines across the corners
  matrix.drawLine(0, 1, 1, 0, c); // x0, y0, x1, y1, color
  matrix.drawLine(5, 0, 6, 1, c); // x0, y0, x1, y1, color
  matrix.drawLine(0, 5, 1, 6, c); // x0, y0, x1, y1, color
  matrix.drawLine(5, 6, 6, 5, c); // x0, y0, x1, y1, color

  //center dot
  matrix.drawPixel(3, 3, c); // x, y, color

  //four boxes near center
  matrix.drawRect(1, 1, 2, 2, c); // x0, y0, width, height
  matrix.drawRect(4, 1, 2, 2, c); // x0, y0, width, height
  matrix.drawRect(1, 4, 2, 2, c); // x0, y0, width, height
  matrix.drawRect(4, 4, 2, 2, c); // x0, y0, width, height

  //clear out corner pixel of boxes
  matrix.drawPixel(1, 1, 0); // x, y, color
  matrix.drawPixel(5, 1, 0); // x, y, color
  matrix.drawPixel(1, 5, 0); // x, y, color
  matrix.drawPixel(5, 5, 0); // x, y, color
}

void SnowFlake11(uint32_t c) {
  //corner lines
  matrix.drawLine(0, 2, 2, 0, c); // x0, y0, x1, y1, color
  matrix.drawLine(4, 0, 6, 2, c); // x0, y0, x1, y1, color
  matrix.drawLine(0, 4, 2, 6, c); // x0, y0, x1, y1, color
  matrix.drawLine(4, 6, 6, 4, c); // x0, y0, x1, y1, color

  //center X
  matrix.drawLine(2, 2, 4, 4, c); // x0, y0, x1, y1, color
  matrix.drawLine(2, 4, 4, 2, c); // x0, y0, x1, y1, color

}

//8x8
void SnowFlake12(uint32_t c) {
  matrix.drawLine(1, 1, 6, 6, c); // x0, y0, x1, y1, color
  matrix.drawLine(1, 6, 6, 1, c); // x0, y0, x1, y1, color
  matrix.fillRect(3, 0, 2, 2, c); // x0, y0, width, height
  matrix.fillRect(0, 3, 2, 2, c); // x0, y0, width, height
  matrix.fillRect(6, 3, 2, 2, c); // x0, y0, width, height
  matrix.fillRect(3, 6, 2, 2, c); // x0, y0, width, height
  matrix.show();

}
