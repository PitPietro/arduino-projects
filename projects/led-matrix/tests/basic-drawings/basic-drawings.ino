#include <Adafruit_NeoPixel.h>
#include <Adafruit_GFX.h>
#include <Adafruit_NeoMatrix.h>

#define PIN 6 // pin on the Arduino connected to the NeoPixels
#define M_WIDTH 8 // matrix width
#define M_HEIGHT 8 // matrix height

// The 8x8 matrix uses 800 KHz (v2) pixels that expect GRB color data.
Adafruit_NeoMatrix matrix = Adafruit_NeoMatrix(8, 8, PIN, NEO_MATRIX_TOP + NEO_MATRIX_RIGHT + NEO_MATRIX_ROWS + NEO_MATRIX_ZIGZAG, NEO_GRB + NEO_KHZ800 );

uint32_t red = matrix.Color(255, 0, 0);
uint32_t yellow = matrix.Color(255, 255, 0);
uint32_t green = matrix.Color(0, 255, 0);
uint32_t blue = matrix.Color(0, 0, 255);
uint32_t white = matrix.Color(255, 255, 255);

/**
   Test Basic Drawings
   This sketch is based on Adafruit NeoPixel NeoMatrix 8x8 - 64 RGB LED Pixel Matrix: https://www.adafruit.com/product/1487
   Adafruit_NeoMatrix's drawing functions are taken from Adafruit_GFX library: https://learn.adafruit.com/adafruit-gfx-graphics-library/graphics-primitives
*/
void setup(void) {
  matrix.begin();
  matrix.setBrightness(255);

  matrix.fillScreen(0);
  matrix.show(); // send updated pixel colors to the hardware
}

void loop() {
  matrix.fillScreen(0);

  matrix.fillRect(0, 0, M_WIDTH, M_HEIGHT, yellow); // x0, y0, width, height, color
  rows_cols_test(red);
  rows_cols_test(green);
  rows_cols_test(blue);

  smile(yellow);
  smile(green);
  smile(blue);

  forbidden_access();

  matrix.show();
  delay(5000);
}

void smile(uint32_t base) {
  matrix.fillScreen(0);

  matrix.fillRect(0, 0, M_WIDTH, M_HEIGHT, base); // x0, y0, width, height, color
  matrix.fillRect(1, 1, 2, 2, white); // x0, y0, width, height, color
  matrix.fillRect(5, 1, 2, 2, white); // x0, y0, width, height, color
  matrix.drawLine(1, 4, 1, 5, white); // x0, y0, x1, y1, color
  matrix.drawPixel(2, 5, white); // x0, y0, color
  matrix.drawFastHLine(2, 6, 4, white); // x0, y0, length, color
  matrix.drawLine(6, 4, 6, 5, white); // x0, y0, x1, y1, color
  matrix.drawPixel(5, 5, white); // x0, y0, color
  delay(2000);

  matrix.show();
}

void forbidden_access() {
  matrix.fillScreen(0);

  matrix.fillRect(0, 0, M_WIDTH, M_HEIGHT, red); // x0, y0, width, height, color
  matrix.fillRect(1, 3, 6, 2, white); // x0, y0, width, height, color
  matrix.drawTriangle(0, 0, 0, 1, 1, 0, blue); // x0, y0, x1, y1, x2, y2, color
  matrix.drawTriangle(7, 7, 6, 7, 7, 6, blue); // x0, y0, x1, y1, x2, y2, color
  matrix.drawTriangle(0, 7, 0, 6, 1, 7, blue); // x0, y0, x1, y1, x2, y2, color
  matrix.drawTriangle(7, 0, 6, 0, 7, 1, blue); // x0, y0, x1, y1, x2, y2, color

  delay(1000);

  matrix.show();
}


void rows_cols_test(uint32_t c) {
  int i, j;

  for (j = 0; j < M_HEIGHT; j++) {
    for (i = 0; i < M_WIDTH; i++) {
      // matrix.fillScreen(0);
      matrix.drawPixel(i, j, c); // x0, y0, color
      delay(20);
      matrix.show();
    }
  }
}

/*
  void test_graphics_primitives(uint32_t c) {
  unsigned short int custom_delay = 1500;

  matrix.fillScreen(0);
  rows_cols_test(c);
  matrix.show();
  delay(50);

  matrix.fillScreen(0);
  // void drawLine(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, uint16_t color);
  matrix.drawLine(0, 2, 4, 7, c); // x0, y0, x1, y1, color
  matrix.show();
  delay(500);

  matrix.fillScreen(0);
  // void drawFastVLine(uint16_t x0, uint16_t y0, uint16_t length, uint16_t color);
  matrix.drawFastVLine(3, 2, 4, c); // x0, y0, length, color
  matrix.show();
  delay(500);

  matrix.fillScreen(0);
  // void drawFastHLine(uint8_t x0, uint8_t y0, uint8_t length, uint16_t color);
  matrix.drawFastHLine(0, 2, 4, c); // x0, y0, length, color
  matrix.show();
  delay(500);

  matrix.fillScreen(0);
  // void drawRect(uint16_t x0, uint16_t y0, uint16_t w, uint16_t h, uint16_t color);
  matrix.drawRect(0, 2, 4, 3, c); // x0, y0, width, height, color
  matrix.show();
  delay(custom_delay);

  matrix.fillScreen(0);
  // void fillRect(uint16_t x0, uint16_t y0, uint16_t w, uint16_t h, uint16_t color);
  matrix.fillRect(0, 1, 6, 4, c); // x0, y0, width, height, color
  matrix.show();
  delay(custom_delay);

  matrix.fillScreen(0);
  // void drawCircle(uint16_t x0, uint16_t y0, uint16_t r, uint16_t color);
  matrix.drawCircle(4, 4, 2, c); // x0, y0, radius, color
  matrix.show();
  delay(custom_delay);

  matrix.fillScreen(0);
  // void fillCircle(uint16_t x0, uint16_t y0, uint16_t r, uint16_t color);
  matrix.fillCircle(2, 3, 2, c); // x0, y0, radius, color
  matrix.show();
  delay(custom_delay);

  matrix.fillScreen(0);
  // void drawRoundRect(uint16_t x0, uint16_t y0, uint16_t w, uint16_t h, uint16_t radius, uint16_t color);
  matrix.drawRoundRect(1, 1, 5, 4, 2, c); // x0, y0, width, height, radius, color
  matrix.show();
  delay(custom_delay);

  matrix.fillScreen(0);
  // void fillRoundRect(uint16_t x0, uint16_t y0, uint16_t r, uint16_t color);
  matrix.fillRoundRect(2, 2, 5, 4, 4, c); // x0, y0, width, height, radius, color
  matrix.show();
  delay(custom_delay);

  matrix.fillScreen(0);
  // void drawTriangle(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t color);
  matrix.drawTriangle(0, 0, 3, 3, 0, 3, c); // x0, y0, x1, y1, x2, y2, color
  matrix.show();
  delay(custom_delay);

  matrix.fillScreen(0);
  // void fillTriangle(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t color);
  matrix.fillTriangle(0, 4, 3, 4, 0, 7, c); // x0, y0, x1, y1, x2, y2, color
  matrix.show();
  delay(custom_delay);
  }
*/
