/*
   Documentation: https://nRF24.github.io/RF24

   A simple example of sending data from 1 nRF24L01 transceiver to another.

   This example was written to be used on 2 devices acting as "nodes".
   Use the Serial Monitor to change each node's behavior.
*/
#include <SPI.h>
#include "printf.h"
#include "RF24.h"

// instantiate an object for the nRF24L01 transceiver
RF24 radio(9, 10); // using pin 9 for the CE pin, and pin 10 for the CSN pin

// Let these addresses be used for the pair
uint8_t address[][6] = {"1Node", "2Node"};
// It is very helpful to think of an address as a path instead of as
// an identifying device destination

// to use different addresses on a pair of radios, we need a variable to
// uniquely identify which address this radio will use to transmit
bool radioNumber = 1; // 0 uses address[0] to transmit, 1 uses address[1] to transmit

// Used to control whether this node is sending or receiving
bool isSender = false;  // true = TX role, false = RX role

// For this example, we'll be using a payload containing
// a single float number that will be incremented
// on every successful transmission
float payload = 0.0;

// reset the board software-side
void(* resetFunc) (void) = 0;

void setup() {
  // initialize serial communication at 9600 baud rate
  Serial.begin(9600);
  // wait for serial port to connect. Needed for native USB port only.
  while (!Serial)
    ;

  // initialize the transceiver on the SPI bus
  if (!radio.begin()) {
    Serial.println("radio hardware is not responding!");
    while (1) {} // hold in infinite loop
  }

  // To set the radioNumber via the Serial monitor on startup

  Serial.println("\n# is this device a sender (TX) or a receiver (RX)?");
  while (!Serial.available()) {
    // wait for user input
  }
  String input = Serial.readString();
  Serial.print("This device is ");
  Serial.println(input);

  // since the Serial Monitor is by default set to 'Newline', you need to add '\n' to the string
  // you could also change 'Newline' to 'No line ending' everytime you open the Serial Monitor
  if (input == "TX\n") {
    isSender = true;
  } else if (input == "RX\n") {
    isSender = false;
  } else {
    Serial.println("Nor 'TX' or 'RX' have been selected: resetting the board");

    // reset the board and start over the programm
    resetFunc();
  }


  if (isSender) {
    radioNumber = 1;

    // inform the sender that it can start being a receiver by pressing 'R' on the Serial Monitor
    Serial.println("# Change behaviour: press 'R' to begin receiving from the other node");
  } else {
    radioNumber = 0;

    // inform the receiver that it can start being a sender by pressing 'T' on the Serial Monitor
    Serial.println("# Change behaviour: press 'T' to begin transmitting to the other node");
  }

  // Set the PA Level low to try preventing power supply related problems
  // because these examples are likely run with nodes in close proximity to
  // each other.
  radio.setPALevel(RF24_PA_LOW);  // RF24_PA_MAX is default.

  // save on transmission time by setting the radio to only transmit the
  // number of bytes we need to transmit a float
  radio.setPayloadSize(sizeof(payload)); // float datatype occupies 4 bytes

  // set the TX address of the RX node into the TX pipe
  radio.openWritingPipe(address[radioNumber]); // always uses pipe 0

  // set the RX address of the TX node into a RX pipe
  radio.openReadingPipe(1, address[!radioNumber]); // using pipe 1

  // additional setup specific to the node's role
  if (isSender) {
    // put radio in TX mode
    radio.stopListening();
  } else {
    // put radio in RX mode
    radio.startListening();
  }

  // debugging info
  Serial.println("### start debugging info ###");
  printf_begin();             // needed only once for printing details
  // radio.printDetails();       // (smaller) function that prints raw register values
  radio.printPrettyDetails(); // (larger) function that prints human readable data
  Serial.println("### end debugging info ###\n");
}

void loop() {

  if (isSender) {
    // this device is a TX node

    unsigned long start_timer = micros();                    // start the timer
    bool report = radio.write(&payload, sizeof(float));      // transmit & save the report
    unsigned long end_timer = micros();                      // end the timer

    if (report) {
      Serial.print("Transmission successful! "); // payload was delivered
      Serial.print("Time to transmit = ");
      Serial.print(end_timer - start_timer); // print the timer result
      Serial.print(" us. Sent: ");
      Serial.println(payload); // print payload sent
      payload += 0.01;                                       // increment float payload
    } else {
      // Serial.println("Transmission failed or timed out"); // payload was not delivered
    }

    // to make this example readable in the serial monitor
    delay(1000);  // slow transmissions down by 1 second

  } else {
    // this device is a RX node

    uint8_t pipe;

    // if there is a payload, get the pipe number that recieved it
    if (radio.available(&pipe)) {
      // get the size of the payload
      uint8_t bytes = radio.getPayloadSize();
      radio.read(&payload, bytes);            // fetch payload from FIFO
      Serial.print("Received ");
      Serial.print(bytes);                    // print the size of the payload
      Serial.print(" bytes on pipe ");
      Serial.print(pipe);                     // print the pipe number
      Serial.print(": ");
      Serial.println(payload);                // print the payload's value
    }
  }

  // change the role via the serial monitor
  if (Serial.available()) {
    char c = toupper(Serial.read());
    if (c == 'T' && !isSender) {
      // become the TX node

      isSender = true;
      Serial.println("# Change to Transmit Role (TX) ~ press 'R' to switch back");
      radio.stopListening();
    } else if (c == 'R' && isSender) {
      // become the RX node

      isSender = false;
      Serial.println("# Change to Receive Tole (RX) ~ press 'T' to switch back");
      radio.startListening();
    }
  }
}
