# TX log

```bash
# is this device a sender (TX) or a receiver (RX)?
```

Press `TX` on the Serial Monitor.

```bash
This device is TX

# Change behaviour: press 'R' to begin receiving from the other node
### start debugging info ###
SPI Frequency = 10 Mhz
Channel = 76 (~ 2476 MHz)
RF Data Rate = 1 MBPS
RF Power Amplifier = PA_LOW
RF Low Noise Amplifier = Enabled
CRC Length = 16 bits
Address Length = 5 bytes
Static Payload Length = 4 bytes
Auto Retry Delay = 1500 microseconds
Auto Retry Attempts = 15 maximum
Packets lost on current channel = 0
Retry attempts made for last transmission = 0
Multicast = Disabled
Custom ACK Payload = Disabled
Dynamic Payloads = Disabled
Auto Acknowledgment = Enabled
Primary Mode = TX
TX address = 0x65646f4e32
pipe 0 ( open ) bound = 0x65646f4e32
pipe 1 ( open ) bound = 0x65646f4e31
pipe 2 (closed) bound = 0xc3
pipe 3 (closed) bound = 0xc4
pipe 4 (closed) bound = 0xc5
pipe 5 (closed) bound = 0xc6
### end debugging info ###

Transmission successful! Time to transmit = 552 us. Sent: 0.00
Transmission successful! Time to transmit = 552 us. Sent: 0.01
Transmission successful! Time to transmit = 552 us. Sent: 0.02
Transmission successful! Time to transmit = 552 us. Sent: 0.03
Transmission successful! Time to transmit = 552 us. Sent: 0.04
Transmission successful! Time to transmit = 552 us. Sent: 0.05
Transmission successful! Time to transmit = 552 us. Sent: 0.06
Transmission successful! Time to transmit = 552 us. Sent: 0.07
Transmission successful! Time to transmit = 552 us. Sent: 0.08
Transmission successful! Time to transmit = 552 us. Sent: 0.09
Transmission successful! Time to transmit = 552 us. Sent: 0.10
Transmission successful! Time to transmit = 552 us. Sent: 0.11
```

Press `R` on the Serial Monitor.

```bash
# Change to Receive Tole (RX) ~ press 'T' to switch back
Received 4 bytes on pipe 0: 0.00
Received 4 bytes on pipe 0: 0.00
Received 4 bytes on pipe 0: 0.00
Received 4 bytes on pipe 0: 0.00
Received 4 bytes on pipe 0: 0.00
Received 4 bytes on pipe 0: 0.00
Received 4 bytes on pipe 1: 0.11
Received 4 bytes on pipe 1: 0.12
Received 4 bytes on pipe 1: 0.13
Received 4 bytes on pipe 1: 0.14
Received 4 bytes on pipe 1: 0.15
Received 4 bytes on pipe 1: 0.16
Received 4 bytes on pipe 1: 0.17
Received 4 bytes on pipe 1: 0.18
Received 4 bytes on pipe 1: 0.19
Received 4 bytes on pipe 1: 0.20
```
