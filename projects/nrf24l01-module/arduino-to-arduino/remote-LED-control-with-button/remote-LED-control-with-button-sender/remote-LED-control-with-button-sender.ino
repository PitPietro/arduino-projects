/*
   Documentation: https://nRF24.github.io/RF24

   Sender - Send the state of the button attached to digital pin 8
*/
#include <SPI.h>
#include "RF24.h"

// instantiate an object for the nRF24L01 transceiver
RF24 radio(9, 10); // using pin 9 for the CE pin, and pin 10 for the CSN pin

const byte address[6] = "00001";

// define the pin number to which the button is connected
const short int button_pin = 8;

// initialize the button state to LOW
bool button_state = false;

void setup() {
  // initialize serial communication at 9600 baud rate
  Serial.begin(9600);
  // do not wait for serial port to connect. Let it works even if the user does not open the Serial Monitor

  // initialize the transceiver on the SPI bus
  if (!radio.begin()) {
    Serial.println("radio hardware is not responding!");
    while (1) {} // hold in infinite loop
  }

  // To set the radioNumber via the Serial monitor on startup
  Serial.print("# This device is a sender ~ TX");

  // Set the PA Level low to try preventing power supply related problems
  // because these examples are likely run with nodes in close proximity to
  // each other.
  radio.setPALevel(RF24_PA_LOW);  // RF24_PA_MAX is default.

  // save on transmission time by setting the radio to only transmit the
  // number of bytes we need to transmit a boolean
  // radio.setPayloadSize(sizeof(button_pin));

  // set the TX address of the RX node into the TX pipe
  radio.openWritingPipe(address);

  // stop listening for incoming data
  radio.stopListening();

  // se the button pin as digital input
  pinMode(button_pin, INPUT);
}

void loop() {
  radio.stopListening();

  // read the state of the button
  button_state = digitalRead(button_pin);

  bool report = radio.write(&button_state, sizeof(button_state));

  if (report) {
    // print payload sent
    Serial.print("Sent button state: ");
    Serial.println(button_state);
  }

  // slow transmissions down by 10 milliseconds
  delay(10);
}
