/*
   Servo Motors Receiver

   Receive the position of the joystick and move the servo motors consequently

   Servo motors wiring diagram:
   - GND  --> GND (black wire)
   - 5V   --> VCC (red wire)
   - PWM  --> DATA IN (white wire)

   nRF24l01 wiring diagram:
   - GND --> GND
   - D13 --> SCK
   - D12 --> MISO
   - D11 --> MOSI
   - D10 --> CSN
   - D09 --> CE
   -  /  --> IRQ
   - 3V3 --> VCC

   Docs: https://github.com/nRF24/RF24
*/
#include <Servo.h>
#include <SPI.h>
#include "RF24.h"
#include "printf.h"

#define DIR_1 0
#define STOP 80
#define DIR_2 180
// deeded for diagonals
#define OFFSET 40

Servo sx_servo, dx_servo;

enum position {
  NONE,       // x =  510 ; y =  510
  UP,         // x =  510 ; y =    0
  DOWN,       // x =  510 ; y = 1023
  LEFT,       // x =    0 ; y =  510
  RIGHT,      // x = 1023 ; y =  510
  UP_RIGHT,   // x = 1023 ; y =    0
  UP_LEFT,    // x =    0 ; y =    0
  DOWN_RIGHT, // x = 1023 ; y = 1023
  DOWN_LEFT,  // x =    0 ; y = 1023
};

position joystick_position;

// instantiate an object for the nRF24L01 transceiver
// pin 09 for CE and pin 10 for CSN
RF24 radio(9, 10);

const byte address[6] = "00001";

void setup() {
  // initialize serial communication at 9600 baud rate
  Serial.begin(9600);

  // attach sx servo to digital pin 06
  sx_servo.attach(6);
  // attach dx servo to digital pin 03
  dx_servo.attach(3);

  // stop them is they were not
  sx_servo.write(STOP);
  dx_servo.write(STOP);

  // initialize the transceiver on the SPI bus
  if (!radio.begin()) {
    Serial.println("radio hardware is not responding!");
    while (1) {}  // hold in infinite loop
  }

  Serial.println("# This device is a sender ~ TX");

  // Set the PA Level low to try preventing power supply related problems since
  // these examples are likely run with nodes in close proximity to each other.
  radio.setPALevel(RF24_PA_LOW);  // RF24_PA_MAX is default.

  // set the RX address of the TX node into a RX pipe
  radio.openReadingPipe(0, address);

  // start listening for incoming data
  radio.startListening();

  // debugging info
  Serial.println("### start debugging info ###");
  printf_begin();  // needed only once for printing details
  // radio.printDetails();       // (smaller) function that prints raw register values
  radio.printPrettyDetails();  // (larger) function that prints human readable data
  Serial.println("### end debugging info ###\n");
}

void loop() {
  radio.startListening();

  // while there is nothing available from the radio, do nothing
  while (!radio.available());

  // read the payload
  radio.read(&joystick_position, sizeof(joystick_position));

  Serial.print("Received state: ");
  Serial.println(joystick_position);

  switch (joystick_position) {
    case 0: {
        // NONE
        sx_servo.write(STOP);
        dx_servo.write(STOP);
        break;
      };
    case 1: {
        // UP
        sx_servo.write(DIR_2);
        dx_servo.write(DIR_1);
        break;
      };
    case 2: {
        // DOWN
        sx_servo.write(DIR_1);
        dx_servo.write(DIR_2);
        break;
      };
    case 3: {
        // LEFT
        sx_servo.write(STOP);
        dx_servo.write(DIR_1);
        break;
      };
    case 4: {
        // RIGHT
        sx_servo.write(DIR_2);
        dx_servo.write(STOP);
        break;
      };
    case 5: {
        // UP_RIGH
        sx_servo.write(DIR_2);
        dx_servo.write(DIR_1 + OFFSET);
        break;
    };
    case 6: {
        // UP_LEFT
        sx_servo.write(DIR_2 - OFFSET);
        dx_servo.write(DIR_1);
        break;
      };
    case 7: {
        // DOWN_RIGHT
        sx_servo.write(DIR_1);
        dx_servo.write(DIR_2 - OFFSET);
        break;
      };
    case 8: {
        // DOWN_LEFT
        sx_servo.write(DIR_1 + OFFSET);
        dx_servo.write(DIR_2);
        break;
      };
      default: {
        // do nothing
      }
  }

  delay(50);
}
