/*
   Arduino Simple Payloads

   send/receive different types of payloads through the nRF24L01 wireless module from/to a Raspberry Pi
   or any other Linux-based SoC computers

   Docs:
   1. https://github.com/nRF24/RF24

   Tutorial: https://pietropoluzzi.it/blog/hardware/wireless-communication-nrf24l01-module/
*/
#include <SPI.h>
#include "printf.h"
#include "RF24.h"

// use 'typedef' to define different versions for the payload data type to comment/uncomment when needed
typedef char payload_type; // uncomment if the payload data type is 'char'
// typedef bool payload_type; // uncomment if the payload data type is 'bool'
// typedef short payload_type; // uncomment if the payload data type is 'short'
// typedef int payload_type; // uncomment if the payload data type is 'int'
// typedef long payload_type; // uncomment if the payload data type is 'long'
// typedef float payload_type; // uncomment if the payload data type is 'float'
// typedef double payload_type; // uncomment if the payload data type is 'double'

// instantiate an object for the nRF24L01 transceiver
RF24 radio(9, 10); // using pin 9 for the CE pin, and pin 10 for the CSN pin

// Let these addresses be used for the pair
uint8_t address[][6] = {"1Node", "2Node"};
// It is very helpful to think of an address as a path instead of as
// an identifying device destination

// to use different addresses on a pair of radios, we need a variable to
// uniquely identify which address this radio will use to transmit
bool radioNumber = 1; // 0 uses address[0] to transmit, 1 uses address[1] to transmit

// Used to control whether this node is sending or receiving
bool isSender = false;  // true = TX role, false = RX role

// change the default value of the payload according to 'payload_type'
payload_type payload = 'a'; // 0.0; // 0; // true;

// reset the board software-side
void(* resetFunc) (void) = 0;

void setup() {
  // initialize serial communication at 9600 baud rate
  Serial.begin(9600);
  // wait for serial port to connect. Needed for native USB port only.
  while (!Serial)
    ;

  // initialize the transceiver on the SPI bus
  if (!radio.begin()) {
    Serial.println("radio hardware is not responding!");
    while (1) {} // hold in infinite loop
  }

  // set the radioNumber via the Serial monitor on startup
  Serial.println("\n# is this device a sender (TX) or a receiver (RX)?");
  while (!Serial.available()) {
    // wait for user input
  }
  String input = Serial.readString();
  Serial.print("This device is ");
  Serial.println(input);

  // since the Serial Monitor is by default set to 'Newline', you need to add '\n' to the string
  // you could also change 'Newline' to 'No line ending' everytime you open the Serial Monitor
  if (input == "TX\n") {
    isSender = true;
  } else if (input == "RX\n") {
    isSender = false;
  } else {
    Serial.println("Nor 'TX' or 'RX' have been selected: resetting the board");

    // reset the board and start over the programm
    resetFunc();
  }

  if (isSender) {
    radioNumber = 1;
  } else {
    radioNumber = 0;
  }

  // Set the PA Level low to try preventing power supply related problems because these examples are likely run with nodes in close proximity to
  // each other.
  radio.setPALevel(RF24_PA_LOW); // RF24_PA_MAX is default.

  // save on transmission time by setting the radio to only transmit the number of bytes we need to transmit
  radio.setPayloadSize(sizeof(payload));

  // set the TX address of the RX node into the TX pipe
  radio.openWritingPipe(address[radioNumber]); // always uses pipe 0

  // set the RX address of the TX node into a RX pipe
  radio.openReadingPipe(1, address[!radioNumber]);

  // additional setup specific to the node's role
  if (isSender) {
    // put radio in TX mode
    radio.stopListening();
  } else {
    // put radio in RX mode
    radio.startListening();
  }

  // debugging info
  Serial.println("### start debugging info ###");
  printf_begin(); // needed only once for printing details
  // radio.printDetails(); // (smaller) function that prints raw register values
  radio.printPrettyDetails(); // (larger) function that prints human readable data
  Serial.println("### end debugging info ###\n");
}

void loop() {
  if (isSender) {
    // this device is a TX node

    unsigned long start_timer = micros(); // start the timer
    // transmit and save the report
    bool report = radio.write(&payload, sizeof(payload_type));
    unsigned long end_timer = micros(); // end the timer

    if (report) {
      Serial.print("Transmission successful! "); // payload was delivered
      Serial.print("Time to transmit = ");
      Serial.print(end_timer - start_timer); // print the timer result
      Serial.print(" us. Sent: ");
      Serial.println(payload); // print payload sent

      // if the value has been delivered, increment the payload
      increment_payload();
    } else {
      // payload was not delivered
      // Serial.println("Transmission failed or timed out");
    }

    // make this example readable in the serial monitor
    delay(1000); // slow transmissions down by 1 second
  } else {
    // this device is a RX node

    uint8_t pipe;

    // if there is a payload, get the pipe number that recieved it
    if (radio.available(&pipe)) {
      // get the size of the payload
      uint8_t bytes = radio.getPayloadSize();

      // fetch payload from FIFO
      radio.read(&payload, bytes);

      Serial.print("Received ");
      Serial.print(bytes); // print the size of the payload
      Serial.print(" bytes on pipe ");
      Serial.print(pipe); // print the pipe number
      Serial.print(": ");
      Serial.println(payload); // print the payload's value
    }
  }
}

void increment_payload() {
  payload += 1; // uncomment if you need to increment a 'char' or 'short' or 'int' or 'long' payload
  // payload = !payload; // uncomment if you need to increment a 'bool' payload
  // payload += 0.01; // uncomment if you need to increment a 'float' or 'double' payload
}
