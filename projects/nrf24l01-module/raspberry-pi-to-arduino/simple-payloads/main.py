import time
import board
import struct
import argparse
from digitalio import DigitalInOut

parser = argparse.ArgumentParser(description='Choose to be sender or receiver.')
parser.add_argument('-r', '--role', choices=['TX', 'RX'], required=True,
                    help='Select TX to be a sender device. Select RX to be a receiver device.')

args = parser.parse_args()

# prints: 'Namespace(...)' with the arguments inside
# print(args)

is_sender = None

if args.role == 'TX':
    is_sender = True
elif args.role == 'RX':
    is_sender = False

# if running this on a ATSAMD21 M0 based board
# from circuitpython_nrf24l01.rf24_lite import RF24
from circuitpython_nrf24l01.rf24 import RF24

# https://circuitpython-nrf24l01.readthedocs.io/en/latest/#pinout
# invalid default values for scoping
SPI_BUS, CSN_PIN, CE_PIN = (None, None, None)

# using board.SPI() automatically selects the MCU's
SPI_BUS = board.SPI()  # init spi bus object

# change these (digital output) pins accordingly
CE_PIN = DigitalInOut(board.D4)
CSN_PIN = DigitalInOut(board.D5)


# initialize the nRF24L01 on the spi bus object
nrf = RF24(SPI_BUS, CSN_PIN, CE_PIN)
# On Linux, csn value is a bit coded
#   0 = bus 0, CE0  # SPI bus 0 is enabled by default
#   10 = bus 1, CE0  # enable SPI bus 2 prior to running this
#   21 = bus 2, CE1  # enable SPI bus 1 prior to running this

# set the Power Amplifier level to -12 dBm since this test example is
# usually run with nRF24L01 transceivers in close proximity
nrf.pa_level = -12

# addresses needs to be in a buffer protocol object (bytearray)
address = [b"1Node", b"2Node"]

# to use different addresses on a pair of radios, you need a variable to
# uniquely identify which address this radio will use to transmit

radio_number = None

if is_sender:
    radio_number = 1
else:
    radio_number = 0

# set TX address of RX node into the TX pipe
nrf.open_tx_pipe(address[radio_number])  # always uses pipe 0

# set RX address of TX node into an RX pipe
nrf.open_rx_pipe(1, address[not radio_number])

# using the python keyword global is bad practice.
# instead, use a 1 item list to store the payload to send

payload = ['a'] # uncomment if you need to send/receive a 'char' payload
# payload = [True] # uncomment if you need to send/receive a 'bool' payload
# payload = [0.0] # uncomment if you need to send/receive a 'float' or 'double' payload
# payload = [0] # uncomment if you need to send/receive a 'short' or 'int' or 'long' payload

# struct.pack() format for the different data types of the payloads
format_string = "<" # little-endian

pack_format = format_string + "c" # uncomment if you need to pack/unpack a 'char' payload
# pack_format = format_string + "?" # uncomment if you need to pack/unpack a 'bool' payload
# pack_format = format_string + "h" # uncomment if you need to pack/unpack a 'short' payload
# pack_format = format_string + "i" # uncomment if you need to pack/unpack a 'int' payload
# pack_format = format_string + "l" # uncomment if you need to pack/unpack a 'long' payload
# pack_format = format_string + "f" # uncomment if you need to pack/unpack a 'float' payload

# size of the struct: the bytes object produced by struct.pack(...) function
struct_size = struct.calcsize(pack_format)

def sender(count=20):  # transmit 'count' number of packets
    """
    Transmits an incrementing payload every second
    """

    # adds compatibility with Arduino boards
    nrf.allow_ask_no_ack = False
    nrf.dynamic_payloads = False
    nrf.payload_length = struct_size

    # ensures the nRF24L01 is in TX mode
    nrf.listen = False

    while count:
        # use struct.pack to packetize your data into a usable payload

        # 'char' data type is seen as bytes of length 1 from Python
        payload[0] = bytes(payload[0], 'UTF-8') # uncomment if you need to pack a 'char' payload

        buffer = struct.pack(pack_format, payload[0])
        
        start_timer = time.monotonic_ns()  # start timer
        result = nrf.send(buffer)
        # print('send result: ', result)
        end_timer = time.monotonic_ns()  # end timer
        if not result:
            print("send() failed or timed out")
        else:
            print(
                "Transmission successful! Time to Transmit:",
                f"{(end_timer - start_timer) / 1000} us. Sent: {payload[0]}"
            )
            increment_payload()
        time.sleep(1)
        count -= 1


def receiver(timeout=600):
    """
    Polls the radio and prints the received value.
    This method expires after 'timeout' seconds of no received transmission.
    """

    # adds compatibility with Arduino boards
    nrf.allow_ask_no_ack = False
    nrf.dynamic_payloads = False
    nrf.payload_length = struct_size

    # put radio into RX mode and power up
    nrf.listen = True

    start = time.monotonic()
    while (time.monotonic() - start) < timeout:
        
        if nrf.available():
            # grab information about the received payload
            payload_size, pipe_number = (nrf.any(), nrf.pipe)
            
            # fetch 1 payload from RX FIFO
            # also clears nrf.irq_dr status flag
            buffer = nrf.read()
            
            # buffer[:struct_size] truncates padded 0s if dynamic payloads are disabled (and they are disabled)
            
            payload[0] = struct.unpack(pack_format, buffer[:struct_size])[0]

            # print details about the received packet
            print(f"Received {payload_size} bytes on pipe {pipe_number}: {payload[0]}")
            start = time.monotonic()

    print('timeout is expired')

    # recommended behavior is to keep in TX mode while idle
    # put the nRF24L01 is in TX mode
    nrf.listen = False


def increment_payload(increment_size = 1):
    """
    'increment_size' is needed to test large payloads like 'long' and 'long long'.
    """

    # 'chr()' function produce Unicode character from a 'long' number
    # 'ord()' function produce a 'long' number (Python integer) from a Unicode character

    payload[0] = chr(ord(payload[0]) + 1) # uncomment if you need to increment a 'char' payload
    # payload[0] = not payload[0]         # uncomment if you need to increment a 'bool' payload
    # payload[0] += 0.01                  # uncomment if you need to increment a 'float' or 'double' payload
    # payload[0] += increment_size        # uncomment if you need to increment a 'short' or 'int' or 'long' payload

# select which method to call
if is_sender:
    sender()
else:
    receiver()

# run the code from 'nrf24l01' project folder
# cd raspberry-pi-to-arduino/simple-payloads/

# possible ways to run the programm
# python3 main.py -r TX
# python3 main.py -r RX
# python3 main.py --role TX
# python3 main.py --role RX