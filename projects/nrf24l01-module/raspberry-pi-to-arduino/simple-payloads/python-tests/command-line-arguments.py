import sys

print('total number of arguments:', format(len(sys.argv)))

# print all arguments
print('argument list:', str(sys.argv))
 
# Print arguments one by one
print('1st argument:', str(sys.argv[0]))
print('2nd argument:', str(sys.argv[1]))
print('3rd argument:', str(sys.argv[2]))
print('4th argument:', str(sys.argv[3]))

# the first argument is always the script itself 

# run the code from 'nrf24l01' project folder
# cd raspberry-pi-to-arduino/simple-payloads/python-tests
# python3 command-line-arguments.py hello 2.3 "s"