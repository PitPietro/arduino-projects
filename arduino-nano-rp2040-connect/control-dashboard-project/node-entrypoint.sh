#!/usr/bin/env sh

set -e

# install the dependencies
npm install

# continue with the default entrypoint behavior
exec "$@"