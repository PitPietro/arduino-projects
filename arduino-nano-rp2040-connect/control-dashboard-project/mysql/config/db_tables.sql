CREATE TABLE IF NOT EXISTS imu (
  id INT PRIMARY KEY AUTO_INCREMENT,
  acc_x FLOAT NOT NULL COMMENT 'Accelerometer on x axes',
  acc_y FLOAT NOT NULL COMMENT 'Accelerometer on y axes',
  acc_z FLOAT NOT NULL COMMENT 'Accelerometer on z axes',
  gyro_x FLOAT NOT NULL COMMENT 'Gyroscope on x axes',
  gyro_y FLOAT NOT NULL COMMENT 'Gyroscope on y axes',
  gyro_z FLOAT NOT NULL COMMENT 'Gyroscope on z axes',
  time BIGINT UNSIGNED NOT NULL COMMENT 'time when the data was captured'
);

CREATE TABLE IF NOT EXISTS temperature (
  id INT PRIMARY KEY AUTO_INCREMENT,
  temp_c FLOAT NOT NULL COMMENT 'Temperature in Celsius from IMU chip',
  time BIGINT UNSIGNED NOT NULL COMMENT 'time when the data was captured'
);

-- TODO to test ---
CREATE TABLE IF NOT EXISTS microphone (
  id INT PRIMARY KEY AUTO_INCREMENT,
  left TINYINT NOT NULL COMMENT 'Left microphone channel',
  right TINYINT NOT NULL COMMENT 'Right microphone channel',
  time BIGINT UNSIGNED NOT NULL COMMENT 'time when the data was captured'
);

-- https://dev.mysql.com/doc/refman/8.0/en/integer-types.html --