#include <Arduino_LSM6DSOX.h>  // https://www.arduino.cc/reference/en/libraries/arduino_lsm6dsox/
#include <ArduinoJson.h>       // https://arduinojson.org/
#include <WiFiNINA.h>          // https://www.arduino.cc/en/Reference/WiFiNINA
#include <MQTT.h>              // https://www.arduino.cc/reference/en/libraries/mqtt/
#include "arduino-secrets.h"

MQTTClient mqtt_client;
WiFiClient wifi_client;

// MQTT broker port is 1883 by default, but you can change it
const short int broker_port = 1883;

// Wifi radio's status
int status = WL_IDLE_STATUS;

float acc_x, acc_y, acc_z, gyr_x, gyr_y, gyr_z;
float acc_sample_rate, gyr_sample_rate;
double refresh_rate;

/**
   Send IMU Data to MQTT ~ Arduino Nano RP2040 Connect

  Press Ctrl + Shift + N and create 'arduino-secrets.h' file as follow:
  ```c
   #define WIFI_SSID "" // place your WiFi name inside the quotes
   #define WIFI_PWD "" // place your WiFi password inside the quotes
   #define BROKER_IP "192.168.x.x" // place MQTT Broker IP address inside the quotes
   #define DEV_NAME  "" // place MQTT Client ID name inside the quotes
   #define MQTT_USER "" // place MQTT username inside the quotes (leave empty if MQTT is unauthenticated)
   #define MQTT_PWD  "" // place MQTT password inside the quotes (leave empty if MQTT is unauthenticated)
   ```

   Note: 'arduino-secrets.h' files are indicated in '.gitignore' to avoid loose of sensitive data.

   If you can't manage to get the <u>Arduino Nano RP2040 Connect</u> working, try to run this command:
   ```shell
   sudo ~/.arduino15/packages/arduino/hardware/mbed_nano/4.0.2/post-install.sh
   ```

   An accelerometer is an electromechanical device that measures acceleration forces.
   A gyroscope sensor measures and maintains orientation and angular speed of an object.

   Gyroscopes are more advanced than accelerometers: they can measure the tilt and lateral
   orientation of an object, whereas an accelerometer can only measure its linear motion.

   Docs: https://docs.arduino.cc/tutorials/nano-rp2040-connect/rp2040-imu-basics
*/
void setup() {
  // initialize serial communication and wait for port to open
  Serial.begin(9600);
  // wait for serial port to connect. Needed for native USB port only
  while (!Serial)
    ;

  // initialize the IMU, return 1 on success and 0 on failure
  // de-initialize the IMU with 'IMU.end()' void function
  if (!IMU.begin()) {
    Serial.println("»»» Failed to initialize IMU «««");
    while (1)
      ;
  }

  acc_sample_rate = IMU.accelerationSampleRate();
  gyr_sample_rate = IMU.gyroscopeSampleRate();

  // print on Serial Monitor IMU's accelerometer sample rate in Hz (Hertz)
  Serial.print("»»» Accelerometer sample rate = ");
  Serial.print(acc_sample_rate); // should be 104.00 Hz
  Serial.println("Hz");

  // print on Serial Monitor IMU's gyroscope sample rate in Hz (Hertz)
  Serial.print("»»» Gyroscope sample rate = ");
  Serial.print(gyr_sample_rate); // should be 104.00 Hz
  Serial.println("Hz");
  Serial.println();

  if(acc_sample_rate != gyr_sample_rate) {
    Serial.println("»»» Sample rates are not equal, taking accelerometer as reference");
  }

  // check for the WiFi module
  if (WiFi.status() == WL_NO_MODULE) {
    Serial.println("»»» Communication with WiFi module failed! «««");
    while (true)
      ;  // do not continue
  }

  // attempt to connect to Wifi network
  while (status != WL_CONNECTED) {
    Serial.print("»»» Attempting to connect to network: ");
    Serial.println(WIFI_SSID);

    // connect to WPA/WPA2 network
    status = WiFi.begin(WIFI_SSID, WIFI_PWD);

    // wait for connection
    delay(50);
  }

  // the board is now connected, print out the data
  Serial.println("»»» You're connected to the network");
  print_wifi_status();

  // MQTT brokers usually use port 8883 for secure connections
  mqtt_client.begin(BROKER_IP, broker_port, wifi_client);
  mqtt_client.onMessage(message_handler);
  connect();
}

void loop() {
  mqtt_client.loop();
  static auto lastCheck = millis();

  if (!mqtt_client.connected()) {
    connect();
  }
  
  // refresh rate in milliseconds
  refresh_rate = (1/acc_sample_rate)*1000;
  // Serial.print("»»» Refresh rate = ");
  // Serial.println(refresh_rate);
  
  // ~~~ START update function (should be continuously polled) ~~~
  // query if new acceleration data from the IMU is available
  
  // if (IMU.accelerationAvailable()) {

    // read the relative position of the board
    // query the IMU's accelerometer and return the acceleration in g's
    // IMU.readAcceleration(acc_x, acc_y, acc_z);
  // s}

  // query if new gyroscope data from the IMU is available
  // if (IMU.gyroscopeAvailable()) {

    // query the IMU's gyroscope and return the angular speed in dps (degrees per second)
    // IMU.readGyroscope(gyr_x, gyr_y, gyr_z);
  // }
  // ~~~ END update function ~~~

  // check sensor values every acc_sample_rate
  if (millis() - lastCheck >= refresh_rate) {
    lastCheck = millis();

    // if no new acceleration nor gyroscope data is available, break the loop
    if (!IMU.accelerationAvailable() || !IMU.gyroscopeAvailable()) {
      Serial.println("»« No data available");
      return;
    }

    Serial.println("»« I am in");

    // return the acceleration in g's
    IMU.readAcceleration(acc_x, acc_y, acc_z);

    // return the angular speed in dps (degrees per second)
    IMU.readGyroscope(gyr_x, gyr_y, gyr_z);

    boolean rc;

    float imu_values[6] = { acc_x, acc_y, acc_z, gyr_x, gyr_y, gyr_z };

    send_imu(imu_values, "imu");
  }
}

/*
Send Arduino's 6-axis IMU JSON values to MQTT broker
*/
boolean send_imu(float* values, char* topic) {
  StaticJsonDocument<256> doc;

  doc["acc_x"] = values[0];
  doc["acc_y"] = values[1];
  doc["acc_z"] = values[2];

  doc["gyro_x"] = values[3];
  doc["gyro_y"] = values[4];
  doc["gyro_z"] = values[5];

  // generate the minified JSON
  char out[128];
  int bytes_dim = serializeJson(doc, out);

  Serial.print("bytes = ");
  Serial.println(bytes_dim, DEC);  // JSON's bytes weight

  // returns 1 if the MQTT client publish the messagge without errors
  return mqtt_client.publish(topic, out);

  // Serial.print("rc = ");
  // Serial.println(rc);
}

void connect() {
  static auto lastCheck = millis();

  Serial.print("»»» Checking WiFi Status");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print("»");
    delay(50);
  }
  Serial.print("\n»»» Connecting to MQTT queue");

  // try to connect to MQTT queue every second
  while (!mqtt_client.connect(DEV_NAME, MQTT_USER, MQTT_PWD) && (millis() - lastCheck >= 1000)) {
    Serial.print("»");
    delay(50);
  }

  char msg[30];
  sprintf(msg, "\n»»» Connected to %s", BROKER_IP);
  Serial.println(msg);
}

void message_handler(String& topic, String& payload) {
  Serial.println("»»» incoming: " + topic + " - " + payload);
}

void print_wifi_status() {
  Serial.println("»»» Board Information «««");

  // print the board's IP address
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  Serial.println();
  Serial.println("Network Information:");
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print the received signal strength
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.println(rssi);

  byte encryption = WiFi.encryptionType();
  Serial.print("Encryption Type:");
  Serial.println(encryption, HEX);
  Serial.println();
}