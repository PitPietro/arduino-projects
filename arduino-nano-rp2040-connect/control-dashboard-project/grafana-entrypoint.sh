#!/usr/bin/env sh

# environment variables are defined in docker-compose.yml

# echo '-------- ls --------';
# ls -lha /

echo "apiVersion: 1" > mysql-datasource.yaml
echo "datasources:" >> mysql-datasource.yaml
echo "  - name: MySQL" >> mysql-datasource.yaml
echo "    type: mysql" >> mysql-datasource.yaml
echo "    url: ${MYSQL_HOST}:3306 # changed to actual local IP (maybe better if static) or Docker's network name" >> mysql-datasource.yaml
echo "    user: ${MYSQL_USER}" >> mysql-datasource.yaml
echo "    jsonData:" >> mysql-datasource.yaml
echo "      database: ${MYSQL_DATABASE}" >> mysql-datasource.yaml
echo "      maxOpenConns: 100" >> mysql-datasource.yaml
echo "      maxIdleConns: 100" >> mysql-datasource.yaml
echo "      maxIdleConnsAuto: true" >> mysql-datasource.yaml
echo "      connMaxLifetime: 14400" >> mysql-datasource.yaml
echo "    secureJsonData:" >> mysql-datasource.yaml
echo "      password: ${MYSQL_PASSWORD}" >> mysql-datasource.yaml

cat mysql-datasource.yaml

mv mysql-datasource.yaml /etc/grafana/provisioning/datasources

/rename-db-name.sh ${MYSQL_DATABASE}

mv /arduino-dashboard.json /var/lib/grafana/dashboards

exec /run.sh $@