#include <Arduino_LSM6DSOX.h> // https://www.arduino.cc/reference/en/libraries/arduino_lsm6dsox/

float acceleration_x, acceleration_y, acceleration_z;
float gyroscope_x, gyroscope_y, gyroscope_z;

/**
   Access IMU Data on Nano RP2040 Connect

   An accelerometer is an electromechanical device that measures acceleration forces.
   A gyroscope sensor measures and maintains orientation and angular speed of an object.

   Gyroscopes are more advanced than accelerometers: they can measure the tilt and lateral
   orientation of an object, whereas an accelerometer can only measure its linear motion.

   Docs: https://docs.arduino.cc/tutorials/nano-rp2040-connect/rp2040-imu-basics
*/
void setup() {
  // initialize serial communication and wait for port to open
  Serial.begin(9600);
  // wait for serial port to connect. Needed for native USB port only
  while (!Serial);

  // initialize the IMU, return 1 on success and 0 on failure
  // de-initialize the IMU with 'IMU.end()' void function
  if (!IMU.begin()) {
    Serial.println("### Failed to initialize IMU ###");
    while (1);
  }

  // print on Serial Monitor IMU's accelerometer sample rate in Hz (Hertz)
  Serial.print("Accelerometer sample rate = ");
  Serial.print(IMU.accelerationSampleRate());
  Serial.println("Hz");
  Serial.println();

  // print on Serial Monitor IMU's gyroscope sample rate in Hz (Hertz)
  Serial.print("Gyroscope sample rate = ");
  Serial.print(IMU.gyroscopeSampleRate());
  Serial.println("Hz");
  Serial.println();

}

void loop() {

  // query if new acceleration data from the IMU is available
  if (IMU.accelerationAvailable()) {

    // read the relative position of the board
    // query the IMU's accelerometer and return the acceleration in g's
    IMU.readAcceleration(acceleration_x, acceleration_y, acceleration_z);

    Serial.println("Accelerometer data: ");
    Serial.print(acceleration_x);
    Serial.print('\t');
    Serial.print(acceleration_y);
    Serial.print('\t');
    Serial.println(acceleration_z);
    Serial.println();
  }

  // query if new gyroscope data from the IMU is available
  if (IMU.gyroscopeAvailable()) {

    // query the IMU's gyroscope and return the angular speed in dps (degrees per second)
    IMU.readGyroscope(gyroscope_x, gyroscope_y, gyroscope_z);

    Serial.println("Gyroscope data: ");
    Serial.print(gyroscope_x);
    Serial.print('\t');
    Serial.print(gyroscope_y);
    Serial.print('\t');
    Serial.println(gyroscope_z);
    Serial.println();
  }

  // add a short delay on the bottom of loop()
  delay(500);
}
