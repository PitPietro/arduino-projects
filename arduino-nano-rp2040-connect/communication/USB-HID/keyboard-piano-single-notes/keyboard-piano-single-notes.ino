#include "PluggableUSBHID.h"
#include "USBKeyboard.h" // https://github.com/arduino/ArduinoCore-mbed/tree/master/libraries/USBHID

USBKeyboard Keyboard;

// pin numbers for the buttons
const char pin_d11 = 11;
const char pin_d10 = 10;
const char pin_d9 = 9;
const char pin_d8 = 8;
const char pin_d7 = 7;
const char pin_d6 = 6;
const char pin_d5 = 5;
const char pin_d4 = 4;
const char pin_d3 = 3;
const char pin_d2 = 2;

// avoid repeting the same code foreach button
void press_release_key(char pin_number, char key_to_use);

/*
  Keyboard control for Virtual MIDI Piano

  You need to use an Arduino Nano (RP 2040 Connect)
  The comments assume: Base Octave = 1, Transpose = 0.

  Virtual MIDI Piano Keyboard: https://vmpk.sourceforge.io/
  Links: https://www.arduino.cc/reference/en/language/functions/usb/keyboard/keyboardmodifiers/
*/
void setup() {
  // initialize inputs
  pinMode(pin_d11, INPUT);
  pinMode(pin_d10, INPUT);
  pinMode(pin_d9, INPUT);
  pinMode(pin_d8, INPUT);
  pinMode(pin_d7, INPUT);
  pinMode(pin_d6, INPUT);
  pinMode(pin_d5, INPUT);
  pinMode(pin_d4, INPUT);
  pinMode(pin_d3, INPUT);
  pinMode(pin_d2, INPUT);

  // Serial.begin(9600);
}

/*
   Keyboard Piano Single Note with Arduino Nano

  This is the worst code I've ever written.
  Arduino Nano boards cannot use "Keyboard.h" library so they have to
  use "PluggableUSBHID.h" and "USBKeyboard.h" that lead to this ugly code.
*/
void loop() {
  if (digitalRead(pin_d11) == HIGH) {
    Keyboard.printf("m'\n\r");
  }
  
  if (digitalRead(pin_d10) == HIGH) {
    Keyboard.printf("n\n\r");
  }

  if (digitalRead(pin_d9) == HIGH) {
    Keyboard.printf("b\n\r");
  }

  if (digitalRead(pin_d8) == HIGH) {
    Keyboard.printf("g\n\r");
  }

  if (digitalRead(pin_d7) == HIGH) {
    Keyboard.printf("v\n\r");
  }

  if (digitalRead(pin_d6) == HIGH) {
    Keyboard.printf("c\n\r");
  }

  if (digitalRead(pin_d5) == HIGH) {
    Keyboard.printf("d\n\r");
  }

  if (digitalRead(pin_d4) == HIGH) {
    Keyboard.printf("x\n\r");
  }

  if (digitalRead(pin_d3) == HIGH) {
    Keyboard.printf("s\n\r");
  }

  if (digitalRead(pin_d2) == HIGH) {
    Keyboard.printf("z\n\r");
  }


  delay(100);
}
